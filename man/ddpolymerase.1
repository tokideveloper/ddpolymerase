.TH DDPOLYMERASE 1 "Sep 2023" "ddpolymerase 0.2.0"
.SH NAME
ddpolymerase \- copy, repair and verify a file
.SH SYNOPSIS
.B ddpolymerase
[\fIOPTIONS\fR]
\,[\fB--src \fISOURCE\fR] [\fB--dest \fIDEST\fR]
.SH DESCRIPTION
In several passes (see \fB--max-passes\fR), \fBddpolymerase\fR tries to verify that \fIDEST\fR is a copy of \fISOURCE\fR (global verification) and repairs \fIDEST\fR where necessary.
Blocks which are equal in \fISOURCE\fR and \fIDEST\fR are called "good blocks".
If there are differences, ddpolymerase tries to repair and re-verify them locally up to a maximum number of times (see \fB--max-tries\fR).
If that maximum number of repair tries is exceeded then the respective blocks are called "bad blocks", otherwise "repaired blocks".
Successfully repaired blocks beyond the end of \fIDEST\fR are called "copied blocks" rather than "repaired blocks".
.PP
At the end of each pass, ddpolymerase tries to truncate \fIDEST\fR to the size of \fISOURCE\fR if \fB--max-tries\fR is greater than zero, otherwise not.
Note that e.g. block devices cannot be truncated.
.PP
If there is a pass where all blocks match then ddpolymerase terminates successfully.
If the lengths of \fISOURCE\fR and \fIDEST\fR differ at termination or if there are blocks to be repaired during the last pass or if an error occurs then ddpolymerase terminates unsuccessfully (see \fBEXIT STATUS\fR).
.PP
.SH ARGS
.TP
.I SOURCE
The source file. Defaults to stdin. The result is undefined if \fISOURCE\fR changes during a run.
.TP
.I DEST
The destination file. Defaults to stdout.
.SH OPTIONS
.TP
.B --abort-on-bad-block
Abort as soon as a bad block has been detected.
.TP
.BI "--block-log-file " FILE
Log numbers of good, copied, repaired and bad blocks and errors to \fIFILE\fR. See \fBBLOCK LOG FILE\fR.
.TP
.BI "--block-size " [auto|BYTES]
Block size of destination filesystem or device. If set to \fIauto\fR, ddpolymerase tries to determine the block size and falls back to 4KiB if unsuccessful. [default: auto]
.TP
.BI "--buf-size " [auto|BYTES]
Shortcut for \fB--ibuf-size\fR and \fB--obuf-size\fR with the same values. [default: auto]
.TP
.BI "--color " [never|always|auto]
Use colored output if supported. [default: auto]
.TP
.B --copy-first
During the first pass, (1) assume that all blocks are to be repaired, (2) do not verify locally after each repair try but treat the repaired blocks as successfully repaired. Implicitly set if \fIDEST\fR is not seekable (e.g. a pipe, a file redirection or plain stdout).
.TP
\fB-f\fR, \fB--force\fR
Do not abort on effectively empty \fISOURCE\fR. Do not abort if destination block device is initially known to be smaller than \fISOURCE\fR.
.TP
\fB-h\fR, \fB--help\fR
Print help information.
.TP
.BI "--ibuf-size " [auto|BYTES]
Maximum read-at-once size (input buffer size) in bytes (with optional unit suffix). If set to \fIauto\fR, ddpolymerase tries to determine an appropriate input buffer size and falls back to 1MiB if unsuccessful. [default: auto]
.TP
.BI "--max-passes " NUMBER
Maximum number of passes. Defaults to (and must be) 1 if \fIDEST\fR is not seekable (e.g. a pipe, a file redirection or plain stdout). Defaults to 4 otherwise.
.TP
.BI "--max-tries " NUMBER
Maximum number of repair tries per block and pass. If set to 0, blocks to be repaired will not be repaired but counted as bad blocks immediately. [default: 3]
.TP
.BI "--obuf-size " [auto|BYTES]
Maximum write-at-once size (output buffer size) in bytes (with optional unit suffix). If set to \fIauto\fR, ddpolymerase tries to determine an appropriate output buffer size and falls back to 1MiB if unsuccessful. [default: auto]
.TP
.BI "--on-dest-error " [abort|skip]
How to handle errors when reading from or writing to \fIDEST\fR. \fIabort\fR aborts the run. \fIskip\fR skips the affected ranges. [default: abort]
.TP
.BI "--on-src-error " [abort|skip|zeroize]
How to handle errors when reading from \fISOURCE\fR. \fIabort\fR aborts the run. \fIskip\fR skips the affected ranges in both \fISRC\fR and \fIDEST\fR but appends NUL bytes to \fIDEST\fR if necessary. \fIzeroize\fR pretends that NUL bytes have been read from \fISRC\fR successfully. [default: abort]
.TP
\fB-q\fR, \fB--quiet\fR
Suppress all information on stderr but error messages.
.TP
.BI "--stats-unit " [human-readable|blk|B|KB|KiB|MB|MiB|...|YB|YiB]
Show statistics in the given unit. If set to \fIhuman-readable\fR, ddpolymerase chooses an appropriate binary unit dynamically. [default: human-readable]
.TP
\fB-V\fR, \fB--version\fR
Print version information.
.P
Allowed unit suffixes for \fIBYTES\fR are B=1, KB=1000, KiB=1024, MB=1000*1000, MiB=1024*1024 etc. for G, T, P, E, Z, Y.
.SH HINTS
.TP
If ddpolymerase appears to be slow, try setting \fB--ibuf-size\fR and \fB--obuf-size\fR to other values.
.P
.SH PROGRESS BAR SYMBOLS
.TP
.B ">"
Cursor
.TP
.B "<space>"
Unvisited
.TP
.B "~"
Pending
.TP
.B "."
Good blocks
.TP
.B "c"
Copied blocks
.TP
.B "r"
Repaired blocks
.TP
.B "b"
Bad blocks
.TP
.B "s"
Source error blocks
.TP
.B "d"
Destination error blocks
.SH BLOCK LOG FILE
Each line of the block log file has one of the following syntaxes and semantics:
.TP
.B P \fIn\fR \fIb\fR \fIc\fR \fIs\fR
Pass number \fIn\fR; \fIb\fR bytes in \fISOURCE\fR; spans \fIc\fR blocks of \fIs\fR bytes each. \fIb\fR and \fIc\fR appear as question marks '?' if the size of \fISOURCE\fR is initially unknown.
.TP
.B g \fIf\fR \fIt\fR \fIc\fR
Good blocks from \fIf\fR to \fIt\fR (inclusive); spans \fIc\fR blocks
.TP
.B c \fIf\fR \fIt\fR \fIc\fR
Copied blocks from \fIf\fR to \fIt\fR (inclusive); spans \fIc\fR blocks
.TP
.B r \fIf\fR \fIt\fR \fIc\fR
Repaired blocks from \fIf\fR to \fIt\fR (inclusive); spans \fIc\fR blocks
.TP
.B b \fIf\fR \fIt\fR \fIc\fR
Bad blocks from \fIf\fR to \fIt\fR (inclusive); spans \fIc\fR blocks
.TP
.B s \fIf\fR \fIt\fR \fIc\fR
Source error blocks from \fIf\fR to \fIt\fR (inclusive); spans \fIc\fR blocks
.TP
.B d \fIf\fR \fIt\fR \fIc\fR
Destination error blocks from \fIf\fR to \fIt\fR (inclusive); spans \fIc\fR blocks
.TP
The order in which the lines are written within a pass is undefined.
.SH EXIT STATUS
.TP
.B 0
Last pass was a successful verification pass.
.TP
Exit status values up to 15 are a binary combination (i.e. sum) of the following values:
.P
.TP
.B 1
Different file lengths at termination. Ignored (i.e. 0) if \fIDEST\fR is a block device.
.TP
.B 2
Last pass was a pass with successful repairs.
.TP
.B 4
Bad blocks left.
.TP
.B 8
Source errors and/or destination errors occurred in the last pass.
.TP
Other exit status values include:
.P
.TP
.B 64
Argument error.
.TP
.B 65
Other type of error.
.SH EXAMPLES
.TP 
Repair file \fIB\fR in order to become a copy of file \fIA\fR:
.P
       \fBddpolymerase --src=\fIA\fB --dest=\fIB\fR
.TP
Copy file \fIA\fR to \fIB\fR and repair where necessary:
.P
       \fBddpolymerase --copy-first --src=\fIA\fB --dest=\fIB\fR
.TP
Copy file \fIA\fR to \fIB\fR without any verification:
.P
       \fBddpolymerase --copy-first --max-passes=1 --src=\fIA\fB --dest=\fIB\fR
.TP
Verify that file \fIB\fR is a copy of file \fIA\fR:
.P
       \fBddpolymerase --abort-on-bad-block --max-passes=1 --max-tries=0 --src=\fIA\fB --dest=\fIB\fR
.TP
Show the differences between file \fIA\fR and \fIB\fR as bad blocks without repairing them:
.P
       \fBddpolymerase --max-passes=1 --max-tries=0 --src=\fIA\fB --dest=\fIB\fR
.TP
Copy an image file to a block device and repair where necessary:
.P
       \fBddpolymerase --copy-first --src=image.iso --dest=/dev/sd\fIX\fR
.TP
Copy a block device to an image file and repair where necessary:
.P
       \fBddpolymerase --copy-first --src=/dev/sd\fIX\fB --dest=image.iso\fR
.TP
Copy a block device to another block device and repair where necessary:
.P
       \fBddpolymerase --copy-first --src=/dev/sd\fIX\fB --dest=/dev/sd\fIY\fR
.TP
Zeroize a block device:
.P
       \fBddpolymerase --src=/dev/zero --dest=/dev/sd\fIX\fR
.TP
Try to remagnetize file \fIA\fR on an HDD:
.P
       \fBddpolymerase --copy-first --max-passes=1 --src=\fIA\fB --dest=\fIA\fR
.\".SH BUGS
.\".TP
.\"(none known)
.\".P
.SH AUTHOR
.TP
Written by Tobias Killer.
.P
.SH REPORTING BUGS
.TP
Report bugs to https://codeberg.org/tokideveloper/ddpolymerase/issues
.P
.SH COPYRIGHT
.TP
Copyright (C) 2023 Tobias Killer.  License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>. This is free software: you are free to change and redistribute it.  There is NO WARRANTY, to the extent permitted by law.
.P
