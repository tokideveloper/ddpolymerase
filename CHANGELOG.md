# Changelog

## v0.2.0 - 2023-09-10

### Added

- Add support for stdin and stdout.
- Add command line switches for error handling.
- Add bash-completion support.
- Add command-line option `--buf-size` as shortcut for `--ibuf-size` and `--obuf-size` (for easier use).
- Add non-Unicode support.

### Changed

- Always show progress as percentage (for unification).
- Do not complain about different file lengths if the destination is a block device (to avoid confusion).

### Fixed

- Minor bug fixes.
- Minor help text / manpage fixes.

## v0.1.1 - 2022-02-08

### Added

- Add semantics to `--force`: Do not abort if destination block device is smaller than source.

### Fixed

- Fix formatting of manpage examples.
- Fix semantics of `--max-passes=0`: Do nothing and return with exit status 64.
- Add "Reporting Bugs" section to manpage.
- Fix error handling if `dd` fails.
- Fix error handling if `dd` is to be terminated.
- Fix handling of blocks beyond destination file end.
- Fix semantics of `--copy-first --max-tries=0`: Do nothing and return with exit status 64.
- Fix error messages: Add/remove "Error" and "Info" prefixes.

## v0.1.0 - 2021-08-30

### Added

- first implementation
