/* ddpolymerase: Copy, repair and verify a file
 * Copyright (C) 2021-2023  Tobias Killer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! This application crate `ddpolymerase` is for copying, repairing and verifying a file.
//! Currently for Linux only.
//!
//! Given a source file and a destination file, `ddpolymerase` compares both files block-by-block
//! and only copies those blocks from source to destination which differ from destination.

extern crate termion;

use crate::block_log_file_writer::BlockLogFileWriter;
use crate::ddpolymerase::PassExitStatus;

use std::convert::TryInto;
use std::fs;
use std::path::{Path, PathBuf};
use std::process::ExitCode;
use std::str;
use std::str::FromStr;

use structopt::StructOpt;

use termion::{color, style};

mod block_log_file_writer;
mod buffer;
mod dd_reader;
mod dd_writer;
mod ddpolymerase;
mod progress_bar;
mod utils;

/// Exit status code for success.
const EXIT_SUCCESSFUL: u8 = 0;

/// Exit status code bit indicating different file lengths in the last pass.
const EXIT_DIFFERENT_FILE_LENGTHS: u8 = 1;

/// Exit status code bit indicating fixed but not verified blocks in the last pass.
const EXIT_FIXED_BUT_NOT_VERIFIED: u8 = 2;

/// Exit status code bit indicating bad blocks in the last pass.
const EXIT_BAD_BLOCKS_LEFT: u8 = 4;

/// Exit status code bit indicating read and/or write errors in the last pass.
const EXIT_READ_WRITE_ERRORS: u8 = 8;

/// Exit status code for argument error.
const EXIT_ARG_ERROR: u8 = 64;

/// Exit status code for other errors.
const EXIT_ERROR: u8 = 65;

/// Mode how to use colors.
enum ColorMode {
    /// Never use colors.
    Never,
    /// Always use colors.
    Always,
    /// Use colors only if printed to a terminal.
    Auto,
}

impl FromStr for ColorMode {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "never" => Ok(ColorMode::Never),
            "always" => Ok(ColorMode::Always),
            "auto" => Ok(ColorMode::Auto),
            _ => Err("Could not parse color mode".to_string()),
        }
    }
}

/// A size in bytes.
#[derive(Clone, PartialEq)]
enum AutoBytes {
    /// The size is determined automatically.
    Auto,
    /// The size is given in bytes with optional unit suffix.
    Bytes(String),
}

impl FromStr for AutoBytes {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "auto" => Ok(AutoBytes::Auto),
            b => Ok(AutoBytes::Bytes(b.to_string())),
        }
    }
}

/// How to handle errors when reading from or writing to destination.
pub(crate) enum OnDestinationErrorMode {
    /// Abort the run.
    Abort,
    /// Skip the affected ranges in destination.
    Skip,
}

impl FromStr for OnDestinationErrorMode {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "abort" => Ok(OnDestinationErrorMode::Abort),
            "skip" => Ok(OnDestinationErrorMode::Skip),
            _ => Err("Could not parse on-dest-error mode".to_string()),
        }
    }
}

/// How to handle errors when reading from source.
pub(crate) enum OnSourceErrorMode {
    /// Abort the run.
    Abort,
    /// Skip the affected ranges in both source and destination.
    Skip,
    /// Pretend that NUL bytes have been read from source successfully.
    Zeroize,
}

impl FromStr for OnSourceErrorMode {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "abort" => Ok(OnSourceErrorMode::Abort),
            "skip" => Ok(OnSourceErrorMode::Skip),
            "zeroize" => Ok(OnSourceErrorMode::Zeroize),
            _ => Err("Could not parse on-src-error mode".to_string()),
        }
    }
}

/// The command line parameters.
#[derive(StructOpt)]
#[structopt(about = "Copy, repair and verify a file.")]
struct Cli {
    #[structopt(
        long = "abort-on-bad-block",
        help = "Abort as soon as a bad block has been detected"
    )]
    abort_on_bad_block: bool,
    #[structopt(
        long = "block-log-file",
        help = "Log numbers of good, copied, repaired and bad blocks and errors to <block-log-file>"
    )]
    block_log_file: Option<PathBuf>,
    #[structopt(
        long = "block-size",
        default_value = "auto",
        help = "Block size of destination filesystem or device in bytes (with optional unit suffix). If set to `auto`, ddpolymerase tries to determine the block size and falls back to 4KiB if unsuccessful. Allowed values: [auto|BYTES]"
    )]
    block_size: AutoBytes,
    #[structopt(
        long = "buf-size",
        default_value = "auto",
        help = "Shortcut for --ibuf-size and --obuf-size with the same values"
    )]
    buf_size: AutoBytes,
    #[structopt(
        long = "color",
        default_value = "auto",
        help = "Use colored output if supported. Allowed values: [never|always|auto]"
    )]
    color_mode: ColorMode,
    #[structopt(
        long = "copy-first",
        help = "During the first pass, (1) assume that all blocks are to be repaired, (2) do not verify locally after each repair try but treat the repaired blocks as successfully repaired. Implicitly set if <dest> is not seekable"
    )]
    copy_first: bool,
    #[structopt(
        short = "f",
        long = "force",
        help = "Do not abort on effectively empty <src>. Do not abort if destination block device is initially known to be smaller than <src>"
    )]
    force: bool,
    #[structopt(
        long = "ibuf-size",
        default_value = "auto",
        help = "Maximum read-at-once size (input buffer size) in bytes (with optional unit suffix). If set to `auto`, ddpolymerase tries to determine an appropriate input buffer size and falls back to 1MiB if unsuccessful. Allowed values: [auto|BYTES]"
    )]
    ibuf_size: AutoBytes,
    #[structopt(
        long = "max-passes",
        help = "Maximum number of passes. Defaults to (and must be) 1 if <dest> is not seekable. Defaults to 4 otherwise"
    )]
    max_passes: Option<usize>,
    #[structopt(
        long = "max-tries",
        default_value = "3",
        help = "Maximum number of repair tries per block and pass. If set to 0, blocks to be repaired will not be repaired but counted as bad blocks immediately"
    )]
    max_tries: usize,
    #[structopt(
        long = "obuf-size",
        default_value = "auto",
        help = "Maximum write-at-once size (output buffer size) in bytes (with optional unit suffix). If set to `auto`, ddpolymerase tries to determine an appropriate output buffer size and falls back to 1MiB if unsuccessful. Allowed values: [auto|BYTES]"
    )]
    obuf_size: AutoBytes,
    #[structopt(
        long = "on-dest-error",
        default_value = "abort",
        help = "How to handle errors when reading from or writing to <dest>. `abort` aborts the run. `skip` skips the affected ranges. Allowed values: [abort|skip]"
    )]
    on_dest_error_mode: OnDestinationErrorMode,
    #[structopt(
        long = "on-src-error",
        default_value = "abort",
        help = "How to handle errors when reading from <src>. `abort` aborts the run. `skip` skips the affected ranges in both <src> and <dest> but appends NUL bytes to <dest> if necessary. `zeroize` pretends that NUL bytes have been read from <src> successfully. Allowed values: [abort|skip|zeroize]"
    )]
    on_src_error_mode: OnSourceErrorMode,
    #[structopt(
        short = "q",
        long = "quiet",
        help = "Suppress all information on stderr but error messages"
    )]
    quiet: bool,
    #[structopt(
        long = "stats-unit",
        default_value = "human-readable",
        help = "Show statistics in the given unit. If set to `human-readable`, ddpolymerase chooses an appropriate binary unit dynamically. Allowed values: [human-readable|blk|B|KB|KiB|MB|MiB|...|YB|YiB]"
    )]
    stats_unit: progress_bar::StatsUnit,
    #[structopt(
        long = "src",
        help = "The source file. Defaults to stdin. The result is undefined if <src> changes during a run"
    )]
    src: Option<PathBuf>,
    #[structopt(long = "dest", help = "The destination file. Defaults to stdout")]
    dest: Option<PathBuf>,
}

/// The main function.
fn main() -> ExitCode {
    // read args
    let args = Cli::from_args();
    let source_file_path = match args.src {
        Some(p) => p,
        None => {
            let mut p = PathBuf::new();
            p.push("/dev/stdin");
            p
        }
    };
    let dest_file_path = match args.dest {
        Some(p) => p,
        None => {
            let mut p = PathBuf::new();
            p.push("/dev/stdout");
            p
        }
    };
    let block_size = args.block_size;
    let buf_size = args.buf_size;
    let mut ibuf_size = args.ibuf_size;
    let mut obuf_size = args.obuf_size;
    let max_passes = args.max_passes;
    let max_tries = args.max_tries;
    let abort_on_bad_block = args.abort_on_bad_block;
    let on_dest_error_mode = args.on_dest_error_mode;
    let on_source_error_mode = args.on_src_error_mode;
    let block_log_file_path = args.block_log_file;
    let no_initial_ver = args.copy_first;
    let force = args.force;
    let quiet = args.quiet;
    let use_colors = match args.color_mode {
        ColorMode::Never => false,
        ColorMode::Always => true,
        ColorMode::Auto => {
            let mut result = true;
            result &= match fs::File::create("/dev/stderr") {
                Ok(f) => termion::is_tty(&f),
                Err(_) => false,
            };
            result
        }
    };
    let stats_unit = args.stats_unit;

    // args checks
    if !Path::new(&source_file_path).exists() {
        eprintln!("Error: Source file does not exist.");
        return ExitCode::from(EXIT_ARG_ERROR);
    }
    if Path::new(&source_file_path).is_dir() {
        eprintln!("Error: Source must be a file, not a directory.");
        return ExitCode::from(EXIT_ARG_ERROR);
    }
    if Path::new(&dest_file_path).exists() && Path::new(&dest_file_path).is_dir() {
        eprintln!("Error: Destination must be a file, not a directory.");
        return ExitCode::from(EXIT_ARG_ERROR);
    }
    if let Some(ref block_log_file_path) = block_log_file_path {
        if Path::new(&block_log_file_path).exists() && Path::new(&block_log_file_path).is_dir() {
            eprintln!("Error: Block log file is an existing directory.");
            return ExitCode::from(EXIT_ARG_ERROR);
        }
    }

    // --- some examinations
    // source
    let is_seekable_source = match utils::is_seekable_file(&source_file_path) {
        Ok(is_seekable) => is_seekable,
        Err(e) => {
            eprintln!(
                "Error: Could not determine whether the source is seekable: {}",
                e
            );
            return ExitCode::from(EXIT_ERROR);
        }
    };
    let is_redirected_source = match utils::is_probably_redirected(0) {
        Ok(is_probably_redirected) => is_probably_redirected,
        Err(e) => {
            eprintln!(
                "Error: Could not determine whether the source is a redirection: {}",
                e
            );
            return ExitCode::from(EXIT_ERROR);
        }
    };
    let is_truncatable_source =
        // Block devices are truncatable in the sense that dd does not fail when fixing the length.
        is_seekable_source && !is_redirected_source;
    // dest
    let is_seekable_dest = if Path::new(&dest_file_path).exists() {
        match utils::is_seekable_file(&dest_file_path) {
            Ok(is_seekable) => is_seekable,
            Err(e) => {
                eprintln!(
                    "Error: Could not determine whether the destination is seekable: {}",
                    e
                );
                return ExitCode::from(EXIT_ERROR);
            }
        }
    } else {
        true
    };
    let is_redirected_dest = if Path::new(&dest_file_path).exists() {
        match utils::is_probably_redirected(1) {
            Ok(is_probably_redirected) => is_probably_redirected,
            Err(e) => {
                eprintln!(
                    "Error: Could not determine whether the destination is a redirection: {}",
                    e
                );
                return ExitCode::from(EXIT_ERROR);
            }
        }
    } else {
        false
    };
    let is_truncatable_dest = if Path::new(&dest_file_path).exists() {
        // Block devices are truncatable in the sense that dd does not fail when fixing the length.
        is_seekable_dest && !is_redirected_dest
    } else {
        true
    };

    // re-interpretations
    /// Default maximum number of passes for seekable source and destination files.
    const NORMAL_MAX_PASSES: usize = 4;
    /// Default maximum number of passes for unseekable source and destination files.
    const RESTRICTED_MAX_PASSES: usize = 1;
    let max_passes_source = match max_passes {
        Some(p) => {
            if is_truncatable_source {
                p
            } else if p == RESTRICTED_MAX_PASSES {
                RESTRICTED_MAX_PASSES
            } else {
                eprintln!(
                    "Error: Number of maximum passes must be {}, because the source is not seekable (e.g. a pipe, a file redirection or plain stdin).",
                    RESTRICTED_MAX_PASSES
                );
                return ExitCode::from(EXIT_ARG_ERROR);
            }
        }
        None => {
            if is_truncatable_source {
                NORMAL_MAX_PASSES
            } else {
                RESTRICTED_MAX_PASSES
            }
        }
    };
    let max_passes_dest = match max_passes {
        Some(p) => {
            if is_truncatable_dest {
                p
            } else if p == RESTRICTED_MAX_PASSES {
                RESTRICTED_MAX_PASSES
            } else {
                eprintln!(
                    "Error: Number of maximum passes must be {}, because the destination is not seekable (e.g. a pipe, a file redirection or plain stdout).",
                    RESTRICTED_MAX_PASSES
                );
                return ExitCode::from(EXIT_ARG_ERROR);
            }
        }
        None => {
            if is_truncatable_dest {
                NORMAL_MAX_PASSES
            } else {
                RESTRICTED_MAX_PASSES
            }
        }
    };
    let max_passes = std::cmp::min(max_passes_source, max_passes_dest);
    let no_initial_ver = if is_truncatable_dest {
        no_initial_ver
    } else {
        if !no_initial_ver && !quiet {
            eprintln!(
                "Info: Implicit --copy-first, because the destination is not seekable (e.g. a pipe, a file redirection or plain stdout)."
            );
        }
        true
    };

    // --- early exits
    if max_passes == 0 {
        eprintln!("Error: Maximum number of passes must be greater than zero.");
        return ExitCode::from(EXIT_ARG_ERROR);
    }
    if no_initial_ver && max_tries == 0 {
        eprintln!("Error: The combination --copy-first --max-tries=0 is not allowed.");
        return ExitCode::from(EXIT_ARG_ERROR);
    }
    if buf_size != AutoBytes::Auto && ibuf_size != AutoBytes::Auto {
        eprintln!("Error: The combination --buf-size=<not \"auto\"> --ibuf-size=<not \"auto\"> is not allowed.");
        return ExitCode::from(EXIT_ARG_ERROR);
    }
    if buf_size != AutoBytes::Auto && obuf_size != AutoBytes::Auto {
        eprintln!("Error: The combination --buf-size=<not \"auto\"> --obuf-size=<not \"auto\"> is not allowed.");
        return ExitCode::from(EXIT_ARG_ERROR);
    }
    match on_source_error_mode {
        OnSourceErrorMode::Skip => {
            if !is_truncatable_source {
                eprintln!("Error: --on-src-err=skip is not allowed, because the source is not seekable (e.g. a pipe, a file redirection or plain stdin).");
                return ExitCode::from(EXIT_ARG_ERROR);
            }
            if !is_truncatable_dest {
                eprintln!("Error: --on-src-err=skip is not allowed, because the destination is not seekable (e.g. a pipe, a file redirection or plain stdout).");
                return ExitCode::from(EXIT_ARG_ERROR);
            }
        }
        OnSourceErrorMode::Zeroize => {
            if !is_truncatable_source {
                eprintln!("Error: --on-src-err=zeroize is not allowed, because the source is not seekable (e.g. a pipe, a file redirection or plain stdin).");
                return ExitCode::from(EXIT_ARG_ERROR);
            }
        }
        OnSourceErrorMode::Abort => {}
    }
    match on_dest_error_mode {
        OnDestinationErrorMode::Skip => {
            if !is_truncatable_dest {
                eprintln!("Error: --on-dest-err=skip is not allowed, because the destination is not seekable (e.g. a pipe, a file redirection or plain stdout).");
                return ExitCode::from(EXIT_ARG_ERROR);
            }
        }
        OnDestinationErrorMode::Abort => {}
    }

    // --- conversions
    // block size
    if !Path::new(&dest_file_path).exists() {
        let file = fs::File::create(&dest_file_path);
        match file {
            Ok(_) => {}
            Err(err) => {
                eprintln!("Error: Could not create destination file: {}", err);
                return ExitCode::from(EXIT_ERROR);
            }
        }
    }
    let block_size = match block_size {
        AutoBytes::Auto => match utils::get_block_size(&dest_file_path) {
            Ok(Some(bs)) => {
                if !quiet {
                    eprintln!("Info: Determined block size: {}", bs);
                }
                bs.to_string()
            }
            Ok(None) | Err(_) => {
                let default = "4KiB";
                if !quiet {
                    eprintln!(
                        "Info: Could not determine block size. Falling back to {}.",
                        default
                    );
                }
                default.to_string()
            }
        },
        AutoBytes::Bytes(bytes) => bytes,
    };
    let block_size: u128 = match utils::number_without_unit_suffix(&block_size) {
        Some(v) => v,
        None => {
            eprintln!("Error: Block size must be a valid number.");
            return ExitCode::from(EXIT_ARG_ERROR);
        }
    };
    let block_size: usize = match block_size.try_into() {
        Ok(v) => v,
        Err(_e) => {
            eprintln!(
                "Error: Block size must be between 1 and {} inclusive due to architecture limits.",
                usize::MAX
            );
            return ExitCode::from(EXIT_ARG_ERROR);
        }
    };
    let block_size: u64 = match block_size.try_into() {
        Ok(v) => v,
        Err(_e) => {
            eprintln!(
                "Error: Block size must be between 1 and {} inclusive.",
                u64::MAX
            );
            return ExitCode::from(EXIT_ARG_ERROR);
        }
    };
    // buf size
    if buf_size != AutoBytes::Auto {
        ibuf_size = buf_size.clone();
        obuf_size = buf_size;
    }
    // ibuf size
    let ibuf_size = match ibuf_size {
        AutoBytes::Auto => {
            let default = "1MiB";
            match utils::get_optimal_buf_size(&source_file_path) {
                Ok(bs) => {
                    if bs > block_size {
                        if !quiet {
                            eprintln!("Info: Determined optimal input buffer size: {}", bs);
                        }
                        bs.to_string()
                    } else {
                        if !quiet {
                            eprintln!("Info: Could not determine a plausible optimal input buffer size. Falling back to {}.", default);
                        }
                        default.to_string()
                    }
                }
                Err(_) => {
                    if !quiet {
                        eprintln!(
                        "Info: Could not determine optimal input buffer size. Falling back to {}.",
                        default
                    );
                    }
                    default.to_string()
                }
            }
        }
        AutoBytes::Bytes(bytes) => bytes,
    };
    let ibuf_size: u128 = match utils::number_without_unit_suffix(&ibuf_size) {
        Some(v) => v,
        None => {
            eprintln!("Error: Input buffer size must be a valid number.");
            return ExitCode::from(EXIT_ARG_ERROR);
        }
    };
    let ibuf_size: u64 = match ibuf_size.try_into() {
        Ok(v) => v,
        Err(_e) => {
            eprintln!(
                "Error: Input buffer size must be between 1 and {} inclusive.",
                u64::MAX
            );
            return ExitCode::from(EXIT_ARG_ERROR);
        }
    };
    // obuf size
    let obuf_size = match obuf_size {
        AutoBytes::Auto => {
            let default = "1MiB";
            match utils::get_optimal_buf_size(&dest_file_path) {
                Ok(bs) => {
                    if bs > block_size {
                        if !quiet {
                            eprintln!("Info: Determined optimal output buffer size: {}", bs);
                        }
                        bs.to_string()
                    } else {
                        if !quiet {
                            eprintln!("Info: Could not determine a plausible optimal output buffer size. Falling back to {}.", default);
                        }
                        default.to_string()
                    }
                }
                Err(_) => {
                    if !quiet {
                        eprintln!(
                        "Info: Could not determine optimal output buffer size. Falling back to {}.",
                        default
                    );
                    }
                    default.to_string()
                }
            }
        }
        AutoBytes::Bytes(bytes) => bytes,
    };
    let obuf_size: u128 = match utils::number_without_unit_suffix(&obuf_size) {
        Some(v) => v,
        None => {
            eprintln!("Error: Output buffer size must be a valid number.");
            return ExitCode::from(EXIT_ARG_ERROR);
        }
    };
    let obuf_size: u64 = match obuf_size.try_into() {
        Ok(v) => v,
        Err(_e) => {
            eprintln!(
                "Error: Output buffer size must be between 1 and {} inclusive.",
                u64::MAX
            );
            return ExitCode::from(EXIT_ARG_ERROR);
        }
    };

    // other args checks
    if block_size == 0 {
        eprintln!("Error: Block size must be greater than zero.");
        return ExitCode::from(EXIT_ARG_ERROR);
    }
    if ibuf_size == 0 {
        eprintln!("Error: Input buffer size must be greater than zero.");
        return ExitCode::from(EXIT_ARG_ERROR);
    }
    if obuf_size == 0 {
        eprintln!("Error: Output buffer size must be greater than zero.");
        return ExitCode::from(EXIT_ARG_ERROR);
    }

    // file metadata
    let source_file_length = match utils::get_file_size(&source_file_path) {
        Ok(len) => len,
        Err(err) => {
            eprintln!("Error: Could not determine source file length: {}", err);
            return ExitCode::from(EXIT_ERROR);
        }
    };
    let dest_file_length = {
        if Path::new(&dest_file_path).exists() {
            match utils::get_file_size(&dest_file_path) {
                Ok(len) => len,
                Err(err) => {
                    eprintln!(
                        "Error: Could not determine destination file length: {}",
                        err
                    );
                    return ExitCode::from(EXIT_ERROR);
                }
            }
        } else {
            Some(0)
        }
    };

    // warnings
    let ibuf_size = if ibuf_size < block_size {
        if !quiet {
            eprintln!("Warning: Input buffer size is less than block size. Input buffer size rounded up to block size.");
        }
        block_size
    } else {
        ibuf_size
    };
    if ibuf_size % block_size != 0 && !quiet {
        eprintln!(
            "Warning: Input buffer size rounded down to next integer multiple of block size."
        );
    }
    let blocks_per_ibuf = ibuf_size / block_size;
    //
    let obuf_size = if obuf_size < block_size {
        if !quiet {
            eprintln!("Warning: Output buffer size is less than block size. Output buffer size rounded up to block size.");
        }
        block_size
    } else {
        obuf_size
    };
    if obuf_size % block_size != 0 && !quiet {
        eprintln!(
            "Warning: Output buffer size rounded down to next integer multiple of block size."
        );
    }
    let obuf_size_in_blocks = obuf_size / block_size;

    // errors
    if !force {
        if let (Some(source_file_length), Some(dest_file_length)) =
            (source_file_length, dest_file_length)
        {
            if source_file_length > dest_file_length {
                if let Ok(Some(utils::FileType::BlockDevice)) =
                    utils::get_file_type(&dest_file_path)
                {
                    eprintln!("Error: Source file is larger than destination block device. Use -f to proceed anyway.");
                    return ExitCode::from(EXIT_ERROR);
                }
            }
        }
    }

    // init
    let (term_width, _term_height) = match termion::terminal_size() {
        Ok((w, h)) => (w, h),
        Err(_) => (80, 25),
    };
    // open block file
    let mut block_log_file_writer = match block_log_file_path {
        Some(block_log_file_path) => {
            let file = fs::File::create(&block_log_file_path);
            match file {
                Ok(f) => Some(BlockLogFileWriter::new(f)),
                Err(err) => {
                    eprintln!("Error: Could not create block log file: {}", err);
                    return ExitCode::from(EXIT_ERROR);
                }
            }
        }
        None => None,
    };

    let mut exit_status = EXIT_SUCCESSFUL;
    let mut msg = None;
    let mut mesg = String::new();
    let mut msg2 = None;
    let mut fg_color = None;
    let mut is_first_pass = true;
    let mut no_global_ver;
    let mut no_local_ver;
    let mut error_occurred = false;

    // passes
    for pass in 0..max_passes {
        if no_initial_ver && is_first_pass {
            is_first_pass = false;
            no_global_ver = true;
            no_local_ver = true;
        } else {
            no_global_ver = false;
            no_local_ver = false;
        }
        let ddpm = ddpolymerase::DdPolymerase::new(
            &source_file_path,
            &dest_file_path,
            source_file_length,
            block_size,
            blocks_per_ibuf,
            obuf_size_in_blocks,
            max_tries,
            abort_on_bad_block,
            &on_source_error_mode,
            &on_dest_error_mode,
            &mut block_log_file_writer,
            no_global_ver,
            no_local_ver,
            is_truncatable_dest,
            quiet,
            max_passes,
            pass + 1,
            term_width,
            stats_unit,
            use_colors,
            force,
        );
        if let Err(err) = ddpm {
            eprintln!("Init failed: {}", err);
            exit_status = EXIT_ERROR;
            break;
        }
        let ddpm = ddpm.unwrap();
        match ddpm.pass() {
            Ok(PassExitStatus {
                mut different_file_lengths,
                fixed_but_not_verified,
                bad_blocks_left,
                read_write_errors,
            }) => {
                if different_file_lengths {
                    if let Ok(Some(utils::FileType::BlockDevice)) =
                        utils::get_file_type(&dest_file_path)
                    {
                        different_file_lengths = false;
                    }
                }
                if !different_file_lengths
                    && !fixed_but_not_verified
                    && !bad_blocks_left
                    && !read_write_errors
                {
                    msg = Some("Verification successful.");
                    fg_color = Some(format!("{}", color::Fg(color::LightGreen)));
                    exit_status = EXIT_SUCCESSFUL;
                    break;
                } else {
                    mesg.clear();
                    if different_file_lengths {
                        mesg.push_str("Different file lengths. ");
                    }
                    if fixed_but_not_verified {
                        mesg.push_str("Repaired/copied but not verified. ");
                    }
                    if bad_blocks_left {
                        mesg.push_str("Bad blocks left. ");
                    }
                    if read_write_errors {
                        mesg.push_str("Read/write errors.");
                    }
                    msg = Some(&mesg);
                    fg_color = if different_file_lengths || bad_blocks_left || read_write_errors {
                        Some(format!("{}", color::Fg(color::LightRed)))
                    } else {
                        Some(format!("{}", color::Fg(color::LightYellow)))
                    };
                    exit_status = if different_file_lengths {
                        EXIT_DIFFERENT_FILE_LENGTHS
                    } else {
                        0
                    } + if fixed_but_not_verified {
                        EXIT_FIXED_BUT_NOT_VERIFIED
                    } else {
                        0
                    } + if bad_blocks_left {
                        EXIT_BAD_BLOCKS_LEFT
                    } else {
                        0
                    } + if read_write_errors {
                        EXIT_READ_WRITE_ERRORS
                    } else {
                        0
                    };
                    if abort_on_bad_block && bad_blocks_left {
                        break;
                    }
                    if !fixed_but_not_verified && !bad_blocks_left && !read_write_errors {
                        break;
                    }
                }
            }
            Err(err) => {
                msg = Some("Error:");
                msg2 = Some(format!(" {}", err));
                fg_color = Some(format!("{}", color::Fg(color::LightRed)));
                exit_status = EXIT_ERROR;
                error_occurred = true;
                break;
            }
        }
    }
    if !quiet || error_occurred {
        if let Some(m) = msg {
            if use_colors {
                if let Some(ref c) = fg_color {
                    eprint!("{}{}{}", c, color::Bg(color::Black), style::Bold);
                }
            }
            eprint!("{}", m);
            if use_colors && fg_color.is_some() {
                eprint!("{}", style::Reset);
            }
        }
        if let Some(m) = msg2 {
            eprint!("{}", m);
        }
        eprintln!();
    }
    ExitCode::from(exit_status)
}
