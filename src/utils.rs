/* ddpolymerase: Copy, repair and verify a file
 * Copyright (C) 2021-2023  Tobias Killer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! Some utility functions and structs.

use std::cmp;
use std::fs;
use std::io::{self, Write};
use std::os::unix::fs::FileTypeExt;
use std::path::{Path, PathBuf};
use std::process;
use std::str::{self, FromStr};

/// Represents the value of a byte unit prefix.
#[derive(Copy, Clone)]
pub(crate) struct ByteUnitPrefixNum {
    /// The exponent.
    pub(crate) exponent: u32,
    /// The base, 1000 or 1024.
    pub(crate) base: u128,
    /// If the byte unit is binary.
    pub(crate) is_binary: bool,
}

impl FromStr for ByteUnitPrefixNum {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let byte_unit_prefix = ByteUnitPrefix::from_str(s)?;
        byte_unit_prefix_to_byte_unit_prefix_num(byte_unit_prefix)
    }
}

/// Converts a [`ByteUnitPrefix`] to a [`ByteUnitPrefixNum`].
fn byte_unit_prefix_to_byte_unit_prefix_num(
    byte_unit_prefix: ByteUnitPrefix,
) -> Result<ByteUnitPrefixNum, String> {
    let exponent: u32 = exponent_name_to_number(byte_unit_prefix.letter)?;
    let base: u128 = if byte_unit_prefix.is_binary {
        1024
    } else {
        1000
    };
    Ok(ByteUnitPrefixNum {
        exponent,
        base,
        is_binary: base == 1024,
    })
}

/// Maps an exponent letter to the exponent value.
///
/// `None` maps to `Ok(0)`, `Some('K')` maps to `Ok(1)`, `Some('M')` maps to `Ok(2)` etc. for `G`, `T`, `P`, `E`, `Z` and `Y`.
fn exponent_name_to_number(letter: Option<char>) -> Result<u32, String> {
    match letter {
        None => Ok(0),
        Some('K') => Ok(1),
        Some('M') => Ok(2),
        Some('G') => Ok(3),
        Some('T') => Ok(4),
        Some('P') => Ok(5),
        Some('E') => Ok(6),
        Some('Z') => Ok(7),
        Some('Y') => Ok(8),
        _ => Err(format!(
            "Could not parse unit prefix: `{}`",
            letter.unwrap()
        )),
    }
}

/// Describes the SI prefix (e.g. "Mi") of a byte unit suffix (e.g. "MiB").
///
/// - `letter`: one of `'K'`, `'M'`, `'G'`, `'T'`, `'P'`, `'E'`, `'Z'`, `'Y'` if `Some(...)` or no letter if `None`.
/// - `is_binary`: `true` if base is 1024, `false` if base is 1000. Does not matter if `letter` is `None`.
#[derive(Copy, Clone)]
pub(crate) struct ByteUnitPrefix {
    letter: Option<char>,
    is_binary: bool,
}

impl FromStr for ByteUnitPrefix {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut the_letter = None;
        let mut i_read = false;
        let mut b_read = false;
        for c in s.chars() {
            match c {
                x@('K' | 'M' | 'G' | 'T' | 'P' | 'E' | 'Z' | 'Y') if the_letter.is_none() && !i_read && !b_read => the_letter = Some(x),
                'i' if the_letter.is_some() && !i_read && !b_read => i_read = true,
                'B' if /*((the_letter.is_none() && !i_read) || (the_letter.is_some())) &&*/ !b_read => b_read = true,
                _ => return Err(format!("Could not parse byte unit prefix: `{}`", s)),
            }
        }
        if the_letter.is_some() && !b_read {
            return Err(format!("Could not parse byte unit prefix: `{}`", s));
        }

        Ok(ByteUnitPrefix {
            letter: the_letter,
            is_binary: i_read,
        })
    }
}

/// Converts a string containing a number and a unit suffix to its value in bytes.
///
/// For example, `"2KiB"` will be converted to `Some(2048u128)`.
///
/// If an error occurs, `None` is returned.
pub(crate) fn number_without_unit_suffix(number_with_unit_suffix: &str) -> Option<u128> {
    // split into number and unit suffix
    let mut split_pos = 0;
    for c in number_with_unit_suffix.chars() {
        match c {
            '0'..='9' => split_pos += 1,
            _ => break,
        }
    }
    let (the_number, the_unit_prefix) = number_with_unit_suffix.split_at(split_pos);

    // parse number
    let the_number = match the_number.parse::<u128>() {
        Ok(val) => val,
        _ => return None,
    };

    // parse unit prefix
    let byte_unit_prefix = match ByteUnitPrefix::from_str(the_unit_prefix) {
        Ok(bs) => bs,
        _ => return None,
    };

    // calculate value
    let ByteUnitPrefixNum {
        exponent,
        base,
        is_binary: _,
    } = match byte_unit_prefix_to_byte_unit_prefix_num(byte_unit_prefix) {
        Ok(eb) => eb,
        _ => return None,
    };

    the_number.checked_mul(base.checked_pow(exponent)?)
}

/// Converts a number to a number with a unit prefix.
///
/// Parameters:
/// - `number`: The number to convert.
/// - `binary`: `true` if `1024` is to be used as base, `false` if `1000`.
/// - `max_exponent`: The maximum exponent to use.
///
/// Examples:
/// - `number_with_unit_prefix(2048.0, true, 0)` returns `(2048.0, "".to_string())`.
/// - `number_with_unit_prefix(2048.0, true, 1)` returns `(2.0, "Ki".to_string())`.
/// - `number_with_unit_prefix(2048.0, true, 2)` returns `(2.0, "Ki".to_string())`.
/// - `number_with_unit_prefix(2048.0, false, 0)` returns `(2048.0, "".to_string())`.
/// - `number_with_unit_prefix(2048.0, false, 1)` returns `(2.048, "Ki".to_string())`.
/// - `number_with_unit_prefix(2048.0, false, 2)` returns `(2.048, "Ki".to_string())`.
pub(crate) fn number_with_unit_prefix(
    number: f64,
    binary: bool,
    max_exponent: Option<u32>,
) -> (f64, String) {
    let mut num = number;
    let factor = if binary { 1024.0 } else { 1000.0 };
    let mut exponent = 0;
    let max_exp = max_exponent.unwrap_or(8);
    let ignore = max_exponent.is_some();
    for _ in 0..(cmp::min(8, max_exp)) {
        if ignore || num >= factor {
            num /= factor;
            exponent += 1;
        }
    }
    let mut prefix = match exponent_to_exponent_name(exponent) {
        Ok(exp_name) => exp_name,
        _ => unreachable!(),
    };
    if exponent > 0 && binary {
        prefix.push('i');
    }
    (num, prefix)
}

/// Converts an exponent to its name's initial.
///
/// Examples:
/// `exponent_to_exponent_name(0)` returns `Ok("".to_string())`.
/// `exponent_to_exponent_name(1)` returns `Ok("K".to_string())`.
/// `exponent_to_exponent_name(2)` returns `Ok("M".to_string())`.
fn exponent_to_exponent_name(exponent: u32) -> Result<String, String> {
    match exponent {
        0 => Ok("".to_string()),
        1 => Ok("K".to_string()),
        2 => Ok("M".to_string()),
        3 => Ok("G".to_string()),
        4 => Ok("T".to_string()),
        5 => Ok("P".to_string()),
        6 => Ok("E".to_string()),
        7 => Ok("Z".to_string()),
        8 => Ok("Y".to_string()),
        _ => Err(format!("No known unit prefix for exponent {}", exponent)),
    }
}

/// Formats the given `secs` seconds using at maximum `max_units` many appropriate unit suffixes.
///
/// Examples:
///
/// `seconds_to_string(0, 0)` returns `"".to_string()`.
/// `seconds_to_string(0, 3)` returns `"0 s".to_string()`.
/// `seconds_to_string(5, 1)` returns `"5 s".to_string()`.
/// `seconds_to_string(70, 2)` returns `"1 min 10 s".to_string()`.
/// `seconds_to_string(70, 1)` returns `"1 min".to_string()`.
pub(crate) fn seconds_to_string(secs: u64, max_units: usize) -> String {
    /// The four units `s` (for seconds), `min` (for minutes), `h` (for hours) and `d` (for days)
    /// combined with their numbers to the respective next higher unit.
    const CONVERSIONS: [(u64, &str); 4] = [(60, "s"), (60, "min"), (24, "h"), (u64::MAX, "d")];

    let mut parts = [0; CONVERSIONS.len()];
    let mut rem = secs;
    for (i, (c, _)) in CONVERSIONS.iter().enumerate() {
        parts[i] = rem % c;
        rem /= c;
    }

    let mut units = 0;
    let mut result = String::new();
    for i in (0..parts.len()).rev() {
        if units < max_units && (parts[i] > 0 || !result.is_empty()) {
            if !result.is_empty() {
                result.push(' ');
            }
            result.push_str(&format!("{} {}", parts[i], CONVERSIONS[i].1));
            units += 1;
        }
    }
    if units < max_units && result.is_empty() {
        result.push_str("0 s");
    }

    result
}

/// The allowed file types.
#[derive(PartialEq)]
pub(crate) enum FileType {
    RegularFile,
    Directory,
    BlockDevice,
    Pipe,
}

/// Returns the file size of the file given by `file_path` or `Ok(None)` if the file is a pipe.
pub(crate) fn get_file_size(file_path: &PathBuf) -> io::Result<Option<u64>> {
    match get_file_type(file_path)? {
        Some(FileType::RegularFile) => Ok(Some(fs::File::open(file_path)?.metadata()?.len())),
        Some(FileType::Directory) => Err(io::Error::new(
            io::ErrorKind::Other,
            format!(
                "Cannot determine the file size of a directory: {}",
                file_path.display()
            ),
        )),
        Some(FileType::BlockDevice) => Ok(Some(get_block_device_size(file_path)?)),
        Some(FileType::Pipe) | None => Ok(None),
    }
}

/// Returns the file type of the file given by `file_path` or `Ok(None)` if the file refers to unredirected/unpiped stdin/stdout/stderr.
pub(crate) fn get_file_type(path: &PathBuf) -> io::Result<Option<FileType>> {
    let p = Path::new(path);
    let ft = fs::metadata(p)?.file_type();
    if ft.is_dir() {
        return Ok(Some(FileType::Directory));
    }
    if ft.is_file() {
        return Ok(Some(FileType::RegularFile));
    }
    if ft.is_block_device() {
        return Ok(Some(FileType::BlockDevice));
    }
    if ft.is_fifo() {
        return Ok(Some(FileType::Pipe));
    }
    if ft.is_socket() {
        return Err(io::Error::new(
            io::ErrorKind::Other,
            format!("Is a socket: {}", path.display()),
        ));
    }
    Ok(None) // interpreted as unredirected/unpiped stdin/stdout/stderr
}

/// Returns `Ok(true)` if the file behind the file descriptor `fd` is a terminal, and `Ok(false)` if not.
fn is_a_terminal(fd: i32) -> io::Result<bool> {
    let exit_status = process::Command::new("test")
        .arg("-t")
        .arg(fd.to_string())
        .status()?;
    Ok(exit_status.success())
}

/// Returns `Ok(true)` if the file behind the file descriptor `fd` is probably redirected, and `Ok(false)` if not.
///
/// Note: If `fd` is not 0, 1 or 2 then the result is `Ok(false)`, independently from the real situation.
pub(crate) fn is_probably_redirected(fd: i32) -> io::Result<bool> {
    let is_std = matches!(fd, 0 | 1 | 2);
    if !is_std {
        return Ok(false);
    }
    let is_terminal = is_a_terminal(fd)?;
    let mut path = PathBuf::new();
    path.push(match fd {
        0 => "/dev/stdin",
        1 => "/dev/stdout",
        2 => "/dev/stderr",
        _ => unreachable!(),
    });
    let is_probably_a_pipe = matches!(get_file_type(&path), Ok(Some(FileType::Pipe)));
    Ok(!is_terminal && !is_probably_a_pipe)
}

/// Returns `Ok(true)` if the file given by `file_path` is seekable, and `Ok(false)` if not.
pub(crate) fn is_seekable_file(path: &PathBuf) -> io::Result<bool> {
    Ok(match get_file_type(path)? {
        Some(FileType::RegularFile) | Some(FileType::Directory) | Some(FileType::BlockDevice) => {
            true
        }
        Some(FileType::Pipe) | None => false,
    })
}

/// Returns the size of the special block device given by `device_path`.
fn get_block_device_size(device_path: &PathBuf) -> io::Result<u64> {
    let output = process::Command::new("blockdev")
        .arg("--getsize64")
        .arg(device_path)
        .output()?;

    io::stderr().write_all(&output.stderr).unwrap();
    if !output.status.success() {
        return Err(io::Error::new(
            io::ErrorKind::Other,
            format!("blockdev failed with {}", output.status),
        ));
    }
    let s = str::from_utf8(&output.stdout)
        .unwrap()
        .trim_end()
        .to_string();
    Ok(s.parse::<u64>().unwrap())
}

/// Returns the block size of the file given by `path`.
///
/// If the file is unredirected/unpiped stdin/stdout/stderr then `Ok(None)` is returned.
pub(crate) fn get_block_size(path: &PathBuf) -> io::Result<Option<u64>> {
    match get_file_type(path)? {
        Some(FileType::RegularFile) | Some(FileType::Directory) | Some(FileType::Pipe) => {
            Ok(Some(get_file_block_size(path)?))
        }
        Some(FileType::BlockDevice) => Ok(Some(get_block_device_block_size(path)?)),
        None => Ok(None),
    }
}

/// Returns the block size of the file given by `file_path`.
///
/// The file is assumed to be a regular file, a directory or a pipe.
fn get_file_block_size(file_path: &PathBuf) -> io::Result<u64> {
    let output = process::Command::new("stat")
        .arg("--file-system")
        .arg("--cached=never")
        .arg("--printf=%S")
        .arg(file_path)
        .output()?;

    io::stderr().write_all(&output.stderr).unwrap();
    if !output.status.success() {
        return Err(io::Error::new(
            io::ErrorKind::Other,
            format!("stat failed with {}", output.status),
        ));
    }
    let s = str::from_utf8(&output.stdout).unwrap().to_string();
    Ok(s.parse::<u64>().unwrap())
}

/// Returns the block size of the file given by `device_path`.
///
/// The file is assumed to be a special block device.
fn get_block_device_block_size(device_path: &PathBuf) -> io::Result<u64> {
    let output = process::Command::new("blockdev")
        .arg("--getbsz")
        .arg(device_path)
        .output()?;

    io::stderr().write_all(&output.stderr).unwrap();
    if !output.status.success() {
        return Err(io::Error::new(
            io::ErrorKind::Other,
            format!("blockdev failed with {}", output.status),
        ));
    }
    let s = str::from_utf8(&output.stdout)
        .unwrap()
        .trim_end()
        .to_string();
    Ok(s.parse::<u64>().unwrap())
}

/// Returns the optimal buffer size for the file given by `path`.
pub(crate) fn get_optimal_buf_size(path: &PathBuf) -> io::Result<u64> {
    match get_file_type(path)? {
        Some(FileType::RegularFile) | Some(FileType::Directory) | Some(FileType::Pipe) | None => {
            get_optimal_file_buf_size(path)
        }
        Some(FileType::BlockDevice) => get_optimal_block_device_buf_size(path),
    }
}

/// Returns the optimal buffer size for the file given by `file_path`.
///
/// The file is assumed to be a regular file, a directory or a pipe.
fn get_optimal_file_buf_size(file_path: &PathBuf) -> io::Result<u64> {
    let output = process::Command::new("stat")
        .arg("--cached=never")
        .arg("--printf=%o")
        .arg(file_path)
        .output()?;

    io::stderr().write_all(&output.stderr).unwrap();
    if !output.status.success() {
        return Err(io::Error::new(
            io::ErrorKind::Other,
            format!("stat failed with {}", output.status),
        ));
    }
    let s = str::from_utf8(&output.stdout).unwrap().to_string();
    Ok(s.parse::<u64>().unwrap())
}

/// Returns the optimal buffer size for the file given by `device_path`.
///
/// The file is assumed to be a special block device.
fn get_optimal_block_device_buf_size(device_path: &PathBuf) -> io::Result<u64> {
    let output = process::Command::new("blockdev")
        .arg("--getioopt")
        .arg(device_path)
        .output()?;

    io::stderr().write_all(&output.stderr).unwrap();
    if !output.status.success() {
        return Err(io::Error::new(
            io::ErrorKind::Other,
            format!("blockdev failed with {}", output.status),
        ));
    }
    let s = str::from_utf8(&output.stdout)
        .unwrap()
        .trim_end()
        .to_string();
    let val = s.parse::<u64>().unwrap();
    if val == 0 {
        Err(io::Error::new(
            io::ErrorKind::Other,
            "Could not determine optimal buffer size.".to_string(),
        ))
    } else {
        Ok(val)
    }
}

/// Returns the rounded up number of blocks of the given `bytes` with regard to `block_size`.
pub(crate) fn round_up_to_full_block(bytes: u64, block_size: u64) -> u64 {
    bytes / block_size + if bytes % block_size == 0 { 0 } else { 1 }
}
