/* ddpolymerase: Copy, repair and verify a file
 * Copyright (C) 2021-2023  Tobias Killer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! The writer using the program `dd`.

use std::ffi::OsString;
use std::io::{self, Write};
use std::path::PathBuf;
use std::process;

/// The state of the writer.
pub(crate) struct DdWriter<'a> {
    /// The output file path.
    ofile_path: &'a PathBuf,
    /// The output buffer size in bytes.
    obuf_size: u64,
    /// If the output file is seekable.
    is_seekable_file: bool,
    /// If stderr, inherited from `dd`, is to be suppressed.
    suppress_stderr: bool,
    /// The `dd` child process.
    child: Option<process::Child>,
    /// The stdin of the `dd` child process where to write to.
    writer: Option<process::ChildStdin>,
    /// If an end-of-file mark has been reached.
    eof_reached: bool,
    /// The current byte position within the file.
    curr_byte_pos: u64,
}

impl<'a> DdWriter<'a> {
    /// Creates a new writer.
    ///
    /// Parameters:
    /// - `ofile_path`: The output file path.
    /// - `obuf_size`: The output buffer size in bytes.
    /// - `is_seeekable_file`: If the output file is seekable.
    /// - `suppress_stderr`: If stderr, inherited from `dd`, is to be suppressed.
    pub(crate) fn new(
        ofile_path: &'a PathBuf,
        obuf_size: u64,
        is_seekable_file: bool,
        suppress_stderr: bool,
    ) -> DdWriter<'a> {
        DdWriter {
            ofile_path,
            obuf_size,
            is_seekable_file,
            suppress_stderr,
            child: None,
            writer: None,
            eof_reached: false,
            curr_byte_pos: 0,
        }
    }

    /// Restarts the writer, starting writing at file position `skip_bytes`.
    fn restart(&mut self, skip_bytes: u64) -> io::Result<()> {
        if self.obuf_size == 0 {
            return Err(io::Error::new(
                io::ErrorKind::Other,
                "Could not restart dd for writing",
            ));
        }

        self.terminate_child_process()?;

        // Hint: Since release 9.1 (2022-04-15) of coreutils, dd still accepts 'oflag=seek_bytes' but it is not documented anymore.
        // Instead of 'oflag=seek_bytes', one should append a 'B' to byte values.
        let mut arg_of: OsString = "of=".into();
        arg_of.push(self.ofile_path);
        let mut child = if self.is_seekable_file {
            process::Command::new("dd")
                .arg(arg_of)
                .arg(format!("bs={}", self.obuf_size))
                .arg(format!("seek={}", skip_bytes))
                .arg("conv=fdatasync,notrunc")
                .arg("iflag=fullblock")
                .arg("oflag=seek_bytes,nocache")
                .arg("status=none")
                .stdin(process::Stdio::piped())
                .stderr(if self.suppress_stderr {
                    process::Stdio::null()
                } else {
                    process::Stdio::inherit()
                })
                .spawn()?
        } else {
            process::Command::new("dd")
                .arg(arg_of)
                .arg(format!("bs={}", self.obuf_size))
                .arg("conv=notrunc")
                .arg("iflag=fullblock")
                .arg("oflag=nocache")
                .arg("status=none")
                .stdin(process::Stdio::piped())
                .stderr(if self.suppress_stderr {
                    process::Stdio::null()
                } else {
                    process::Stdio::inherit()
                })
                .spawn()?
        };
        let writer = child.stdin.take().unwrap();

        self.child = Some(child);
        self.writer = Some(writer);
        self.curr_byte_pos = skip_bytes;
        self.eof_reached = false;

        Ok(())
    }

    /// Writes up to `buf.len()` bytes from `buf`, starting at `start_pos` into the output file.
    ///
    /// Returns the number of actually written bytes.
    pub(crate) fn write(&mut self, buf: &[u8], start_pos: u64) -> io::Result<usize> {
        if self.child.is_none() || start_pos != self.curr_byte_pos {
            // Explicit termination of the child process is necessary
            // since `dd` could terminate unsuccessfully and thus
            // throw an error on the following self.restart()
            let _ = self.terminate_child_process();
            self.restart(start_pos)?;
        }
        if self.eof_reached {
            return Ok(0);
        }
        let mut from = 0;
        let to = buf.len();
        if let Some(ref mut writer) = self.writer {
            while from < to {
                let size_written = writer.write(&buf[from..to])?;
                self.curr_byte_pos += size_written as u64;
                if size_written == 0 {
                    self.eof_reached = true;
                    return Ok(from);
                }
                from += size_written;
            }
        }
        Ok(from)
    }

    /// Flushes the written bytes to the output file.
    ///
    /// Iff `terminate_first` is `true` then the child process is explicitly terminated before restarting.
    /// Thus, an unsuccessfully terminated child process will not disturb the restart.
    pub(crate) fn flush(&mut self, terminate_first: bool) -> io::Result<()> {
        if self.is_seekable_file {
            // Sometimes, explicit termination of the child process is necessary
            // since `dd` could terminate unsuccessfully and thus
            // throw an error on the following self.restart()
            if terminate_first {
                let _ = self.terminate_child_process();
            }
            // It seems that the only way to make `dd` flush its buffer
            // is to terminate the associated process.
            self.restart(self.curr_byte_pos)
        } else {
            Ok(())
        }
    }

    /// Terminates the child process.
    fn terminate_child_process(&mut self) -> io::Result<()> {
        if let Some(mut writer) = self.writer.take() {
            if writer.flush().is_err() {
                if let Some(mut child) = self.child.take() {
                    child.kill()?;
                }
            }
            // dropping `writer` closes the child's stdin
        }
        if let Some(mut child) = self.child.take() {
            let status = child.wait()?;
            if !status.success() {
                return Err(io::Error::new(
                    io::ErrorKind::Other,
                    if let Some(code) = status.code() {
                        format!("Writing dd command exited with exit status {}.", code)
                    } else {
                        "Writing dd command terminated by signal.".to_string()
                    },
                ));
            }
        }
        Ok(())
    }
}

impl<'a> Drop for DdWriter<'a> {
    fn drop(&mut self) {
        let _ = self.terminate_child_process();
    }
}
