/* ddpolymerase: Copy, repair and verify a file
 * Copyright (C) 2021-2023  Tobias Killer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! Everything buffer-related.

/// A buffer (excerpt of a file).
pub(crate) struct Buffer {
    /// The buffer itself.
    mem: Vec<u8>,
    /// The block size associated with this buffer.
    block_size: u64,
    /// The number of the first block in [`Self::mem`].
    block_offset: u64,
}

impl Buffer {
    /// Creates a new zeroized `Buffer` with block size `block_size` and of `buffer_size_in_blocks` many blocks.
    pub(crate) fn new(block_size: u64, buffer_size_in_blocks: u64) -> Buffer {
        let size = (block_size * buffer_size_in_blocks) as usize;
        let mem = vec![0; size];
        Buffer {
            mem,
            block_size,
            block_offset: 0,
        }
    }

    /// Returns the buffer as a slice.
    pub(crate) fn buf(&mut self) -> &mut [u8] {
        &mut self.mem[..]
    }

    /// Sets the block offset to `new_block_offset`.
    pub(crate) fn set_block_offset(&mut self, new_block_offset: u64) {
        self.block_offset = new_block_offset;
    }

    /// Reads a slice of `byte_count` bytes of the `Buffer` from block position `from_block` based on [`Self::block_offset`].
    pub(crate) fn read(&mut self, from_block: u64, byte_count: u64) -> &mut [u8] {
        let whole_block_count = byte_count / self.block_size;

        let from = from_block - self.block_offset;
        let to = from + whole_block_count;

        let from = (from * self.block_size) as usize;
        let to = (to * self.block_size + byte_count % self.block_size) as usize;

        &mut self.mem[from..to]
    }
}
