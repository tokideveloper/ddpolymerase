/* ddpolymerase: Copy, repair and verify a file
 * Copyright (C) 2021-2023  Tobias Killer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! The reader using the program `dd`.

use std::ffi::OsString;
use std::io::{self, Read};
use std::path::PathBuf;
use std::process;

/// The state of the reader.
pub(crate) struct DdReader<'a> {
    /// The input file path.
    ifile_path: &'a PathBuf,
    /// The input buffer size in bytes.
    ibuf_size: u64,
    /// If the `dd` option `nocache` is to be used.
    nocache: bool,
    /// If stderr, inherited from `dd`, is to be suppressed.
    suppress_stderr: bool,
    /// The `dd` child process.
    child: Option<process::Child>,
    /// The stdout of the `dd` child process where to read from.
    reader: Option<process::ChildStdout>,
    /// If an end-of-file mark has been reached.
    eof_reached: bool,
    /// The current byte position within the file.
    curr_byte_pos: u64,
}

impl<'a> DdReader<'a> {
    /// Creates a new reader.
    ///
    /// Parameters:
    /// - `ifile_path`: The input file path.
    /// - `ibuf_size`: The input buffer size in bytes.
    /// - `nocache`: If the `dd` option `nocache` is to be used.
    /// - `suppress_stderr`: If stderr, inherited from `dd`, is to be suppressed.
    pub(crate) fn new(
        ifile_path: &'a PathBuf,
        ibuf_size: u64,
        nocache: bool,
        suppress_stderr: bool,
    ) -> DdReader<'a> {
        DdReader {
            ifile_path,
            ibuf_size,
            nocache,
            suppress_stderr,
            child: None,
            reader: None,
            eof_reached: false,
            curr_byte_pos: 0,
        }
    }

    /// Restarts the reader, starting reading at file position `skip_bytes`.
    fn restart(&mut self, skip_bytes: u64) -> io::Result<()> {
        // Hint: Since release 9.1 (2022-04-15) of coreutils, dd still accepts 'iflag=skip_bytes' but it is not documented anymore.
        // Instead of 'iflag=skip_bytes', one should append a 'B' to byte values.
        let mut arg_if: OsString = "if=".into();
        arg_if.push(self.ifile_path);
        let mut child = process::Command::new("dd")
            .arg(arg_if)
            .arg(format!("bs={}", self.ibuf_size))
            .arg(format!("skip={}", skip_bytes))
            .arg(if self.nocache {
                "iflag=nocache,skip_bytes,fullblock"
            } else {
                "iflag=skip_bytes,fullblock"
            })
            .arg("status=none")
            .stdout(process::Stdio::piped())
            .stderr(if self.suppress_stderr {
                process::Stdio::null()
            } else {
                process::Stdio::inherit()
            })
            .spawn()?;
        let reader = child.stdout.take().unwrap();

        self.child = Some(child);
        self.reader = Some(reader);
        self.eof_reached = false;
        self.curr_byte_pos = skip_bytes;

        Ok(())
    }

    /// Reads up to `buf.len()` bytes into `buf`, starting at `start_pos` from the input file.
    ///
    /// Returns the number of actually read bytes.
    pub(crate) fn read(&mut self, buf: &mut [u8], start_pos: u64) -> io::Result<usize> {
        if self.child.is_none() || start_pos != self.curr_byte_pos {
            // Explicit termination of the child process is necessary
            // since `dd` could terminate unsuccessfully and thus
            // throw an error on the following self.restart()
            let _ = self.terminate_child_process();
            self.restart(start_pos)?;
        }
        if self.eof_reached {
            return Ok(0);
        }
        let mut from = 0;
        let to = buf.len();
        if let Some(ref mut reader) = self.reader {
            while from < to {
                let size_read = reader.read(&mut buf[from..to])?;
                self.curr_byte_pos += size_read as u64;
                if size_read == 0 {
                    self.eof_reached = true;
                    self.terminate_child_process()?;
                    return Ok(from);
                }
                from += size_read;
            }
        }
        Ok(from)
    }

    /// Terminates the child process.
    fn terminate_child_process(&mut self) -> io::Result<()> {
        if let Some(_reader) = self.reader.take() {
            // dropping `_reader` closes the child's stdout
        }
        if let Some(mut child) = self.child.take() {
            let status = child.wait()?;
            if !status.success() {
                return Err(io::Error::new(
                    io::ErrorKind::Other,
                    if let Some(code) = status.code() {
                        format!("Reading dd command exited with exit status {}.", code)
                    } else {
                        "Reading dd command terminated by signal.".to_string()
                    },
                ));
            }
        }
        Ok(())
    }
}

impl<'a> Drop for DdReader<'a> {
    fn drop(&mut self) {
        let _ = self.terminate_child_process();
    }
}
