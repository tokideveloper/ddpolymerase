/* ddpolymerase: Copy, repair and verify a file
 * Copyright (C) 2021-2023  Tobias Killer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! The block log file writer.

use std::fs;
use std::io::{self, Write};

/// Loggable block kinds.
#[derive(Copy, Clone, Debug, PartialEq)]
pub(crate) enum BlockKind {
    Good,
    Copied,
    Fixed,
    Bad,
    SourceError,
    DestinationError,
}

/// Represents the state of the block log file writer.
pub(crate) struct BlockLogFileWriter {
    /// The file where to write the logs to.
    file: fs::File,
    /// The position of the first block (inclusive) of the range of a held back block log entry.
    from_block_pos: Option<u64>,
    /// The position of the last block (exclusive) of the range of a held back block log entry.
    to_block_pos: Option<u64>,
    /// The kind of block of a held back block log entry.
    block_kind: Option<BlockKind>,
}

impl BlockLogFileWriter {
    /// Creates a new block log file writer writing to `file`.
    pub(crate) fn new(file: fs::File) -> BlockLogFileWriter {
        BlockLogFileWriter {
            file,
            from_block_pos: None,
            to_block_pos: None,
            block_kind: None,
        }
    }

    /// Logs a pass heading line.
    ///
    /// The line consists of the number of the pass (`pass`),
    /// the source file length in bytes (`source_file_length`),
    /// the source file length in blocks (`source_block_count`)
    /// and the block size in bytes.
    ///
    /// The format of the line is `P <pass> <source_file_length> <source_block_count> <block_size>`.
    ///
    /// If `source_file_length` is `None`, a question mark (`?`) will appear instead.
    /// If `source_block_count` is `None`, a question mark (`?`) will appear instead.
    pub(crate) fn write_pass_line(
        &mut self,
        pass: usize,
        source_file_length: Option<u64>,
        source_block_count: Option<u64>,
        block_size: u64,
    ) -> io::Result<()> {
        self.flush()?;
        writeln!(
            self.file,
            "P {} {} {} {}",
            pass,
            match source_file_length {
                None => "?".to_string(),
                Some(len) => len.to_string(),
            },
            match source_block_count {
                None => "?".to_string(),
                Some(c) => c.to_string(),
            },
            block_size
        )?;
        Ok(())
    }

    /// Logs blocks of a certain kind (`block_kind`) and range (from `from_block_pos` (inclusive) to `to_block_pos` (exclusive)).
    ///
    /// The actual writing is held back until a [`BlockLogFileWriter::flush()`] occurs or when this log is not consecutive to the previous log.
    /// If the log is consecutive to the previous log, the ranges are composed together.
    pub(crate) fn write_block_positions(
        &mut self,
        from_block_pos: u64,
        to_block_pos: u64,
        block_kind: BlockKind,
    ) -> io::Result<()> {
        if to_block_pos <= from_block_pos {
            return Ok(());
        }
        // flush if necessary
        if let Some(self_block_kind) = self.block_kind {
            if block_kind != self_block_kind {
                self.flush()?;
            }
        }
        if let Some(self_to_block_pos) = self.to_block_pos {
            if self_to_block_pos < from_block_pos {
                self.flush()?;
            }
        }
        if let Some(self_from_block_pos) = self.from_block_pos {
            if self_from_block_pos >= to_block_pos {
                self.flush()?;
            }
        }
        // summarize
        if self.from_block_pos.is_none() {
            self.from_block_pos = Some(from_block_pos);
            self.block_kind = Some(block_kind);
        }
        self.to_block_pos = Some(to_block_pos);
        Ok(())
    }

    /// Actually writes a block range log to [`BlockLogFileWriter::file`].
    ///
    /// The logged blocks are of kind `block_kind` and ranges from block `from` (inclusive) to block `to` (exclusive).
    ///
    /// The format of the line is `<block_kind> <from> <to - 1> <to - from>`
    /// where `<block_kind>` is
    /// - `g` for [`BlockKind::Good`],
    /// - `c` for [`BlockKind::Copied`],
    /// - `r` for [`BlockKind::Fixed`],
    /// - `b` for [`BlockKind::Bad`],
    /// - `s` for [`BlockKind::SourceError`],
    /// - `d` for [`BlockKind::DestinationError`].
    fn write_blocks(&mut self, block_kind: BlockKind, from: u64, to: u64) -> io::Result<()> {
        let c = to - from;
        if c > 0 {
            let block_kind_char = match block_kind {
                BlockKind::Good => 'g',
                BlockKind::Copied => 'c',
                BlockKind::Fixed => 'r',
                BlockKind::Bad => 'b',
                BlockKind::SourceError => 's',
                BlockKind::DestinationError => 'd',
            };
            writeln!(self.file, "{} {} {} {}", block_kind_char, from, to - 1, c)?;
        }
        Ok(())
    }

    /// Flushes a held back block range.
    pub(crate) fn flush(&mut self) -> io::Result<()> {
        if self.from_block_pos.is_some() {
            self.write_blocks(
                self.block_kind.unwrap(),
                self.from_block_pos.unwrap(),
                self.to_block_pos.unwrap(),
            )?;
            self.from_block_pos = None;
            self.to_block_pos = None;
            self.block_kind = None;
        }
        Ok(())
    }
}

impl Drop for BlockLogFileWriter {
    fn drop(&mut self) {
        let _ = self.flush();
    }
}
