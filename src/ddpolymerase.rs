/* ddpolymerase: Copy, repair and verify a file
 * Copyright (C) 2021-2023  Tobias Killer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! The main algorithm.

extern crate termion;

use crate::block_log_file_writer::{BlockKind, BlockLogFileWriter};
use crate::buffer;
use crate::dd_reader;
use crate::dd_writer;
use crate::progress_bar::{self, ProgressBarElement};
use crate::utils;
use crate::{OnDestinationErrorMode, OnSourceErrorMode};

use std::cmp;
use std::convert::TryInto;
use std::ffi::OsString;
use std::fs;
use std::io::{self, Write};
use std::path::{Path, PathBuf};
use std::process;

/// A range to repair.
#[derive(Clone, Debug)]
struct RangeToFix {
    /// The position of the first block (inclusive) to repair.
    from_block: u64,
    /// The position of the last block (exclusive) to repair.
    to_block: u64,
    /// The number of remaining tries for repairing.
    remaining_tries: usize,
}

/// The exit status of a single pass.
pub(crate) struct PassExitStatus {
    /// There are repaired blocks in the destination file.
    pub(crate) fixed_but_not_verified: bool,
    /// The file lengths differ at termination of the pass.
    pub(crate) different_file_lengths: bool,
    /// There are bad blocks in the destination file.
    pub(crate) bad_blocks_left: bool,
    /// There were read or write errors during the pass.
    pub(crate) read_write_errors: bool,
}

/// The state of the machine.
pub(crate) struct DdPolymerase<'a> {
    /// The path of the source file.
    source_file_path: &'a PathBuf,
    /// The path of the destination file.
    dest_file_path: &'a PathBuf,
    /// The initial length of the source file if known.
    source_file_length: Option<u64>,
    /// The initial length of the destination file if known.
    dest_file_length: Option<u64>,
    /// The size of a block in bytes.
    block_size: u64,
    /// The size of the input buffer in blocks.
    ibuf_size_in_blocks: u64,
    /// The size of the output buffer in blocks.
    obuf_size_in_blocks: u64,
    /// The maximum number of tries for repairing a single block.
    max_tries: usize,
    /// If the machine stops immediately after detecting a bad block.
    abort_on_bad_block: bool,
    /// The mode how to handle source errors.
    on_source_error_mode: &'a OnSourceErrorMode,
    /// The mode how to handle destination errors.
    on_dest_error_mode: &'a OnDestinationErrorMode,
    /// `true` iff global verification is disabled.
    no_global_ver: bool,
    /// `true` iff local verification is disabled.
    no_local_ver: bool,
    /// Where to log results of block processing.
    block_log_file_writer: &'a mut Option<BlockLogFileWriter>,
    /// `true` iff showing the progress bar during a pass is disabled.
    quiet: bool,
    /// The expected maximum number of passes.
    max_passes: usize,
    /// The number of the current pass (starting at 1).
    curr_pass: usize,

    /// The initial number of blocks in the source file if known.
    source_block_count: Option<u64>,
    /// The initial number of blocks in the destination file if known.
    dest_block_count: Option<u64>,
    /// `true` iff the destination file did not exist before creating this machine.
    dest_file_created: bool,

    /// Counter counting the number of copied blocks.
    copied_block_count: u64,
    /// Counter counting the number of repaired blocks.
    fixed_block_count: u64,
    /// Counter counting the number of bad blocks.
    bad_block_count: u64,
    /// Counter counting the number of source error blocks.
    source_error_block_count: u64,
    /// Counter counting the number of destination error blocks.
    dest_error_block_count: u64,

    /// The number of bytes read from source.
    effective_source_file_length: u64,

    /// `true` iff the destination file is truncatable.
    is_truncatable_dest: bool,
    /// `true` iff truncation of the destination file to an empty one is allowed.
    is_truncation_to_empty_file_allowed: bool,

    /// The progress bar.
    pb: Option<progress_bar::ProgressBar>,
}

impl<'a> DdPolymerase<'a> {
    /// Creates a new machine.
    ///
    /// Parameters:
    /// - `source_file_path`: The source file path.
    /// - `dest_file_path`: The destination file path.
    /// - `source_file_length`: The source file length if known.
    /// - `block_size`: The size of a block in bytes.
    /// - `ibuf_size_in_blocks`: The size of the input buffer in blocks.
    /// - `obuf_size_in_blocks`: The size of the output buffer in blocks.
    /// - `max_tries`: The maximum number of tries for repairing a single block.
    /// - `abort_on_bad_block`: If the machine has to stop immediately on detecting a bad block.
    /// - `on_source_error_mode`: The mode how to handle source errors.
    /// - `on_dest_error_mode`: The mode how to handle destination errors.
    /// - `block_log_file_writer`: The logger.
    /// - `no_global_ver`: If global verification is disabled.
    /// - `no_local_ver`: If local verification is disabled.
    /// - `is_truncatable_dest`: If the destination file is truncatable.
    /// - `quiet`: If showing the progress bar is disabled.
    /// - `max_passes`: The expected maximum number of passes.
    /// - `curr_pass`: The number of the current pass (starting at 1).
    /// - `term_width`: The width of the terminal in characters.
    /// - `stats_unit`: The unit to be used in the statistics below the progress bar.
    /// - `use_colors`: If to use colors.
    /// - `is_truncation_to_empty_file_allowed`: If truncation of the destination file to an empty one is allowed.
    #[allow(clippy::too_many_arguments)]
    pub(crate) fn new(
        source_file_path: &'a PathBuf,
        dest_file_path: &'a PathBuf,
        source_file_length: Option<u64>,
        block_size: u64,
        ibuf_size_in_blocks: u64,
        obuf_size_in_blocks: u64,
        max_tries: usize,
        abort_on_bad_block: bool,
        on_source_error_mode: &'a OnSourceErrorMode,
        on_dest_error_mode: &'a OnDestinationErrorMode,
        block_log_file_writer: &'a mut Option<BlockLogFileWriter>,
        no_global_ver: bool,
        no_local_ver: bool,
        is_truncatable_dest: bool,
        quiet: bool,
        max_passes: usize,
        curr_pass: usize,
        term_width: u16,
        stats_unit: progress_bar::StatsUnit,
        use_colors: bool,
        is_truncation_to_empty_file_allowed: bool,
    ) -> io::Result<DdPolymerase<'a>> {
        // create destination file if necessary
        let dest_path = Path::new(dest_file_path);
        let dest_file_created = !dest_path.exists();
        if dest_file_created {
            fs::File::create(dest_file_path)?;
        }
        // get length of destination file
        let dest_file_length = utils::get_file_size(dest_file_path)?;

        // calcs
        let source_block_count =
            source_file_length.map(|len| utils::round_up_to_full_block(len, block_size));
        let dest_block_count =
            dest_file_length.map(|len| utils::round_up_to_full_block(len, block_size));

        // init progress bar
        let show_source_errors = match on_source_error_mode {
            OnSourceErrorMode::Abort => false,
            OnSourceErrorMode::Skip => true,
            OnSourceErrorMode::Zeroize => true,
        };
        let show_dest_errors = match on_dest_error_mode {
            OnDestinationErrorMode::Abort => false,
            OnDestinationErrorMode::Skip => true,
        };
        let pb = if quiet {
            None
        } else {
            Some(progress_bar::ProgressBar::new(
                block_size,
                source_block_count,
                max_passes,
                curr_pass,
                term_width,
                stats_unit,
                use_colors,
                show_source_errors,
                show_dest_errors,
            ))
        };

        Ok(DdPolymerase {
            source_file_path,
            dest_file_path,
            block_size,
            ibuf_size_in_blocks,
            obuf_size_in_blocks,
            max_tries,
            abort_on_bad_block,
            on_source_error_mode,
            on_dest_error_mode,
            no_global_ver,
            no_local_ver,
            block_log_file_writer,
            quiet,
            max_passes,
            curr_pass,
            source_file_length,
            dest_file_length,
            source_block_count,
            dest_block_count,
            dest_file_created,
            copied_block_count: 0,
            fixed_block_count: 0,
            bad_block_count: 0,
            source_error_block_count: 0,
            dest_error_block_count: 0,
            effective_source_file_length: 0,
            is_truncatable_dest,
            is_truncation_to_empty_file_allowed,
            pb,
        })
    }

    /// Runs the machine for one pass.
    pub(crate) fn pass(mut self) -> io::Result<PassExitStatus> {
        // init state
        let mut there_was_something_to_fix = false;
        let mut byte_pos = 0u64;
        let mut block_pos = 0u64;

        // write pass number to block file
        if let Some(block_log_file_writer) = self.block_log_file_writer {
            block_log_file_writer.write_pass_line(
                self.curr_pass,
                self.source_file_length,
                self.source_block_count,
                self.block_size,
            )?;
        }

        // init progress bar
        if let Some(ref mut pb) = self.pb {
            pb.draw();
        }

        // init readers
        let ibuf_size = self.block_size * self.ibuf_size_in_blocks;
        let mut source_dd_reader = dd_reader::DdReader::new(
            self.source_file_path,
            ibuf_size,
            false,
            self.is_stderr_to_suppress_for_source(),
        );
        let mut dest_dd_reader = dd_reader::DdReader::new(
            self.dest_file_path,
            ibuf_size,
            true,
            self.is_stderr_to_suppress_for_dest(),
        );

        // init writer
        let obuf_size = self.block_size * self.obuf_size_in_blocks;
        let mut dest_dd_writer = dd_writer::DdWriter::new(
            self.dest_file_path,
            obuf_size,
            utils::is_seekable_file(self.dest_file_path)?,
            self.is_stderr_to_suppress_for_dest(),
        );

        // init buffers
        let mut source_buf = buffer::Buffer::new(self.block_size, self.ibuf_size_in_blocks);
        let mut dest_buf = buffer::Buffer::new(self.block_size, self.ibuf_size_in_blocks); // sic!

        // main loop
        while match self.source_file_length {
            Some(len) => byte_pos < len,
            None => true,
        } {
            if let Some(ref mut pb) = self.pb {
                pb.tick();
            }

            // read ibuf
            let (source_bytes_read, skip_because_of_read_err, mark_as_source_error) = self
                .read_from_source(
                    &mut source_dd_reader,
                    &mut dest_dd_writer,
                    &mut source_buf,
                    byte_pos,
                    ibuf_size,
                )?;

            if mark_as_source_error {
                // mark as source error
                let source_blocks_read =
                    utils::round_up_to_full_block(source_bytes_read as u64, self.block_size);
                let from_block = block_pos;
                let to_block = block_pos + source_blocks_read;
                self.source_error_block_count += source_blocks_read;
                if let Some(ref mut pb) = self.pb {
                    pb.set_plus_one_path(self.curr_pass < self.max_passes);
                    pb.add_source_error_blocks(source_blocks_read);
                    pb.set_elems(from_block, to_block, ProgressBarElement::SourceError);
                    pb.tick();
                }
                if let Some(ref mut block_log_file_writer) = self.block_log_file_writer {
                    block_log_file_writer.write_block_positions(
                        from_block,
                        to_block,
                        BlockKind::SourceError,
                    )?;
                }
            }

            // after reading from source
            self.effective_source_file_length += source_bytes_read as u64;
            let source_blocks_read =
                utils::round_up_to_full_block(source_bytes_read as u64, self.block_size);
            if let Some(ref mut pb) = self.pb {
                pb.add_read_blocks(source_blocks_read);
            }
            let source_eof_reached = source_bytes_read == 0;
            if let Some(len) = self.source_file_length {
                if source_eof_reached && byte_pos < len {
                    // early source EOF
                    return Err(io::Error::new(
                        io::ErrorKind::Other,
                        "Unexpected source EOF.".to_string(),
                    ));
                }
            }
            if source_eof_reached {
                break;
            }

            // from_block and to_block
            let from_block = block_pos;
            let to_block = if self.source_block_count.is_some()
                && block_pos + self.ibuf_size_in_blocks > self.source_block_count.unwrap()
            {
                self.source_block_count.unwrap()
            } else {
                from_block + source_blocks_read
            };

            if !skip_because_of_read_err {
                // mark as pending
                if let Some(ref mut pb) = self.pb {
                    pb.set_pos(to_block - 1);
                    pb.set_elems(from_block, to_block, ProgressBarElement::Pending);
                    pb.tick();
                }

                // padding
                source_buf.buf()[source_bytes_read..].fill(0);
                //
                source_buf.set_block_offset(from_block);

                // read from destination
                let max_dest_bytes_to_read = source_bytes_read;
                let (_dest_bytes_read, skip_because_of_dest_read_err) = if !self.no_global_ver {
                    let dest_bytes_read = dest_dd_reader
                        .read(&mut dest_buf.buf()[..max_dest_bytes_to_read], byte_pos);
                    match dest_bytes_read {
                        Ok(n) => (n, false),
                        Err(e) => match self.on_dest_error_mode {
                            OnDestinationErrorMode::Abort => {
                                return Err(e);
                            }
                            OnDestinationErrorMode::Skip => (0, true),
                        },
                    }
                } else {
                    (0, false)
                };

                if skip_because_of_dest_read_err {
                    // skip because of a destination read error
                    let dest_read_error_ranges = vec![RangeToFix {
                        from_block,
                        to_block,
                        remaining_tries: 0,
                    }];
                    self.dest_error_block_count += to_block - from_block;
                    if let Some(ref mut pb) = self.pb {
                        pb.set_plus_one_path(self.curr_pass < self.max_passes);
                        pb.add_dest_error_blocks(to_block - from_block);
                    }
                    self.mark_as(
                        &dest_read_error_ranges,
                        ProgressBarElement::DestinationError,
                    );
                    if let Some(ref mut block_log_file_writer) = self.block_log_file_writer {
                        block_log_file_writer.write_block_positions(
                            from_block,
                            to_block,
                            BlockKind::DestinationError,
                        )?;
                    }
                } else {
                    // compare blocks in ibuf (global verification)
                    let vector_to_fix = if self.no_global_ver {
                        vec![RangeToFix {
                            from_block,
                            to_block,
                            remaining_tries: self.max_tries,
                        }]
                    } else {
                        self.cmp_blocks(
                            source_buf.buf(),
                            dest_buf.buf(),
                            from_block,
                            self.max_tries,
                            true,
                        )
                    };
                    let vector_to_fix = self.slice_to_obuf_size_and_reverse(vector_to_fix);

                    let good_ranges =
                        DdPolymerase::reverse_ranges(from_block, to_block, &vector_to_fix, 0);
                    self.mark_as(&good_ranges, ProgressBarElement::Good);
                    if let Some(ref mut block_log_file_writer) = self.block_log_file_writer {
                        for range in &good_ranges {
                            block_log_file_writer.write_block_positions(
                                range.from_block,
                                range.to_block,
                                BlockKind::Good,
                            )?;
                        }
                    }

                    // fix range if found
                    if !vector_to_fix.is_empty() {
                        there_was_something_to_fix = true;
                        let no_bad_blocks = self.try_fix_ranges(
                            vector_to_fix,
                            &mut source_buf,
                            &mut dest_dd_writer,
                        )?;

                        // abort on bad block
                        if self.abort_on_bad_block && !no_bad_blocks {
                            // abort
                            if let Some(ref mut pb) = self.pb {
                                pb.set_pos_to_max();
                                pb.tick();
                                pb.finish();
                            }
                            if !self.quiet {
                                eprintln!();
                            }
                            return self.create_pass_exit_status();
                        }
                    }
                }
            }

            // next block
            block_pos = to_block;
            byte_pos += source_bytes_read as u64;
            if let Some(ref mut pb) = self.pb {
                pb.set_pos(block_pos);
            }
        }
        if let Some(ref mut pb) = self.pb {
            pb.set_pos_to_max();
        }
        // flush
        match dest_dd_writer.flush(true) {
            Ok(()) => {}
            Err(_) => {
                return Err(io::Error::new(
                    io::ErrorKind::Other,
                    "Error on flushing, probably due to I/O error, missing permissions or no space left on file system or device.".to_string(),
                ));
            }
        }
        // fix file length
        if !self.is_truncation_to_empty_file_allowed && self.effective_source_file_length == 0 {
            return Err(io::Error::new(
                io::ErrorKind::Other,
                "Effective source file length is 0. Use -f to proceed anyway.".to_string(),
            ));
        }
        if let Some(dest_file_length) = self.dest_file_length {
            if self.is_truncatable_dest
                && self.max_tries > 0
                && (self.effective_source_file_length != dest_file_length
                    || there_was_something_to_fix)
            {
                self.dd_fix_file_len()?;
                there_was_something_to_fix = true;
            }
        }
        // sync if necessary
        if ((self.max_tries > 0 && there_was_something_to_fix) || self.dest_file_created)
            && utils::is_seekable_file(self.dest_file_path)?
        {
            fs::File::open(self.dest_file_path)?.sync_all()?;
        }
        // finish
        if let Some(ref mut pb) = self.pb {
            pb.tick();
            pb.finish();
        }
        if !self.quiet {
            eprintln!();
        }

        self.create_pass_exit_status()
    }

    /// Reads from source.
    ///
    /// Parameters:
    /// - `source_dd_reader`: The source reader to use.
    /// - `dest_dd_writer`: The used destination writer.
    /// - `source_buf`: The buffer where to write the read bytes to.
    /// - `byte_pos`: The byte position where to start reading from source.
    /// - `ibuf_size`: The size of the input buffer in bytes.
    ///
    /// The `Ok`-ish result is a triple:
    /// 1. The number of source bytes read.
    /// 2. `true` iff the further processing on the affected range has to be skipped due to read error.
    /// 3. `true` iff the affected range has to be marked as source error.
    fn read_from_source(
        &self,
        source_dd_reader: &mut dd_reader::DdReader,
        dest_dd_writer: &mut dd_writer::DdWriter,
        source_buf: &mut buffer::Buffer,
        byte_pos: u64,
        ibuf_size: u64,
    ) -> io::Result<(usize, bool, bool)> {
        match source_dd_reader.read(source_buf.buf(), byte_pos) {
            Ok(source_bytes_read) => Ok((source_bytes_read, false, false)),
            Err(e) => match self.on_source_error_mode {
                OnSourceErrorMode::Abort => Err(e),
                OnSourceErrorMode::Skip => {
                    self.on_source_error_mode_skip(dest_dd_writer, source_buf, byte_pos, ibuf_size)
                }
                OnSourceErrorMode::Zeroize => {
                    let source_bytes_zeroized =
                        self.calc_source_bytes_touched(byte_pos, source_buf.buf().len());
                    source_buf.buf()[..source_bytes_zeroized].fill(0);
                    Ok((source_bytes_zeroized, false, true))
                }
            },
        }
    }

    /// Handler for [`OnSourceErrorMode::Skip`].
    ///
    /// Parameters:
    /// - `dest_dd_writer`: The used destination writer.
    /// - `source_buf`: The buffer where to write the read bytes to.
    /// - `byte_pos`: The byte position where to start reading from source.
    /// - `ibuf_size`: The size of the input buffer in bytes.
    ///
    /// The `Ok`-ish result is a triple:
    /// 1. The number of source bytes read.
    /// 2. `true` iff the further processing on the affected range has to be skipped due to read error.
    /// 3. `true` iff the affected range has to be marked as source error.
    fn on_source_error_mode_skip(
        &self,
        dest_dd_writer: &mut dd_writer::DdWriter,
        source_buf: &mut buffer::Buffer,
        byte_pos: u64,
        ibuf_size: u64,
    ) -> io::Result<(usize, bool, bool)> {
        // make sure the copied bytes have been written
        match dest_dd_writer.flush(true) {
            Ok(()) => {}
            Err(_) => {
                return Err(io::Error::new(
                    io::ErrorKind::Other,
                    "Error on flushing, probably due to I/O error, missing permissions or no space left on file system or device.".to_string(),
                ));
            }
        }

        // we can safely unwrap here, because we checked in advance that dest is seekable
        let curr_dest_file_length = utils::get_file_size(self.dest_file_path)?.unwrap();

        // In case of regular destination files, we assume that the destination file grows with the (first) run.
        // So, we do not need to check the case `curr_dest_file_length < byte_pos` where we should at first
        // append NUL bytes to the dest file in order to match file sizes while growing.
        // However, for destination block devices, `curr_dest_file_length < byte_pos` could be true, so check this case.
        if curr_dest_file_length < byte_pos {
            // case 1: dest file much too short, so: Error!
            Err(io::Error::new(
                io::ErrorKind::Other,
                "Destination file did not grow as expected.".to_string(),
            ))
        } else if curr_dest_file_length >= byte_pos + source_buf.buf().len() as u64 {
            // case 2: dest file is long enough, so: skip buffer
            let source_bytes_skipped =
                self.calc_source_bytes_touched(byte_pos, source_buf.buf().len());
            Ok((source_bytes_skipped, true, true))
        } else {
            // case 3: dest file is too short, so: read from dest, append NUL bytes if necessary and write the result to dest
            let source_bytes_touched =
                self.calc_source_bytes_touched(byte_pos, source_buf.buf().len());
            let dest_bytes_touched = (curr_dest_file_length - byte_pos) as usize;
            let common_bytes_touched = cmp::min(source_bytes_touched, dest_bytes_touched);
            let nul_bytes_to_append = source_bytes_touched - common_bytes_touched;
            if nul_bytes_to_append > 0 {
                let skip = if common_bytes_touched > 0 {
                    // we may read from dest here, because we checked in advance that the dest is seekable
                    let mut temp_dest_dd_reader =
                        dd_reader::DdReader::new(self.dest_file_path, ibuf_size, true, true);
                    match temp_dest_dd_reader
                        .read(&mut source_buf.buf()[..common_bytes_touched], byte_pos)
                    {
                        Ok(dest_bytes_read) => {
                            if dest_bytes_read != common_bytes_touched {
                                // unexpected file length
                                return Err(io::Error::new(
                                    io::ErrorKind::Other,
                                    "Unexpected destination file length.".to_string(),
                                ));
                            }
                            false
                        }
                        Err(e) => match self.on_dest_error_mode {
                            OnDestinationErrorMode::Abort => {
                                return Err(e);
                            }
                            OnDestinationErrorMode::Skip => true,
                        },
                    }
                } else {
                    false
                };
                if !skip {
                    source_buf.buf()
                        [common_bytes_touched..(common_bytes_touched + nul_bytes_to_append)]
                        .fill(0);
                }
                Ok((source_bytes_touched, skip, true))
            } else {
                Ok((source_bytes_touched, true, true))
            }
        }
    }

    /// Tries to repair certain ranges.
    ///
    /// Parameters:
    /// - `vector_to_fix`: The ranges to repair.
    /// - `source_buf`: The source buffer containing the bytes from source.
    /// - `dest_dd_writer`: The writer to use.
    ///
    /// Returns
    /// - `Ok(false)` if bad blocks has been detected.
    /// - `Ok(true)` if no bad blocks has been detected.
    /// - `Err(...)` if an error occurred while repairing.
    fn try_fix_ranges(
        &mut self,
        mut vector_to_fix: Vec<RangeToFix>,
        source_buf: &mut buffer::Buffer,
        dest_dd_writer: &mut dd_writer::DdWriter,
    ) -> io::Result<bool> {
        while let Some(range_to_fix) = vector_to_fix.pop() {
            let (mut new_vector_to_fix, no_bad_blocks) =
                self.try_fix_blocks(&range_to_fix, source_buf, dest_dd_writer)?;
            if self.abort_on_bad_block && !no_bad_blocks {
                return Ok(false);
            }
            while let Some(vec_elem) = new_vector_to_fix.pop() {
                vector_to_fix.push(vec_elem);
            }
        }

        Ok(true)
    }

    /// Tries to repair a certain range of blocks.
    ///
    /// Parameters:
    /// - `range_to_fix`: The range to repair.
    /// - `source_buf`: The source buffer containing the bytes from source.
    /// - `dest_dd_writer`: The writer to use.
    ///
    /// Returns
    /// - `Ok((new_vector_to_fix, no_bad_blocks))` where
    ///   - `new_vector_to_fix` contains the remaining ranges to fix with decremented [`RangeToFix::remaining_tries`] by 1.
    ///   - `no_bad_blocks` is `true` iff no bad blocks have been detected during repair.
    /// - `Err(...)` if an error occurred while repairing.
    fn try_fix_blocks(
        &mut self,
        range_to_fix: &RangeToFix,
        source_buf: &mut buffer::Buffer,
        dest_dd_writer: &mut dd_writer::DdWriter,
    ) -> io::Result<(Vec<RangeToFix>, bool)> {
        let block_count = range_to_fix.to_block - range_to_fix.from_block;
        if range_to_fix.remaining_tries == 0 {
            // mark blocks as bad
            if let Some(ref mut pb) = self.pb {
                pb.set_plus_one_path(self.curr_pass < self.max_passes);
                pb.add_bad_blocks(block_count);
                pb.set_elems(
                    range_to_fix.from_block,
                    range_to_fix.to_block,
                    ProgressBarElement::Bad,
                );
                pb.tick();
            }
            self.bad_block_count += block_count;
            if let Some(ref mut block_log_file_writer) = self.block_log_file_writer {
                block_log_file_writer.write_block_positions(
                    range_to_fix.from_block,
                    range_to_fix.to_block,
                    BlockKind::Bad,
                )?;
            }
            return Ok((Vec::new(), false));
        }

        // write
        let rounded_up_byte_count = block_count * self.block_size;
        let byte_count = if self.effective_source_file_length % self.block_size > 0 {
            rounded_up_byte_count - self.block_size
                + self.effective_source_file_length % self.block_size
        } else {
            rounded_up_byte_count
        };
        let source_read_blocks = source_buf.read(range_to_fix.from_block, byte_count);
        let skip_because_of_dest_write_err = match dest_dd_writer.write(
            source_read_blocks,
            range_to_fix.from_block * self.block_size,
        ) {
            Ok(written_bytes) => source_read_blocks.len() != written_bytes,
            Err(e) => match self.on_dest_error_mode {
                OnDestinationErrorMode::Abort => {
                    return Err(e);
                }
                OnDestinationErrorMode::Skip => true,
            },
        };
        let skip_because_of_dest_write_err = if !skip_because_of_dest_write_err {
            if !self.no_local_ver {
                match dest_dd_writer.flush(false) {
                    Ok(_) => false,
                    Err(_) => {
                        return Err(io::Error::new(
                            io::ErrorKind::Other,
                            "Error on flushing, probably due to I/O error, missing permissions or no space left on file system or device.".to_string(),
                        ));
                    }
                }
            } else {
                false
            }
        } else {
            true
        };

        if !skip_because_of_dest_write_err {
            'block: {
                // local verification
                let result = if !self.no_local_ver {
                    // start a new DdReader since an old one could have old, *cached* values
                    let len = block_count * self.block_size;
                    let mut vec = vec![0u8; len as usize];
                    vec.resize(len as usize, 0);
                    let mut dd_local_ver_reader = dd_reader::DdReader::new(
                        self.dest_file_path,
                        len,
                        true,
                        self.is_stderr_to_suppress_for_dest(),
                    );
                    let bytes_read = match dd_local_ver_reader
                        .read(&mut vec, range_to_fix.from_block * self.block_size)
                    {
                        Ok(n) => Some(n),
                        Err(e) => match self.on_dest_error_mode {
                            OnDestinationErrorMode::Abort => {
                                return Err(e);
                            }
                            OnDestinationErrorMode::Skip => None,
                        },
                    };
                    if let Some(bytes_read) = bytes_read {
                        let dest_read_blocks = &vec[..bytes_read];
                        // compare
                        self.cmp_blocks(
                            source_read_blocks,
                            dest_read_blocks,
                            range_to_fix.from_block,
                            range_to_fix.remaining_tries - 1,
                            false,
                        )
                    } else {
                        break 'block;
                    }
                } else {
                    Vec::new()
                };
                self.mark_other_blocks_as_fixed_or_copied(
                    range_to_fix.from_block,
                    range_to_fix.to_block,
                    &result,
                )?;
                return Ok((result, true));
            }
        }

        // mark blocks as destination error
        self.dest_error_block_count += block_count;
        if let Some(ref mut pb) = self.pb {
            pb.set_plus_one_path(self.curr_pass < self.max_passes);
            pb.add_dest_error_blocks(block_count);
            pb.set_elems(
                range_to_fix.from_block,
                range_to_fix.to_block,
                ProgressBarElement::DestinationError,
            );
            pb.tick();
        }
        if let Some(ref mut block_log_file_writer) = self.block_log_file_writer {
            block_log_file_writer.write_block_positions(
                range_to_fix.from_block,
                range_to_fix.to_block,
                BlockKind::DestinationError,
            )?;
        }
        Ok((Vec::new(), true))
    }

    /// Compares a range of source blocks with destination blocks.
    ///
    /// Parameters:
    /// - `source_blocks`: The source bytes.
    /// - `dest_blocks`: The destination bytes.
    /// - `block_offset`: The offset, i.e. the block position in the files, of the source resp. destination bytes.
    /// - `remaining_tries`: The number of remaining tries of repair for this range.
    /// - `during_global_ver`: `true` if this function has been called during global verification, `false` if during local verification.
    ///
    /// Returns the ranges that are to be repaired, i.e. different.
    fn cmp_blocks(
        &mut self,
        source_blocks: &[u8],
        dest_blocks: &[u8],
        block_offset: u64,
        remaining_tries: usize,
        during_global_ver: bool,
    ) -> Vec<RangeToFix> {
        let source_blocks = if block_offset * self.block_size + source_blocks.len() as u64
            > self.effective_source_file_length
        {
            &source_blocks
                [..(self.effective_source_file_length - block_offset * self.block_size) as usize]
        } else {
            source_blocks
        };

        let mut result = Vec::<RangeToFix>::new();
        let source_block_count =
            utils::round_up_to_full_block(source_blocks.len() as u64, self.block_size) as usize;

        let mut ok_mode;
        let mut from_block_to_fix = None;
        let mut to_block_to_fix = None;

        for block_number in 0..source_block_count {
            let curr_block = block_number as u64 + block_offset;
            let blocks_equal = {
                if let Some(dest_file_length) = self.dest_file_length {
                    if !during_global_ver || curr_block * self.block_size < dest_file_length {
                        self.compare_block(source_blocks, dest_blocks, block_number as u64)
                    } else {
                        false
                    }
                } else {
                    // This should not happen since this function would not be called if the destination has no apparent size.
                    unreachable!();
                }
            };

            // summarize
            if !blocks_equal {
                if from_block_to_fix.is_none() {
                    from_block_to_fix = Some(curr_block);
                }
                to_block_to_fix = Some(curr_block + 1);
                ok_mode = false;
            } else {
                ok_mode = true;
            }

            if ok_mode || block_number == source_block_count - 1 {
                if let (Some(from_block), Some(to_block)) = (from_block_to_fix, to_block_to_fix) {
                    result.push(RangeToFix {
                        from_block,
                        to_block,
                        remaining_tries,
                    });
                    from_block_to_fix = None;
                    to_block_to_fix = None;
                }
            }
        }

        result
    }

    /// Compares a single source block with a single destination block.
    ///
    /// Parameters:
    /// - `source_bytes`: The source bytes.
    /// - `dest_bytes`: The destination bytes.
    /// - `block_number`: The offset, i.e. the block position in the files, of the source resp. destination bytes.
    ///
    /// Returns `true` iff the blocks are equal.
    fn compare_block(&mut self, source_bytes: &[u8], dest_bytes: &[u8], block_number: u64) -> bool {
        let from: usize = (self.block_size * block_number).try_into().unwrap();
        let to: usize = cmp::min(
            from + self.block_size as usize,
            cmp::min(source_bytes.len(), dest_bytes.len()),
        );

        !source_bytes[from..to]
            .iter()
            .zip(dest_bytes[from..to].iter())
            .map(|(s, d)| s.cmp(d))
            .any(|ord| ord != cmp::Ordering::Equal)
    }

    /// Slices the given ranges to repair (`vector`) to output-buffer-size-long ranges and reverses the order of those ranges.
    ///
    /// Note that `vector` will be empty in return.
    fn slice_to_obuf_size_and_reverse(&self, mut vector: Vec<RangeToFix>) -> Vec<RangeToFix> {
        let mut result = Vec::<RangeToFix>::with_capacity(vector.len());
        while let Some(range) = vector.pop() {
            let RangeToFix {
                from_block,
                to_block,
                remaining_tries,
            } = range;
            let range_size_in_blocks = to_block - from_block;
            if range_size_in_blocks <= self.obuf_size_in_blocks {
                result.push(range);
            } else {
                let mut temp_vec = vec![];
                let range_whole_obuf_count = range_size_in_blocks / self.obuf_size_in_blocks;
                for i in 0..range_whole_obuf_count {
                    let from = from_block + i * self.obuf_size_in_blocks;
                    let to = from + self.obuf_size_in_blocks;
                    temp_vec.push(RangeToFix {
                        from_block: from,
                        to_block: to,
                        remaining_tries,
                    });
                }
                if range_size_in_blocks % self.obuf_size_in_blocks != 0 {
                    let from = from_block + range_whole_obuf_count * self.obuf_size_in_blocks;
                    let to = from + range_size_in_blocks % self.obuf_size_in_blocks;
                    temp_vec.push(RangeToFix {
                        from_block: from,
                        to_block: to,
                        remaining_tries,
                    });
                }
                while let Some(r) = temp_vec.pop() {
                    result.push(r);
                }
            }
        }
        result
    }

    /// In the range `from_block` (inclusive) to `to_block` (exclusive), mark, count and write blocks as repaired resp. copied that are not in `new_vector_to_fix`.
    fn mark_other_blocks_as_fixed_or_copied(
        &mut self,
        from_block: u64,
        to_block: u64,
        new_vector_to_fix: &[RangeToFix],
    ) -> io::Result<()> {
        let vector = DdPolymerase::reverse_ranges(from_block, to_block, new_vector_to_fix, 0);
        let eof_pos = self.dest_block_count;
        for range in vector.iter() {
            if let Some(eof_pos) = eof_pos {
                if range.to_block <= eof_pos {
                    self.mark_and_count_and_write_as_fixed(range.from_block, range.to_block)?;
                } else if range.from_block >= eof_pos {
                    self.mark_and_count_and_write_as_copied(range.from_block, range.to_block)?;
                } else {
                    self.mark_and_count_and_write_as_fixed(range.from_block, eof_pos)?;
                    self.mark_and_count_and_write_as_copied(eof_pos, range.to_block)?;
                }
            } else {
                self.mark_and_count_and_write_as_copied(range.from_block, range.to_block)?;
            }
        }
        if let Some(pb) = &mut self.pb {
            pb.tick();
        }
        Ok(())
    }

    /// Marks, counts and writes the range from `from_block` (inclusive) to `to_block` (exclusive) as copied.
    fn mark_and_count_and_write_as_copied(
        &mut self,
        from_block: u64,
        to_block: u64,
    ) -> io::Result<()> {
        self.copied_block_count += to_block - from_block;
        if let Some(pb) = &mut self.pb {
            pb.add_copied_blocks(to_block - from_block);
            pb.set_elems(from_block, to_block, ProgressBarElement::Copied);
            pb.set_plus_one_path(self.curr_pass < self.max_passes);
        }
        if let Some(ref mut block_log_file_writer) = self.block_log_file_writer {
            block_log_file_writer.write_block_positions(from_block, to_block, BlockKind::Copied)?;
        }
        Ok(())
    }

    /// Marks, counts and writes the range from `from_block` (inclusive) to `to_block` (exclusive) as repaired.
    fn mark_and_count_and_write_as_fixed(
        &mut self,
        from_block: u64,
        to_block: u64,
    ) -> io::Result<()> {
        self.fixed_block_count += to_block - from_block;
        if let Some(pb) = &mut self.pb {
            pb.add_fixed_blocks(to_block - from_block);
            pb.set_elems(from_block, to_block, ProgressBarElement::Fixed);
            pb.set_plus_one_path(self.curr_pass < self.max_passes);
        }
        if let Some(ref mut block_log_file_writer) = self.block_log_file_writer {
            block_log_file_writer.write_block_positions(from_block, to_block, BlockKind::Fixed)?;
        }
        Ok(())
    }

    /// Marks the ranges in `vector` with `pb_elem` in the progress bar.
    fn mark_as(&mut self, vector: &[RangeToFix], pb_elem: ProgressBarElement) {
        if let Some(pb) = &mut self.pb {
            for range in vector {
                let RangeToFix {
                    from_block,
                    to_block,
                    remaining_tries: _,
                } = range;
                pb.set_elems(*from_block, *to_block, pb_elem);
                pb.tick();
            }
        }
    }

    /// Returns alle ranges from `from_block` (inclusive) to `to_block` (exclusive) that are not in `vector`.
    ///
    /// The [`RangeToFix::remaining_tries`] of the resulting ranges will be `new_remaining_tries`.
    fn reverse_ranges(
        from_block: u64,
        to_block: u64,
        vector: &[RangeToFix],
        new_remaining_tries: usize,
    ) -> Vec<RangeToFix> {
        let mut result = vec![];

        let mut vector = vector.to_owned();
        vector.sort_by(|a, b| a.from_block.cmp(&b.from_block));

        let mut pos = from_block;
        for range in vector.iter() {
            let RangeToFix {
                from_block: from,
                to_block: to,
                remaining_tries: _,
            } = range;
            if pos < *from {
                result.push(RangeToFix {
                    from_block: pos,
                    to_block: *from,
                    remaining_tries: new_remaining_tries,
                });
            }
            pos = *to;
        }
        if pos < to_block {
            result.push(RangeToFix {
                from_block: pos,
                to_block,
                remaining_tries: new_remaining_tries,
            });
        }

        result
    }

    /// Repairs the length of the destination file by truncating to [`Self::effective_source_file_length`].
    fn dd_fix_file_len(&mut self) -> io::Result<()> {
        let mut arg_of: OsString = "of=".into();
        arg_of.push(self.dest_file_path);
        let output = process::Command::new("dd")
            .arg(arg_of)
            .arg("bs=1")
            .arg(format!("seek={}", self.effective_source_file_length))
            .arg("count=0")
            .arg("conv=fdatasync")
            .arg("status=none")
            .output()?;

        if !self.is_stderr_to_suppress_for_dest() {
            io::stderr().write_all(&output.stderr).unwrap();
        }
        if !output.status.success() {
            match self.on_dest_error_mode {
                OnDestinationErrorMode::Abort => {
                    return Err(io::Error::new(
                        io::ErrorKind::Other,
                        format!(
                            "While repairing file length: dd failed with {}",
                            output.status
                        ),
                    ));
                }
                OnDestinationErrorMode::Skip => {}
            }
        }
        Ok(())
    }

    /// Creates the pass exit status for the pass.
    fn create_pass_exit_status(&self) -> io::Result<PassExitStatus> {
        let new_source_file_length = if self.source_block_count.is_some() {
            match utils::get_file_size(self.source_file_path) {
                Ok(len) => len,
                Err(err) => {
                    return Err(io::Error::new(
                        io::ErrorKind::Other,
                        format!("Could not determine source file length: {}", err),
                    ));
                }
            }
        } else {
            Some(self.effective_source_file_length)
        };
        let new_dest_file_length = match utils::get_file_size(self.dest_file_path) {
            Ok(len) => len,
            Err(err) => {
                return Err(io::Error::new(
                    io::ErrorKind::Other,
                    format!("Could not determine destination file length: {}", err),
                ));
            }
        };
        Ok(PassExitStatus {
            different_file_lengths: self.is_truncatable_dest
                && (new_source_file_length != new_dest_file_length),
            fixed_but_not_verified: self.fixed_block_count + self.copied_block_count > 0,
            bad_blocks_left: self.bad_block_count > 0,
            read_write_errors: self.dest_error_block_count > 0 || self.source_error_block_count > 0,
        })
    }

    /// Returns `true` iff stderr is to be suppressed when reading from source.
    fn is_stderr_to_suppress_for_source(&self) -> bool {
        match self.on_source_error_mode {
            OnSourceErrorMode::Abort => false,
            OnSourceErrorMode::Skip => true,
            OnSourceErrorMode::Zeroize => true,
        }
    }

    /// Returns `true` iff stderr is to be suppressed when reading from or writing to destination.
    fn is_stderr_to_suppress_for_dest(&self) -> bool {
        match self.on_dest_error_mode {
            OnDestinationErrorMode::Abort => false,
            OnDestinationErrorMode::Skip => true,
        }
    }

    /// Computes the number of bytes that lie within the source file and within a buffer of length `buf_len` at position `byte_pos`.
    ///
    /// This function assumes that the source is seekable and that `byte_pos` does not exceed source file length.
    fn calc_source_bytes_touched(&self, byte_pos: u64, buf_len: usize) -> usize {
        // we can safely unwrap here, because we checked in advance that the source is seekable
        let remainder = self.source_file_length.unwrap() - byte_pos;
        if remainder < buf_len as u64 {
            remainder as usize
        } else {
            buf_len
        }
    }
}
