/* ddpolymerase: Copy, repair and verify a file
 * Copyright (C) 2021-2023  Tobias Killer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! A progress bar and related functions.
//!
//! A progress bar consists of a bar and some statistics below it including a spinner.

use crate::utils;

use std::cmp;
use std::convert::TryInto;
use std::io::{self, Write};
use std::str::FromStr;
use std::time;

use termion::{clear, color, cursor, style};

/// The elements the bar consists of.
#[derive(Copy, Clone)]
pub(crate) enum ProgressBarElement {
    Unvisited,
    Pending,
    Good,
    Copied,
    Fixed,
    Bad,
    SourceError,
    DestinationError,
}

/// The state of a progress bar.
pub(crate) struct ProgressBar {
    /// The elements in the bar.
    bar_elements: Vec<ProgressBarElement>,
    /// The size of a block in bytes.
    block_size: u64,
    /// The total number of blocks represented by this progress bar.
    total_block_count: u64,
    /// The position of the cursor.
    curr_pos: u64,
    /// The expected maximum number of passes.
    max_passes: usize,
    /// The number of the current pass (starting at 1).
    curr_pass: usize,
    /// Counter counting the number of read blocks.
    read_block_count: u64,
    /// Counter counting the number of copied blocks.
    copied_block_count: u64,
    /// Counter counting the number of repaired blocks.
    fixed_block_count: u64,
    /// Counter counting the number of bad blocks.
    bad_block_count: u64,
    /// Counter counting the number of source error blocks.
    source_error_block_count: u64,
    /// Counter counting the number of destination error blocks.
    dest_error_block_count: u64,
    /// Timer started on creation of the progress bar.
    start_time: time::Instant,
    /// Timer started for every update of the progress bar.
    update_timer: time::Instant,
    /// The byte unit to be used in the statistics.
    units: StatsUnit,
    /// If to use colors.
    use_colors: bool,
    /// The state of the spinner.
    spinner_state: usize,
    /// If the progress bar has not been printed yet.
    first_time_drawing: bool,
    /// If an additional pass to the current one is upcoming.
    plus_one_pass: bool,
    /// If the progress bar is "dynamic" which means that the bar can be compressed.
    dynamic_bar: bool,
    /// If source errors are shown in the statistics.
    show_source_errors: bool,
    /// If destination errors are shown in the statistics.
    show_dest_errors: bool,
}

/// The unit of the statistics.
#[derive(Copy, Clone)]
pub(crate) enum StatsUnit {
    /// Choose an appropriate binary unit suffix dynamically.
    HumanReadable,
    /// In blocks.
    Blocks,
    /// In the given unit.
    Bytes(utils::ByteUnitPrefixNum),
}

impl FromStr for StatsUnit {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "human-readable" => Ok(StatsUnit::HumanReadable),
            "blk" => Ok(StatsUnit::Blocks),
            _ => match utils::ByteUnitPrefixNum::from_str(s) {
                Ok(bs) => Ok(StatsUnit::Bytes(bs)),
                Err(e) => Err(format!("Could not parse unit for statistics: {}", e)),
            },
        }
    }
}

/// The spinner states (`-`, `\`, `|`, `/`).
const SPINNER_STATES: [char; 4] = ['-', '\\', '|', '/'];

impl ProgressBar {
    /// Creates a new progress bar.
    ///
    /// Parameters:
    /// - `block_size`: The size of a block in bytes.
    /// - `total_block_count`: The total number of blocks represented by this progress bar. If `None` then the progress bar will be [`ProgressBar::dynamic_bar`].
    /// - `max_passes`: The expected maximum number of passes.
    /// - `curr_pass`: The number of the current pass (starting at 1).
    /// - `term_width`: The width of the terminal in characters.
    /// - `units`: The byte unit to be used in the statistics.
    /// - `use_colors`: If to use colors.
    /// - `show_source_errors`: If source errors are shown in the statistics.
    /// - `show_dest_errors`: If destination errors are shown in the statistics.
    #[allow(clippy::too_many_arguments)]
    pub(crate) fn new(
        block_size: u64,
        total_block_count: Option<u64>,
        max_passes: usize,
        curr_pass: usize,
        term_width: u16,
        units: StatsUnit,
        use_colors: bool,
        show_source_errors: bool,
        show_dest_errors: bool,
    ) -> ProgressBar {
        let min_term_width = format!("Pass {}: []", max_passes).len() as u16;
        let bar_elem_count = if min_term_width > term_width {
            0
        } else {
            let temp = (term_width - min_term_width) as usize;
            if let Some(total_block_count) = total_block_count {
                if total_block_count <= usize::MAX.try_into().unwrap() {
                    cmp::min(temp, total_block_count as usize)
                } else {
                    temp
                }
            } else {
                (temp / 2) * 2
            }
        };
        let dynamic_bar = total_block_count.is_none();
        let total_block_count = match total_block_count {
            None => bar_elem_count as u64,
            Some(n) => n,
        };
        let mut bar_elements = Vec::with_capacity(bar_elem_count);
        for _ in 0..bar_elem_count {
            bar_elements.push(ProgressBarElement::Unvisited);
        }
        let start_time = time::Instant::now();
        let update_timer = time::Instant::now();
        ProgressBar {
            bar_elements,
            block_size,
            total_block_count,
            curr_pos: 0,
            max_passes,
            curr_pass,
            read_block_count: 0,
            copied_block_count: 0,
            fixed_block_count: 0,
            bad_block_count: 0,
            source_error_block_count: 0,
            dest_error_block_count: 0,
            start_time,
            update_timer,
            units,
            use_colors,
            spinner_state: 0,
            first_time_drawing: true,
            plus_one_pass: false,
            dynamic_bar,
            show_source_errors,
            show_dest_errors,
        }
    }

    /// Prints the progress bar to stderr.
    pub(crate) fn draw(&mut self) {
        let mut buffer = String::new();

        // move cursor
        if self.first_time_drawing {
            self.first_time_drawing = false;
        } else {
            // move cursor to the beginning of the output
            eprint!(
                "{}",
                cursor::Up(
                    4 + if self.show_source_errors || self.show_dest_errors {
                        1
                    } else {
                        0
                    }
                )
            );
        }

        // progress bar
        if self.use_colors {
            buffer.push_str(&format!(
                "{}{}{}",
                color::Fg(color::LightCyan),
                color::Bg(color::Black),
                style::Bold
            ));
        }
        buffer.push_str("Pass ");
        for _ in 0..(format!("{}", self.max_passes).len() - format!("{}", self.curr_pass).len()) {
            buffer.push(' ');
        }
        buffer.push_str(&format!("{}: ", self.curr_pass));
        if self.use_colors {
            buffer.push_str(&format!("{}", style::Reset));
        }
        let mut bar = String::new();
        if let Some(element_pos_of_curr_pos) = self.bar_pos(self.curr_pos) {
            for (pos, be) in self.bar_elements.iter().enumerate() {
                let s = if pos == element_pos_of_curr_pos {
                    if self.use_colors {
                        format!(
                            "{}{}{}>{}",
                            color::Fg(color::LightCyan),
                            color::Bg(color::Black),
                            style::Bold,
                            style::Reset
                        )
                        .to_owned()
                    } else {
                        ">".to_string()
                    }
                } else {
                    self.elem_char(*be)
                };
                bar.push_str(&s);
            }
        }
        if self.use_colors {
            buffer.push_str(&format!(
                "{}{}",
                color::Fg(color::White),
                color::Bg(color::Black)
            ));
        }
        buffer.push('[');
        if self.use_colors {
            buffer.push_str(&format!("{}", style::Reset));
        }
        buffer.push_str(&bar);
        if self.use_colors {
            buffer.push_str(&format!(
                "{}{}",
                color::Fg(color::White),
                color::Bg(color::Black)
            ));
        }
        {
            let temp = if self.dynamic_bar {
                ")".to_string()
            } else {
                "]".to_string()
            };
            buffer.push_str(&temp);
        }
        if self.use_colors {
            buffer.push_str(&format!("{}", style::Reset));
        }
        eprintln!("{}{}", buffer, clear::UntilNewline);
        buffer.clear();

        // stats
        buffer.push_str(&format!(
            "{} total, ",
            if self.dynamic_bar {
                "?".to_string()
            } else {
                self.bytes_to_string(self.total_block_count)
            }
        ));
        buffer.push_str(&format!(
            "{} read, ",
            self.bytes_to_string(self.read_block_count)
        ));
        buffer.push_str(&format!(
            "{} copied, ",
            self.bytes_to_string(self.copied_block_count)
        ));
        buffer.push_str(&format!(
            "{} repaired, ",
            self.bytes_to_string(self.fixed_block_count)
        ));
        buffer.push_str(&format!(
            "{} bad",
            self.bytes_to_string(self.bad_block_count)
        ));
        eprintln!("{}{}", buffer, clear::UntilNewline);
        buffer.clear();
        if self.show_source_errors || self.show_dest_errors {
            if self.show_source_errors {
                buffer.push_str(&format!(
                    "{} source error",
                    self.bytes_to_string(self.source_error_block_count)
                ));
            }
            if self.show_source_errors && self.show_dest_errors {
                buffer.push_str(", ");
            }
            if self.show_dest_errors {
                buffer.push_str(&format!(
                    "{} destination error",
                    self.bytes_to_string(self.dest_error_block_count)
                ));
            }
            eprintln!("{}{}", buffer, clear::UntilNewline);
            buffer.clear();
        }
        buffer.push_str("rate: ");
        {
            let elapsed_time = self.start_time.elapsed();
            let elapsed_time_as_secs_f64 = elapsed_time.as_secs_f64();
            if elapsed_time_as_secs_f64 > 0.0 {
                match self.units {
                    StatsUnit::Blocks => {
                        let (num, suffix) = utils::number_with_unit_prefix(
                            self.read_block_count as f64 / elapsed_time_as_secs_f64,
                            false,
                            None,
                        );
                        buffer.push_str(&format!("{:.1}{} blk", num, suffix));
                    }
                    StatsUnit::HumanReadable => {
                        let (num, suffix) = utils::number_with_unit_prefix(
                            (self.read_block_count * self.block_size) as f64
                                / elapsed_time_as_secs_f64,
                            true,
                            None,
                        );
                        buffer.push_str(&format!("{:.1} {}B", num, suffix));
                    }
                    StatsUnit::Bytes(utils::ByteUnitPrefixNum {
                        exponent,
                        base: _,
                        is_binary,
                    }) => {
                        let (num, suffix) = utils::number_with_unit_prefix(
                            (self.read_block_count * self.block_size) as f64
                                / elapsed_time_as_secs_f64,
                            is_binary,
                            Some(exponent),
                        );
                        buffer.push_str(&format!("{:.1} {}B", num, suffix));
                    }
                }
            }
            buffer.push_str("/s, ET: ");
            buffer.push_str(&utils::seconds_to_string(
                elapsed_time.as_secs(),
                usize::MAX,
            ));
            eprintln!("{}{}", buffer, clear::UntilNewline);
            buffer.clear();
            buffer.push_str(&format!("{}  at ", SPINNER_STATES[self.spinner_state]));
            {
                let temp = if !self.dynamic_bar && self.total_block_count > 0 {
                    format!(
                        "{:.3}",
                        (self.curr_pos as f64 / self.total_block_count as f64) * 100.0
                    )
                } else {
                    "?".to_string()
                };
                buffer.push_str(&temp);
            }
            buffer.push_str(" %, ETA: ");
            if !self.dynamic_bar && elapsed_time_as_secs_f64 > 0.0 && self.curr_pos > 0 {
                buffer.push_str(&utils::seconds_to_string(
                    ((self.total_block_count - self.curr_pos) as f64
                        / (self.curr_pos as f64 / elapsed_time_as_secs_f64))
                        as u64,
                    2,
                ));
            } else {
                buffer.push('?');
            }
        }
        if self.plus_one_pass {
            buffer.push_str(" (+1 pass)");
        }
        eprintln!("{}{}", buffer, clear::UntilNewline);
        let _ = io::stdout().flush();
    }

    /// Both prints the progress bar using [`ProgressBar::draw()`] and spins the spinner
    /// if 250 milliseconds have been passed since the last call of this function.
    pub(crate) fn tick(&mut self) {
        if self.update_timer.elapsed().as_millis() >= 250 {
            self.update_timer = time::Instant::now();
            self.draw();
            // next spinner state
            self.spinner_state += 1;
            if self.spinner_state >= SPINNER_STATES.len() {
                self.spinner_state = 0;
            }
        }
    }

    /// Formats and returns `block_count` depending on [`ProgressBar::units`].
    fn bytes_to_string(&self, block_count: u64) -> String {
        match self.units {
            StatsUnit::Blocks => {
                format!("{} blk", block_count)
            }
            StatsUnit::HumanReadable => {
                let (num, suffix) = utils::number_with_unit_prefix(
                    (block_count * self.block_size) as f64,
                    true,
                    None,
                );
                if suffix.is_empty() {
                    format!("{} B", num)
                } else {
                    format!("{:.1} {}B", num, suffix)
                }
            }
            StatsUnit::Bytes(utils::ByteUnitPrefixNum {
                exponent,
                base: _,
                is_binary,
            }) => {
                let (num, suffix) = utils::number_with_unit_prefix(
                    (block_count * self.block_size) as f64,
                    is_binary,
                    Some(exponent),
                );
                if suffix.is_empty() {
                    format!("{} B", num)
                } else {
                    format!("{:.1} {}B", num, suffix)
                }
            }
        }
    }

    /// Returns the internal index position of the related bar element
    /// that is associated with `block_pos`.
    ///
    /// The result is `None` if the bar has size 0.
    fn bar_pos(&self, block_pos: u64) -> Option<usize> {
        if self.total_block_count == 0 {
            None
        } else {
            Some(
                (self.bar_elements.len() as u64 * block_pos / self.total_block_count)
                    .try_into()
                    .unwrap(),
            )
        }
    }

    /// Formats and returns `elem` depending on [`ProgressBar::use_colors`].
    ///
    /// The mapping is as follows:
    /// - [`ProgressBarElement::Unvisited`] to `<space>`,
    /// - [`ProgressBarElement::Pending`] to `~`,
    /// - [`ProgressBarElement::Good`] to `.`,
    /// - [`ProgressBarElement::Copied`] to `c`,
    /// - [`ProgressBarElement::Fixed`] to `r`,
    /// - [`ProgressBarElement::Bad`] to `b`,
    /// - [`ProgressBarElement::SourceError`] to `s`,
    /// - [`ProgressBarElement::DestinationError`] to `d`.
    fn elem_char(&self, elem: ProgressBarElement) -> String {
        match elem {
            ProgressBarElement::Unvisited => {
                if self.use_colors {
                    format!(
                        "{}{} {}",
                        color::Fg(color::White),
                        color::Bg(color::Black),
                        style::Reset
                    )
                } else {
                    " ".to_string()
                }
            }
            ProgressBarElement::Pending => {
                if self.use_colors {
                    format!(
                        "{}{}~{}",
                        color::Fg(color::White),
                        color::Bg(color::Black),
                        style::Reset
                    )
                } else {
                    "~".to_string()
                }
            }
            ProgressBarElement::Good => {
                if self.use_colors {
                    format!(
                        "{}{}{}.{}",
                        color::Fg(color::LightGreen),
                        color::Bg(color::Black),
                        style::Bold,
                        style::Reset
                    )
                } else {
                    ".".to_string()
                }
            }
            ProgressBarElement::Copied => {
                if self.use_colors {
                    format!(
                        "{}{}{}c{}",
                        color::Fg(color::LightMagenta),
                        color::Bg(color::Black),
                        style::Bold,
                        style::Reset
                    )
                } else {
                    "c".to_string()
                }
            }
            ProgressBarElement::Fixed => {
                if self.use_colors {
                    format!(
                        "{}{}{}r{}",
                        color::Fg(color::LightYellow),
                        color::Bg(color::Black),
                        style::Bold,
                        style::Reset
                    )
                } else {
                    "r".to_string()
                }
            }
            ProgressBarElement::Bad => {
                if self.use_colors {
                    format!(
                        "{}{}{}b{}",
                        color::Fg(color::LightRed),
                        color::Bg(color::Black),
                        style::Bold,
                        style::Reset
                    )
                } else {
                    "b".to_string()
                }
            }
            ProgressBarElement::SourceError => {
                if self.use_colors {
                    format!(
                        "{}{}{}s{}",
                        color::Fg(color::LightRed),
                        color::Bg(color::White),
                        style::Bold,
                        style::Reset
                    )
                } else {
                    "s".to_string()
                }
            }
            ProgressBarElement::DestinationError => {
                if self.use_colors {
                    format!(
                        "{}{}{}d{}",
                        color::Fg(color::LightRed),
                        color::Bg(color::White),
                        style::Bold,
                        style::Reset
                    )
                } else {
                    "d".to_string()
                }
            }
        }
    }

    /// Sets the internal block position `ProgressBar::curr_pos` to `new_block_pos`.
    pub(crate) fn set_pos(&mut self, new_block_pos: u64) {
        self.compress_bar_if_necessary(new_block_pos);
        self.curr_pos = new_block_pos;
    }

    /// Sets the internal block position `ProgressBar::curr_pos` to the allowed maximum ([`ProgressBar::total_block_count`]).
    pub(crate) fn set_pos_to_max(&mut self) {
        self.set_pos(self.total_block_count);
    }

    /// Makes the last drawing.
    pub(crate) fn finish(&mut self) {
        self.set_pos_to_max();
        self.draw();
        eprint!("{}{}{}", cursor::Up(1), clear::UntilNewline, cursor::Up(1));
    }

    /// Returns the priority of the given element `elem`.
    ///
    /// The mapping is as follows:
    /// - [`ProgressBarElement::Unvisited`] to 0,
    /// - [`ProgressBarElement::Pending`] to 1,
    /// - [`ProgressBarElement::Good`] to 2,
    /// - [`ProgressBarElement::Copied`] to 3,
    /// - [`ProgressBarElement::Fixed`] to 4,
    /// - [`ProgressBarElement::Bad`] to 5,
    /// - [`ProgressBarElement::DestinationError`] to 6 (sic!),
    /// - [`ProgressBarElement::SourceError`] to 7 (sic!).
    ///
    /// Source errors have got higher priority than destination errors, because they also mean a destination error in a sense.
    fn elem_priority(elem: ProgressBarElement) -> usize {
        match elem {
            ProgressBarElement::Unvisited => 0,
            ProgressBarElement::Pending => 1,
            ProgressBarElement::Good => 2,
            ProgressBarElement::Copied => 3,
            ProgressBarElement::Fixed => 4,
            ProgressBarElement::Bad => 5,
            ProgressBarElement::DestinationError => 6,
            ProgressBarElement::SourceError => 7,
        }
    }

    /// Sets the elements of the progress bar from block position `from_block` to block position `to_block` to element `elem`.
    pub(crate) fn set_elems(&mut self, from_block: u64, to_block: u64, elem: ProgressBarElement) {
        if self.total_block_count == 0 || to_block == 0 {
            return;
        }
        self.compress_bar_if_necessary(to_block);
        let from_pos = match self.bar_pos(from_block) {
            None => return,
            Some(val) => val,
        };
        if from_pos >= self.bar_elements.len() {
            return;
        }
        let to_pos_incl = cmp::min(
            match self.bar_pos(to_block - 1) {
                None => return,
                Some(val) => val,
            },
            self.bar_elements.len() - 1,
        );

        for pos in from_pos..=to_pos_incl {
            let pb_elem = self.bar_elements[pos];

            let elem_prio = ProgressBar::elem_priority(elem);
            let pb_elem_prio = ProgressBar::elem_priority(pb_elem);

            if elem_prio > pb_elem_prio {
                self.bar_elements[pos] = elem;
            }
        }
    }

    /// Compresses the bar (using [`ProgressBar::compress_bar`]) to an appropriate size if necessary.
    ///
    /// The minimum number of blocks represented by the bar will be `min_block_count`.
    fn compress_bar_if_necessary(&mut self, min_block_count: u64) {
        if self.dynamic_bar && self.total_block_count > 0 {
            while self.total_block_count < min_block_count {
                self.compress_bar();
            }
        }
    }

    /// Compresses the bar by half if possible or to maximum capacity otherwise.
    fn compress_bar(&mut self) {
        if self.total_block_count <= u64::MAX / 2 {
            self.compress_bar_by_half();
        } else {
            self.compress_bar_to_max_capacity();
        }
    }

    /// Compresses the bar by half.
    fn compress_bar_by_half(&mut self) {
        let len = self.bar_elements.len();

        // calc result bar elements
        let mut result_bar_elements = Vec::with_capacity(len);
        for result_pos in 0..(len / 2) {
            let bar_element_1 = self.bar_elements[result_pos * 2];
            let bar_element_2 = self.bar_elements[result_pos * 2 + 1];
            let bar_element_1_prio = ProgressBar::elem_priority(bar_element_1);
            let bar_element_2_prio = ProgressBar::elem_priority(bar_element_2);
            result_bar_elements.push(if bar_element_1_prio > bar_element_2_prio {
                bar_element_1
            } else {
                bar_element_2
            });
        }
        for _ in 0..(len / 2) {
            result_bar_elements.push(ProgressBarElement::Unvisited);
        }

        // override current bar elements
        self.bar_elements[..len].copy_from_slice(&result_bar_elements[..len]);

        // new total count
        self.total_block_count *= 2;
    }

    /// Compresses the bar to maximum capacity.
    fn compress_bar_to_max_capacity(&mut self) {
        let len = self.bar_elements.len();

        // init result
        let mut result_bar_elements = Vec::with_capacity(len);
        for _ in 0..len {
            result_bar_elements.push(ProgressBarElement::Unvisited);
        }

        // calc result bar elements
        let result_total_block_count = u64::MAX;
        for pos in 0..len {
            let result_pos = (pos as u128 * self.total_block_count as u128
                / result_total_block_count as u128) as usize;
            let bar_element = self.bar_elements[pos];
            let already_result_bar_element = result_bar_elements[result_pos];
            let bar_element_prio = ProgressBar::elem_priority(bar_element);
            let already_result_bar_element_prio =
                ProgressBar::elem_priority(already_result_bar_element);
            result_bar_elements[result_pos] = if bar_element_prio > already_result_bar_element_prio
            {
                bar_element
            } else {
                already_result_bar_element
            };
        }

        // override current bar elements
        self.bar_elements[..len].copy_from_slice(&result_bar_elements[..len]);

        // new total count
        self.total_block_count = result_total_block_count;
    }

    /// Adds `n` blocks to [`ProgressBar::read_block_count`].
    pub(crate) fn add_read_blocks(&mut self, n: u64) {
        self.read_block_count += n;
    }

    /// Adds `n` blocks to [`ProgressBar::copied_block_count`].
    pub(crate) fn add_copied_blocks(&mut self, n: u64) {
        self.copied_block_count += n;
    }

    /// Adds `n` blocks to [`ProgressBar::fixed_block_count`].
    pub(crate) fn add_fixed_blocks(&mut self, n: u64) {
        self.fixed_block_count += n;
    }

    /// Adds `n` blocks to [`ProgressBar::bad_block_count`].
    pub(crate) fn add_bad_blocks(&mut self, n: u64) {
        self.bad_block_count += n;
    }

    /// Adds `n` blocks to [`ProgressBar::source_error_block_count`].
    pub(crate) fn add_source_error_blocks(&mut self, n: u64) {
        self.source_error_block_count += n;
    }

    /// Adds `n` blocks to [`ProgressBar::dest_error_block_count`].
    pub(crate) fn add_dest_error_blocks(&mut self, n: u64) {
        self.dest_error_block_count += n;
    }

    /// Sets [`ProgressBar::plus_one_pass`] to `enable`.
    pub(crate) fn set_plus_one_path(&mut self, enable: bool) {
        self.plus_one_pass = enable;
    }
}
