# ddpolymerase

ddpolymerase – copy, repair and verify a file


## Motivation

The main use case is probably writing an ISO image to a USB flash drive or repairing an existing one.
Repair is necessary because USB flash drives are sometimes slightly faulty on random spots.
This is where *ddpolymerase* comes into play.
It will try to repair only those spots which can save both valuable write cycles and time.


## Build Requirements

- a 64-bit Linux system installed
- Rust/Cargo installed
  - works with `rustc 1.70.0`


## How to build

Run `cargo build --release --locked` in the repository's workspace.
The result will be the executable file `target/release/ddpolymerase`.
Strip it with `strip target/release/ddpolymerase`.


## System Requirements

- a 64-bit Linux system installed (including `coreutils` and `util-linux`)


## How to use

See the [manual page](man/ddpolymerase.1).

For example, in order to repair an existing ISO image `image.iso` on a USB flash drive `/dev/sdX`, enter into a terminal:

```shell_session
# ddpolymerase --src image.iso --dest /dev/sdX
```

If you want to copy the image to the USB flash drive first, it is recommended to add the `--copy-first` option in order to increase speed:

```shell_session
# ddpolymerase --copy-first --src image.iso --dest /dev/sdX
```

A progress bar with some statistics will appear during the run like this:

```
Pass 1: [..rrrrrrrr........rrrrr>                                              ]
1.0 GiB total, 342.0 MiB read, 0 B copied, 171.0 MiB repaired, 0 B bad
rate: 52.0 MiB/s, ET: 6 s
-  at 33.398 %, ETA: 13 s (+1 pass)
```

If the termination message says that there are repaired/copied but unverified spots then you should run *ddpolymerase* again in order to verify and repair the image on the flash drive again.
If the termination message says that there are bad blocks left then the image on the flash drive is (still) broken.

**There is no guarantee that *ddpolymerase* can repair all spots successfully due to potential hardware failures.**


## License

GNU General Public License v3.0 or later

See [COPYING](COPYING) for the full text.
