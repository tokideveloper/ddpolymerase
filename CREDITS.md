Credits
=======

Contributors
------------

- Tobias Killer \<tokidev "AT" posteo "DOT" de\> (original author)


Special thanks to …
-------------------

- danidani \<danidani "AT" posteo "DOT" de\> (for valuable feedback)
