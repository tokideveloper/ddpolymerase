#!/usr/bin/env bash

# ddpolymerase: Copy, repair and verify a file
# Copyright (C) 2021-2023  Tobias Killer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# check if user is root
[ "$EUID" = "0" ] || error_exit "You must be root in order to continue the tests." 1

source ./test_common.sh



### device tests ###

block_device_test_1() {
    next_test "Block Device Test 1 (bad blocks; matching sizes)"
    before_each_test
    prepare_file aaaa > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    prepare_file bbbb > "${DEVICE_FILE_B}"
    prepare_file abaa > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_B="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_B}" "${DEVICE_FILE_B}" || error_exit "Could not setup '${LOOP_DEVICE_B}'." 3
    dmsetup create "${DEVICE_B}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((0 * SECTORS_PER_BLOCK))
$((1 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) zero
$((2 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((2 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "${FILE_A}" --dest "/dev/mapper/${DEVICE_B}" --block-size="${BLOCK_SIZE}"B --ibuf-size="${BLOCK_SIZE}"B --obuf-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_B}"
    losetup -d "${LOOP_DEVICE_B}"
    check_exit_code "$R" 4
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${DEVICE_FILE_B}" 1
    check_diff "${DEVICE_FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 4 6 0 2 4 0 0
    after_each_test
}

block_device_test_2a() {
    next_test "Block Device Test 2a (unmatching sizes)"
    before_each_test
    prepare_file aaa > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    prepare_file bbbb > "${DEVICE_FILE_B}"
    prepare_file aaab > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_B="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_B}" "${DEVICE_FILE_B}" || error_exit "Could not setup '${LOOP_DEVICE_B}'." 3
    dmsetup create "${DEVICE_B}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((4 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((0 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "${FILE_A}" --dest "/dev/mapper/${DEVICE_B}" --block-size="${BLOCK_SIZE}"B --ibuf-size="${BLOCK_SIZE}"B --obuf-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_B}"
    losetup -d "${LOOP_DEVICE_B}"
    check_exit_code "$R" 0
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${DEVICE_FILE_B}" 1
    check_diff "${DEVICE_FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 2 1 0 1 0 0 0
    after_each_test
}

block_device_test_2b() {
    next_test "Block Device Test 2b (source file larger than destination device, no -f)"
    before_each_test
    prepare_file aaaaa > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    prepare_file bbbb > "${DEVICE_FILE_B}"
    prepare_file bbbb > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_B="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_B}" "${DEVICE_FILE_B}" || error_exit "Could not setup '${LOOP_DEVICE_B}'." 3
    dmsetup create "${DEVICE_B}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((4 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((0 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "${FILE_A}" --dest "/dev/mapper/${DEVICE_B}" --block-size="${BLOCK_SIZE}"B --ibuf-size="${BLOCK_SIZE}"B --obuf-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_B}"
    losetup -d "${LOOP_DEVICE_B}"
    check_exit_code "$R" 65
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${DEVICE_FILE_B}" "${DESIRED_FILE_B}" 0
    after_each_test
}

block_device_test_2c() {
    next_test "Block Device Test 2c (source file larger than destination device, with -f)"
    before_each_test
    prepare_file aaaaa > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    prepare_file bbbb > "${DEVICE_FILE_B}"
    prepare_file aaaa > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_B="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_B}" "${DEVICE_FILE_B}" || error_exit "Could not setup '${LOOP_DEVICE_B}'." 3
    dmsetup create "${DEVICE_B}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((4 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((0 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "${FILE_A}" --dest "/dev/mapper/${DEVICE_B}" --force --block-size="${BLOCK_SIZE}"B --ibuf-size="${BLOCK_SIZE}"B --obuf-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_B}"
    losetup -d "${LOOP_DEVICE_B}"
    check_exit_code "$R" 65
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${DEVICE_FILE_B}" 1
    check_diff "${DEVICE_FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 0 1 0 0 0
    after_each_test
}

block_device_test_3() {
    next_test "Block Device Test 3 (unmatching sizes, exit status 2)"
    before_each_test
    prepare_file aaa > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    prepare_file bbbb > "${DEVICE_FILE_B}"
    prepare_file aaab > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_B="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_B}" "${DEVICE_FILE_B}" || error_exit "Could not setup '${LOOP_DEVICE_B}'." 3
    dmsetup create "${DEVICE_B}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((4 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((0 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "${FILE_A}" --dest "/dev/mapper/${DEVICE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=1 --ibuf-size="${BLOCK_SIZE}"B --obuf-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_B}"
    losetup -d "${LOOP_DEVICE_B}"
    check_exit_code "$R" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${DEVICE_FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 0 1 0 0 0
    after_each_test
}

block_device_test_4() {
    next_test "Block Device Test 4 (exit status 6)"
    before_each_test
    prepare_file aaaa > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    prepare_file bbbb > "${DEVICE_FILE_B}"
    prepare_file abaa > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_B="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_B}" "${DEVICE_FILE_B}" || error_exit "Could not setup '${LOOP_DEVICE_B}'." 3
    dmsetup create "${DEVICE_B}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((0 * SECTORS_PER_BLOCK))
$((1 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) zero
$((2 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((2 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "${FILE_A}" --dest "/dev/mapper/${DEVICE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=1 --ibuf-size="${BLOCK_SIZE}"B --obuf-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_B}"
    losetup -d "${LOOP_DEVICE_B}"
    check_exit_code "$R" 6
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${DEVICE_FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 0 2 1 0 0
    after_each_test
}

block_device_test_5() {
    next_test "Block Device Test 5 (exit status 6)"
    before_each_test
    prepare_file aaa > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    prepare_file bbbb > "${DEVICE_FILE_B}"
    prepare_file abab > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_B="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_B}" "${DEVICE_FILE_B}" || error_exit "Could not setup '${LOOP_DEVICE_B}'." 3
    dmsetup create "${DEVICE_B}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((0 * SECTORS_PER_BLOCK))
$((1 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) zero
$((2 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((2 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "${FILE_A}" --dest "/dev/mapper/${DEVICE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=1 --ibuf-size="${BLOCK_SIZE}"B --obuf-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_B}"
    losetup -d "${LOOP_DEVICE_B}"
    check_exit_code "$R" 6
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${DEVICE_FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 0 2 1 0 0
    after_each_test
}

block_device_test_6a() {
    next_test "Block Device Test 6a (destination file error, implicit --on-dest-error=abort)"
    before_each_test
    prepare_file 0123 > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    prepare_file 4567 > "${DEVICE_FILE_B}"
    prepare_file 0567 > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_B="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_B}" "${DEVICE_FILE_B}" || error_exit "Could not setup '${LOOP_DEVICE_B}'." 3
    dmsetup create "${DEVICE_B}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((0 * SECTORS_PER_BLOCK))
$((1 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((2 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((2 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "${FILE_A}" --dest "/dev/mapper/${DEVICE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=1 --ibuf-size="${BLOCK_SIZE}"B --obuf-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_B}"
    losetup -d "${LOOP_DEVICE_B}"
    check_exit_code "$R" 65
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${DEVICE_FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 0 1 0 0 0
    after_each_test
}

block_device_test_6b() {
    next_test "Block Device Test 6b (no destination file error, --on-dest-error=skip)"
    before_each_test
    prepare_file 0123 > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    prepare_file 4567 > "${DEVICE_FILE_B}"
    prepare_file 0123 > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_B="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_B}" "${DEVICE_FILE_B}" || error_exit "Could not setup '${LOOP_DEVICE_B}'." 3
    dmsetup create "${DEVICE_B}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((4 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((0 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "${FILE_A}" --dest "/dev/mapper/${DEVICE_B}" --block-size="${BLOCK_SIZE}"B --on-dest-error=skip --max-passes=1 --ibuf-size="${BLOCK_SIZE}"B --obuf-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_B}"
    losetup -d "${LOOP_DEVICE_B}"
    check_exit_code "$R" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${DEVICE_FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 0 1 0 0 0
    after_each_test
}

block_device_test_6c() {
    next_test "Block Device Test 6c (destination file error, --on-dest-error=skip)"
    before_each_test
    prepare_file 0123 > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    prepare_file 4567 > "${DEVICE_FILE_B}"
    prepare_file 0523 > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_B="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_B}" "${DEVICE_FILE_B}" || error_exit "Could not setup '${LOOP_DEVICE_B}'." 3
    dmsetup create "${DEVICE_B}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((0 * SECTORS_PER_BLOCK))
$((1 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((2 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((2 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "${FILE_A}" --dest "/dev/mapper/${DEVICE_B}" --block-size="${BLOCK_SIZE}"B --on-dest-error=skip --max-passes=1 --ibuf-size="${BLOCK_SIZE}"B --obuf-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_B}"
    losetup -d "${LOOP_DEVICE_B}"
    check_exit_code "$R" 10
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${DEVICE_FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 0 2 0 0 1
    after_each_test
}

block_device_test_6d() {
    next_test "Block Device Test 6d (destination file error, --on-dest-error=skip, bigger ibuf size, test 1)"
    before_each_test
    prepare_file 012345 > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    prepare_file 678901 > "${DEVICE_FILE_B}"
    prepare_file 018945 > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_B="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_B}" "${DEVICE_FILE_B}" || error_exit "Could not setup '${LOOP_DEVICE_B}'." 3
    dmsetup create "${DEVICE_B}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((0 * SECTORS_PER_BLOCK))
$((2 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((3 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((3 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "${FILE_A}" --dest "/dev/mapper/${DEVICE_B}" --block-size="${BLOCK_SIZE}"B --on-dest-error=skip --max-passes=1 --ibuf-size="$((2 * BLOCK_SIZE))"B --obuf-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_B}"
    losetup -d "${LOOP_DEVICE_B}"
    check_exit_code "$R" 10
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${DEVICE_FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 0 2 0 0 1
    after_each_test
}

block_device_test_6e() {
    next_test "Block Device Test 6e (destination file error, --on-dest-error=skip, bigger ibuf size, test 2)"
    before_each_test
    prepare_file 012345 > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    prepare_file 678901 > "${DEVICE_FILE_B}"
    prepare_file 018945 > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_B="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_B}" "${DEVICE_FILE_B}" || error_exit "Could not setup '${LOOP_DEVICE_B}'." 3
    dmsetup create "${DEVICE_B}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((0 * SECTORS_PER_BLOCK))
$((3 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((4 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((4 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "${FILE_A}" --dest "/dev/mapper/${DEVICE_B}" --block-size="${BLOCK_SIZE}"B --on-dest-error=skip --max-passes=1 --ibuf-size="$((2 * BLOCK_SIZE))"B --obuf-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_B}"
    losetup -d "${LOOP_DEVICE_B}"
    check_exit_code "$R" 10
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${DEVICE_FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 0 2 0 0 1
    after_each_test
}

block_device_test_6f() {
    next_test "Block Device Test 6f (destination file error, --on-dest-error=skip, bigger ibuf size, test 3)"
    before_each_test
    prepare_file 012345 > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    prepare_file 678901 > "${DEVICE_FILE_B}"
    prepare_file 018945 > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_B="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_B}" "${DEVICE_FILE_B}" || error_exit "Could not setup '${LOOP_DEVICE_B}'." 3
    dmsetup create "${DEVICE_B}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((0 * SECTORS_PER_BLOCK))
$((2 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) error
$((4 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((4 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "${FILE_A}" --dest "/dev/mapper/${DEVICE_B}" --block-size="${BLOCK_SIZE}"B --on-dest-error=skip --max-passes=1 --ibuf-size="$((2 * BLOCK_SIZE))"B --obuf-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_B}"
    losetup -d "${LOOP_DEVICE_B}"
    check_exit_code "$R" 10
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${DEVICE_FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 0 2 0 0 1
    after_each_test
}

block_device_test_6g() {
    next_test "Block Device Test 6g (destination file error, --on-dest-error=skip, bigger obuf size, test 1)"
    before_each_test
    prepare_file 012345 > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    prepare_file 678901 > "${DEVICE_FILE_B}"
    prepare_file 018345 > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_B="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_B}" "${DEVICE_FILE_B}" || error_exit "Could not setup '${LOOP_DEVICE_B}'." 3
    dmsetup create "${DEVICE_B}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((0 * SECTORS_PER_BLOCK))
$((2 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((3 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((3 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "${FILE_A}" --dest "/dev/mapper/${DEVICE_B}" --block-size="${BLOCK_SIZE}"B --on-dest-error=skip --max-passes=1 --ibuf-size="$((1 * BLOCK_SIZE))"B --obuf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_B}"
    losetup -d "${LOOP_DEVICE_B}"
    check_exit_code "$R" 10
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${DEVICE_FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 0 2 0 0 1
    after_each_test
}

block_device_test_6h() {
    next_test "Block Device Test 6h (destination file error, --on-dest-error=skip, bigger obuf size, test 2)"
    before_each_test
    prepare_file 012345 > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    prepare_file 678901 > "${DEVICE_FILE_B}"
    prepare_file 012945 > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_B="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_B}" "${DEVICE_FILE_B}" || error_exit "Could not setup '${LOOP_DEVICE_B}'." 3
    dmsetup create "${DEVICE_B}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((0 * SECTORS_PER_BLOCK))
$((3 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((4 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((4 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "${FILE_A}" --dest "/dev/mapper/${DEVICE_B}" --block-size="${BLOCK_SIZE}"B --on-dest-error=skip --max-passes=1 --ibuf-size="$((1 * BLOCK_SIZE))"B --obuf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_B}"
    losetup -d "${LOOP_DEVICE_B}"
    check_exit_code "$R" 10
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${DEVICE_FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 0 2 0 0 1
    after_each_test
}

block_device_test_6i() {
    next_test "Block Device Test 6i (destination file error, --on-dest-error=skip, bigger obuf size, test 2)"
    before_each_test
    prepare_file 012345 > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    prepare_file 678901 > "${DEVICE_FILE_B}"
    prepare_file 018945 > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_B="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_B}" "${DEVICE_FILE_B}" || error_exit "Could not setup '${LOOP_DEVICE_B}'." 3
    dmsetup create "${DEVICE_B}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((0 * SECTORS_PER_BLOCK))
$((2 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) error
$((4 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((4 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "${FILE_A}" --dest "/dev/mapper/${DEVICE_B}" --block-size="${BLOCK_SIZE}"B --on-dest-error=skip --max-passes=1 --ibuf-size="$((1 * BLOCK_SIZE))"B --obuf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_B}"
    losetup -d "${LOOP_DEVICE_B}"
    check_exit_code "$R" 10
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${DEVICE_FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 0 2 0 0 1
    after_each_test
}

block_device_test_7a() {
    next_test "Block Device Test 7a (source file error)"
    before_each_test
    prepare_file aaaa > "${DEVICE_FILE_A}"
    cp "${DEVICE_FILE_A}" "${ORIG_FILE_A}"
    prepare_file bbbb > "${FILE_B}"
    prepare_file abbb > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_A="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_A}" "${DEVICE_FILE_A}" || error_exit "Could not setup '${LOOP_DEVICE_A}'." 3
    dmsetup create "${DEVICE_A}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((0 * SECTORS_PER_BLOCK))
$((1 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((2 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((2 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "/dev/mapper/${DEVICE_A}" --dest "${FILE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=1 --ibuf-size="${BLOCK_SIZE}"B --obuf-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_A}"
    losetup -d "${LOOP_DEVICE_A}"
    check_exit_code "$R" 65
    check_unmodified_file "${ORIG_FILE_A}" "${DEVICE_FILE_A}"
    check_diff "${FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 0 1 0 0 0
    after_each_test
}

block_device_test_7b() {
    next_test "Block Device Test 7b (source file error, --on-src-error=zeroize)"
    before_each_test
    prepare_file abcd > "${DEVICE_FILE_A}"
    cp "${DEVICE_FILE_A}" "${ORIG_FILE_A}"
    prepare_file efgh > "${FILE_B}"
    prepare_file __cd > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_A="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_A}" "${DEVICE_FILE_A}" || error_exit "Could not setup '${LOOP_DEVICE_A}'." 3
    dmsetup create "${DEVICE_A}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((0 * SECTORS_PER_BLOCK))
$((1 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((2 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((2 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "/dev/mapper/${DEVICE_A}" --dest "${FILE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=2 --on-src-error=zeroize --ibuf-size="$((2 * BLOCK_SIZE))"B --obuf-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_A}"
    losetup -d "${LOOP_DEVICE_A}"
    check_exit_code "$R" 8
    check_unmodified_file "${ORIG_FILE_A}" "${DEVICE_FILE_A}"
    check_diff "${FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 2 1 0 1 0 2 0
    after_each_test
}

block_device_test_7c() {
    next_test "Block Device Test 7c (source file error, --on-src-error=zeroize, shorter dest file)"
    before_each_test
    prepare_file abcd > "${DEVICE_FILE_A}"
    cp "${DEVICE_FILE_A}" "${ORIG_FILE_A}"
    prepare_file efg > "${FILE_B}"
    prepare_file ab__ > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_A="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_A}" "${DEVICE_FILE_A}" || error_exit "Could not setup '${LOOP_DEVICE_A}'." 3
    dmsetup create "${DEVICE_A}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((0 * SECTORS_PER_BLOCK))
$((2 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) error
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "/dev/mapper/${DEVICE_A}" --dest "${FILE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=2 --on-src-error=zeroize --ibuf-size="$((2 * BLOCK_SIZE))"B --obuf-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_A}"
    losetup -d "${LOOP_DEVICE_A}"
    check_exit_code "$R" 8
    check_unmodified_file "${ORIG_FILE_A}" "${DEVICE_FILE_A}"
    check_diff "${FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 2 2 1 2 0 2 0
    after_each_test
}

block_device_test_7d() {
    next_test "Block Device Test 7d (source file error, --on-src-error=zeroize, longer dest file)"
    before_each_test
    prepare_file abc > "${DEVICE_FILE_A}"
    cp "${DEVICE_FILE_A}" "${ORIG_FILE_A}"
    prepare_file efgh > "${FILE_B}"
    prepare_file ab_ > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_A="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_A}" "${DEVICE_FILE_A}" || error_exit "Could not setup '${LOOP_DEVICE_A}'." 3
    dmsetup create "${DEVICE_A}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((0 * SECTORS_PER_BLOCK))
$((2 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "/dev/mapper/${DEVICE_A}" --dest "${FILE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=2 --on-src-error=zeroize --ibuf-size="$((2 * BLOCK_SIZE))"B --obuf-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_A}"
    losetup -d "${LOOP_DEVICE_A}"
    check_exit_code "$R" 8
    check_unmodified_file "${ORIG_FILE_A}" "${DEVICE_FILE_A}"
    check_diff "${FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 2 2 0 2 0 2 0
    after_each_test
}

block_device_test_7e() {
    next_test "Block Device Test 7e (source file error, --on-src-error=skip)"
    before_each_test
    prepare_file abcd > "${DEVICE_FILE_A}"
    cp "${DEVICE_FILE_A}" "${ORIG_FILE_A}"
    prepare_file efgh > "${FILE_B}"
    prepare_file efcd > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_A="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_A}" "${DEVICE_FILE_A}" || error_exit "Could not setup '${LOOP_DEVICE_A}'." 3
    dmsetup create "${DEVICE_A}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((0 * SECTORS_PER_BLOCK))
$((1 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((2 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((2 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "/dev/mapper/${DEVICE_A}" --dest "${FILE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=2 --on-src-error=skip --ibuf-size="$((2 * BLOCK_SIZE))"B --obuf-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_A}"
    losetup -d "${LOOP_DEVICE_A}"
    check_exit_code "$R" 8
    check_unmodified_file "${ORIG_FILE_A}" "${DEVICE_FILE_A}"
    check_diff "${FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 2 1 0 1 0 2 0
    after_each_test
}

block_device_test_7f() {
    next_test "Block Device Test 7f (source file error, --on-src-error=skip, shorter dest file)"
    before_each_test
    prepare_file abcd > "${DEVICE_FILE_A}"
    cp "${DEVICE_FILE_A}" "${ORIG_FILE_A}"
    prepare_file efg > "${FILE_B}"
    prepare_file abg_ > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_A="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_A}" "${DEVICE_FILE_A}" || error_exit "Could not setup '${LOOP_DEVICE_A}'." 3
    dmsetup create "${DEVICE_A}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((0 * SECTORS_PER_BLOCK))
$((2 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) error
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "/dev/mapper/${DEVICE_A}" --dest "${FILE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=2 --on-src-error=skip --ibuf-size="$((2 * BLOCK_SIZE))"B --obuf-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_A}"
    losetup -d "${LOOP_DEVICE_A}"
    check_exit_code "$R" 8
    check_unmodified_file "${ORIG_FILE_A}" "${DEVICE_FILE_A}"
    check_diff "${FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 2 2 1 1 0 2 0
    after_each_test
}

block_device_test_7f2() {
    next_test "Block Device Test 7f2 (source file error, --on-src-error=skip, shorter dest file, test 2)"
    before_each_test
    prepare_file abcd > "${DEVICE_FILE_A}"
    cp "${DEVICE_FILE_A}" "${ORIG_FILE_A}"
    prepare_file efg > "${FILE_B}"
    prepare_file abg_ > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_A="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_A}" "${DEVICE_FILE_A}" || error_exit "Could not setup '${LOOP_DEVICE_A}'." 3
    dmsetup create "${DEVICE_A}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((0 * SECTORS_PER_BLOCK))
$((2 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) error
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "/dev/mapper/${DEVICE_A}" --dest "${FILE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=2 --on-src-error=skip --ibuf-size="$((1 * BLOCK_SIZE))"B --obuf-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_A}"
    losetup -d "${LOOP_DEVICE_A}"
    check_exit_code "$R" 8
    check_unmodified_file "${ORIG_FILE_A}" "${DEVICE_FILE_A}"
    check_diff "${FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 2 1 1 1 0 2 0
    after_each_test
}

block_device_test_7g() {
    next_test "Block Device Test 7g (source file error, --on-src-error=skip, longer dest file)"
    before_each_test
    prepare_file abc > "${DEVICE_FILE_A}"
    cp "${DEVICE_FILE_A}" "${ORIG_FILE_A}"
    prepare_file efgh > "${FILE_B}"
    prepare_file abg > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_A="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_A}" "${DEVICE_FILE_A}" || error_exit "Could not setup '${LOOP_DEVICE_A}'." 3
    dmsetup create "${DEVICE_A}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((0 * SECTORS_PER_BLOCK))
$((2 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "/dev/mapper/${DEVICE_A}" --dest "${FILE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=2 --on-src-error=skip --ibuf-size="$((2 * BLOCK_SIZE))"B --obuf-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_A}"
    losetup -d "${LOOP_DEVICE_A}"
    check_exit_code "$R" 8
    check_unmodified_file "${ORIG_FILE_A}" "${DEVICE_FILE_A}"
    check_diff "${FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 2 1 0 1 0 2 0
    after_each_test
}

block_device_test_7h() {
    next_test "Block Device Test 7h (source file error, --on-src-error=skip, shorter dest file 2)"
    before_each_test
    prepare_file abcde > "${DEVICE_FILE_A}"
    cp "${DEVICE_FILE_A}" "${ORIG_FILE_A}"
    prepare_file fghi > "${FILE_B}"
    prepare_file abci_ > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_A="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_A}" "${DEVICE_FILE_A}" || error_exit "Could not setup '${LOOP_DEVICE_A}'." 3
    dmsetup create "${DEVICE_A}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((0 * SECTORS_PER_BLOCK))
$((3 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) error
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "/dev/mapper/${DEVICE_A}" --dest "${FILE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=2 --on-src-error=skip --ibuf-size="$((3 * BLOCK_SIZE))"B --obuf-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_A}"
    losetup -d "${LOOP_DEVICE_A}"
    check_exit_code "$R" 8
    check_unmodified_file "${ORIG_FILE_A}" "${DEVICE_FILE_A}"
    check_diff "${FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 2 2 1 1 0 2 0
    after_each_test
}

block_device_test_7i() {
    next_test "Block Device Test 7i (source file error, --on-src-error=zeroize, shorter dest file 2)"
    before_each_test
    prepare_file abcde > "${DEVICE_FILE_A}"
    cp "${DEVICE_FILE_A}" "${ORIG_FILE_A}"
    prepare_file fghi > "${FILE_B}"
    prepare_file abc__ > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_A="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_A}" "${DEVICE_FILE_A}" || error_exit "Could not setup '${LOOP_DEVICE_A}'." 3
    dmsetup create "${DEVICE_A}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((0 * SECTORS_PER_BLOCK))
$((3 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) error
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "/dev/mapper/${DEVICE_A}" --dest "${FILE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=2 --on-src-error=zeroize --ibuf-size="$((3 * BLOCK_SIZE))"B --obuf-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_A}"
    losetup -d "${LOOP_DEVICE_A}"
    check_exit_code "$R" 8
    check_unmodified_file "${ORIG_FILE_A}" "${DEVICE_FILE_A}"
    check_diff "${FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 2 2 1 2 0 2 0
    after_each_test
}

block_device_test_8a() {
    next_test "Block Device Test 8a (source and dest file error, --on-src-error=skip, --on-dest-error=abort, shorter dest file)"
    before_each_test
    prepare_file abcde > "${DEVICE_FILE_A}"
    cp "${DEVICE_FILE_A}" "${ORIG_FILE_A}"
    prepare_file fghi > "${DEVICE_FILE_B}"
    prepare_file abc_ > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_A="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_A}" "${DEVICE_FILE_A}" || error_exit "Could not setup '${LOOP_DEVICE_A}'." 3
    dmsetup create "${DEVICE_A}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((0 * SECTORS_PER_BLOCK))
$((3 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) error
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    local LOOP_DEVICE_B="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_B}" "${DEVICE_FILE_B}" || error_exit "Could not setup '${LOOP_DEVICE_B}'." 3
    dmsetup create "${DEVICE_B}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((0 * SECTORS_PER_BLOCK))
$((3 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "/dev/mapper/${DEVICE_A}" --dest "/dev/mapper/${DEVICE_B}" --force --block-size="${BLOCK_SIZE}"B --max-passes=2 --on-src-error=skip --on-dest-error=abort --ibuf-size="$((3 * BLOCK_SIZE))"B --obuf-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_A}"
    losetup -d "${LOOP_DEVICE_A}"
    dmsetup remove "${DEVICE_B}"
    losetup -d "${LOOP_DEVICE_B}"
    check_exit_code "$R" 65
    check_unmodified_file "${ORIG_FILE_A}" "${DEVICE_FILE_A}"
    after_each_test
}

block_device_test_8b() {
    next_test "Block Device Test 8b (source and dest file error, --on-src-error=skip, --on-dest-error=skip, shorter dest file)"
    before_each_test
    prepare_file abcde > "${DEVICE_FILE_A}"
    cp "${DEVICE_FILE_A}" "${ORIG_FILE_A}"
    prepare_file fghi > "${DEVICE_FILE_B}"
    prepare_file abci > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_A="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_A}" "${DEVICE_FILE_A}" || error_exit "Could not setup '${LOOP_DEVICE_A}'." 3
    dmsetup create "${DEVICE_A}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((0 * SECTORS_PER_BLOCK))
$((3 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) error
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    local LOOP_DEVICE_B="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_B}" "${DEVICE_FILE_B}" || error_exit "Could not setup '${LOOP_DEVICE_B}'." 3
    dmsetup create "${DEVICE_B}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((0 * SECTORS_PER_BLOCK))
$((3 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "/dev/mapper/${DEVICE_A}" --dest "/dev/mapper/${DEVICE_B}" --force --block-size="${BLOCK_SIZE}"B --max-passes=2 --on-src-error=skip --on-dest-error=skip --ibuf-size="$((3 * BLOCK_SIZE))"B --obuf-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_A}"
    losetup -d "${LOOP_DEVICE_A}"
    dmsetup remove "${DEVICE_B}"
    losetup -d "${LOOP_DEVICE_B}"
    check_exit_code "$R" 8
    check_unmodified_file "${ORIG_FILE_A}" "${DEVICE_FILE_A}"
    check_diff "${DEVICE_FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 2 1 0 1 0 2 0
    after_each_test
}

block_device_test_9a() {
    next_test "Block Device Test 9a (mixed, dest=too short block device, --on-src-error=skip, --on-dest-error=skip)"
    before_each_test
    prepare_file abcdefghijklmnop > "${DEVICE_FILE_A}"
    cp "${DEVICE_FILE_A}" "${ORIG_FILE_A}"
    prepare_file a1c3e5g7i9k > "${DEVICE_FILE_B}"
    prepare_file a1c3e5ghi9k > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_A="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_A}" "${DEVICE_FILE_A}" || error_exit "Could not setup '${LOOP_DEVICE_A}'." 3
    dmsetup create "${DEVICE_A}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((0 * SECTORS_PER_BLOCK))
$((1 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((2 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((2 * SECTORS_PER_BLOCK))
$((5 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((6 * SECTORS_PER_BLOCK)) $((8 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((6 * SECTORS_PER_BLOCK))
$((14 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((15 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((15 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    local LOOP_DEVICE_B="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_B}" "${DEVICE_FILE_B}" || error_exit "Could not setup '${LOOP_DEVICE_B}'." 3
    dmsetup create "${DEVICE_B}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((0 * SECTORS_PER_BLOCK))
$((3 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((4 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((4 * SECTORS_PER_BLOCK))
$((5 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((6 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((6 * SECTORS_PER_BLOCK))
$((9 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) zero
$((10 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((10 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "/dev/mapper/${DEVICE_A}" --dest "/dev/mapper/${DEVICE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=2 --force --on-src-error=skip --on-dest-error=skip --ibuf-size="$((1 * BLOCK_SIZE))"B --obuf-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_A}"
    losetup -d "${LOOP_DEVICE_A}"
    dmsetup remove "${DEVICE_B}"
    losetup -d "${LOOP_DEVICE_B}"
    check_exit_code "$R" 65
    check_unmodified_file "${ORIG_FILE_A}" "${DEVICE_FILE_A}"
    after_each_test
}

block_device_test_9b() {
    next_test "Block Device Test 9b (mixed, dest=block device, --on-src-error=skip, --on-dest-error=skip)"
    before_each_test
    prepare_file abcdefghijk > "${DEVICE_FILE_A}"
    cp "${DEVICE_FILE_A}" "${ORIG_FILE_A}"
    prepare_file a1c3e5g7i9k > "${DEVICE_FILE_B}"
    prepare_file a1c3e5ghi9k > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_A="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_A}" "${DEVICE_FILE_A}" || error_exit "Could not setup '${LOOP_DEVICE_A}'." 3
    dmsetup create "${DEVICE_A}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((0 * SECTORS_PER_BLOCK))
$((1 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((2 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((2 * SECTORS_PER_BLOCK))
$((5 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((6 * SECTORS_PER_BLOCK)) $((5 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((6 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    local LOOP_DEVICE_B="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_B}" "${DEVICE_FILE_B}" || error_exit "Could not setup '${LOOP_DEVICE_B}'." 3
    dmsetup create "${DEVICE_B}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((0 * SECTORS_PER_BLOCK))
$((3 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((4 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((4 * SECTORS_PER_BLOCK))
$((5 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((6 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((6 * SECTORS_PER_BLOCK))
$((9 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) zero
$((10 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((10 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "/dev/mapper/${DEVICE_A}" --dest "/dev/mapper/${DEVICE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=2 --force --on-src-error=skip --on-dest-error=skip --ibuf-size="$((1 * BLOCK_SIZE))"B --obuf-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_A}"
    losetup -d "${LOOP_DEVICE_A}"
    dmsetup remove "${DEVICE_B}"
    losetup -d "${LOOP_DEVICE_B}"
    check_exit_code "$R" 12
    check_unmodified_file "${ORIG_FILE_A}" "${DEVICE_FILE_A}"
    check_diff "${DEVICE_FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 2 11 0 1 2 4 2
    after_each_test
}

block_device_test_9b2() {
    next_test "Block Device Test 9b2 (mixed, dest=block device, --on-src-error=skip, --on-dest-error=skip, --copy-first)"
    before_each_test
    prepare_file abcdefghijk > "${DEVICE_FILE_A}"
    cp "${DEVICE_FILE_A}" "${ORIG_FILE_A}"
    prepare_file a1c3e5g7i9k > "${DEVICE_FILE_B}"
    prepare_file a1c3e5ghi9k > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_A="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_A}" "${DEVICE_FILE_A}" || error_exit "Could not setup '${LOOP_DEVICE_A}'." 3
    dmsetup create "${DEVICE_A}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((0 * SECTORS_PER_BLOCK))
$((1 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((2 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((2 * SECTORS_PER_BLOCK))
$((5 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((6 * SECTORS_PER_BLOCK)) $((5 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((6 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    local LOOP_DEVICE_B="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_B}" "${DEVICE_FILE_B}" || error_exit "Could not setup '${LOOP_DEVICE_B}'." 3
    dmsetup create "${DEVICE_B}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((0 * SECTORS_PER_BLOCK))
$((3 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((4 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((4 * SECTORS_PER_BLOCK))
$((5 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((6 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((6 * SECTORS_PER_BLOCK))
$((9 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) zero
$((10 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((10 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "/dev/mapper/${DEVICE_A}" --dest "/dev/mapper/${DEVICE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=2 --copy-first --force --on-src-error=skip --on-dest-error=skip --ibuf-size="$((1 * BLOCK_SIZE))"B --obuf-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_A}"
    losetup -d "${LOOP_DEVICE_A}"
    dmsetup remove "${DEVICE_B}"
    losetup -d "${LOOP_DEVICE_B}"
    check_exit_code "$R" 12
    check_unmodified_file "${ORIG_FILE_A}" "${DEVICE_FILE_A}"
    check_diff "${DEVICE_FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 2 5 0 3 1 4 1
    after_each_test
}

block_device_test_9c() {
    next_test "Block Device Test 9c (mixed, dest=block device, --on-src-error=zeroize, --on-dest-error=skip)"
    before_each_test
    prepare_file abcdefghijk > "${DEVICE_FILE_A}"
    cp "${DEVICE_FILE_A}" "${ORIG_FILE_A}"
    prepare_file a1c3e5g7i9k > "${DEVICE_FILE_B}"
    prepare_file a_c3e5ghi9k > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_A="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_A}" "${DEVICE_FILE_A}" || error_exit "Could not setup '${LOOP_DEVICE_A}'." 3
    dmsetup create "${DEVICE_A}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((0 * SECTORS_PER_BLOCK))
$((1 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((2 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((2 * SECTORS_PER_BLOCK))
$((5 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((6 * SECTORS_PER_BLOCK)) $((5 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((6 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    local LOOP_DEVICE_B="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_B}" "${DEVICE_FILE_B}" || error_exit "Could not setup '${LOOP_DEVICE_B}'." 3
    dmsetup create "${DEVICE_B}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((0 * SECTORS_PER_BLOCK))
$((3 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((4 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((4 * SECTORS_PER_BLOCK))
$((5 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((6 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((6 * SECTORS_PER_BLOCK))
$((9 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) zero
$((10 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((10 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "/dev/mapper/${DEVICE_A}" --dest "/dev/mapper/${DEVICE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=2 --force --on-src-error=zeroize --on-dest-error=skip --ibuf-size="$((1 * BLOCK_SIZE))"B --obuf-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_A}"
    losetup -d "${LOOP_DEVICE_A}"
    dmsetup remove "${DEVICE_B}"
    losetup -d "${LOOP_DEVICE_B}"
    check_exit_code "$R" 12
    check_unmodified_file "${ORIG_FILE_A}" "${DEVICE_FILE_A}"
    check_diff "${DEVICE_FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 2 11 0 2 2 4 4
    after_each_test
}

block_device_test_9c2() {
    next_test "Block Device Test 9c2 (mixed, dest=block device, --on-src-error=zeroize, --on-dest-error=skip, --copy-first)"
    before_each_test
    prepare_file abcdefghijk > "${DEVICE_FILE_A}"
    cp "${DEVICE_FILE_A}" "${ORIG_FILE_A}"
    prepare_file a1c3e5g7i9k > "${DEVICE_FILE_B}"
    prepare_file a_c3e5ghi9k > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_A="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_A}" "${DEVICE_FILE_A}" || error_exit "Could not setup '${LOOP_DEVICE_A}'." 3
    dmsetup create "${DEVICE_A}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((0 * SECTORS_PER_BLOCK))
$((1 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((2 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((2 * SECTORS_PER_BLOCK))
$((5 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((6 * SECTORS_PER_BLOCK)) $((5 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((6 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    local LOOP_DEVICE_B="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_B}" "${DEVICE_FILE_B}" || error_exit "Could not setup '${LOOP_DEVICE_B}'." 3
    dmsetup create "${DEVICE_B}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((0 * SECTORS_PER_BLOCK))
$((3 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((4 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((4 * SECTORS_PER_BLOCK))
$((5 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((6 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((6 * SECTORS_PER_BLOCK))
$((9 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) zero
$((10 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((10 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "/dev/mapper/${DEVICE_A}" --dest "/dev/mapper/${DEVICE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=2 --copy-first --force --on-src-error=zeroize --on-dest-error=skip --ibuf-size="$((1 * BLOCK_SIZE))"B --obuf-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_A}"
    losetup -d "${LOOP_DEVICE_A}"
    dmsetup remove "${DEVICE_B}"
    losetup -d "${LOOP_DEVICE_B}"
    check_exit_code "$R" 12
    check_unmodified_file "${ORIG_FILE_A}" "${DEVICE_FILE_A}"
    check_diff "${DEVICE_FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 2 5 0 3 1 4 2
    after_each_test
}

block_device_test_10a() {
    next_test "Block Device Test 10a (mixed, dest=file, --on-src-error=skip, --on-dest-error=skip)"
    before_each_test
    prepare_file abcdefghijklmnop > "${DEVICE_FILE_A}"
    cp "${DEVICE_FILE_A}" "${ORIG_FILE_A}"
    prepare_file a1c3e5g7i9k > "${FILE_B}"
    prepare_file a1cde5ghijklmn_p > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_A="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_A}" "${DEVICE_FILE_A}" || error_exit "Could not setup '${LOOP_DEVICE_A}'." 3
    dmsetup create "${DEVICE_A}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((0 * SECTORS_PER_BLOCK))
$((1 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((2 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((2 * SECTORS_PER_BLOCK))
$((5 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((6 * SECTORS_PER_BLOCK)) $((8 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((6 * SECTORS_PER_BLOCK))
$((14 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((15 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((15 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "/dev/mapper/${DEVICE_A}" --dest "${FILE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=2 --force --on-src-error=skip --on-dest-error=skip --ibuf-size="$((1 * BLOCK_SIZE))"B --obuf-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_A}"
    losetup -d "${LOOP_DEVICE_A}"
    check_exit_code "$R" 8
    check_unmodified_file "${ORIG_FILE_A}" "${DEVICE_FILE_A}"
    check_diff "${FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 2 10 2 3 0 6 0
    after_each_test
}

block_device_test_10a2() {
    next_test "Block Device Test 10a2 (mixed, dest=file, --on-src-error=skip, --on-dest-error=skip, --copy-first)"
    before_each_test
    prepare_file abcdefghijklmnop > "${DEVICE_FILE_A}"
    cp "${DEVICE_FILE_A}" "${ORIG_FILE_A}"
    prepare_file a1c3e5g7i9k > "${FILE_B}"
    prepare_file a1cde5ghijklmn_p > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_A="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_A}" "${DEVICE_FILE_A}" || error_exit "Could not setup '${LOOP_DEVICE_A}'." 3
    dmsetup create "${DEVICE_A}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((0 * SECTORS_PER_BLOCK))
$((1 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((2 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((2 * SECTORS_PER_BLOCK))
$((5 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((6 * SECTORS_PER_BLOCK)) $((8 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((6 * SECTORS_PER_BLOCK))
$((14 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((15 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((15 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "/dev/mapper/${DEVICE_A}" --dest "${FILE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=2 --copy-first --force --on-src-error=skip --on-dest-error=skip --ibuf-size="$((1 * BLOCK_SIZE))"B --obuf-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_A}"
    losetup -d "${LOOP_DEVICE_A}"
    check_exit_code "$R" 8
    check_unmodified_file "${ORIG_FILE_A}" "${DEVICE_FILE_A}"
    check_diff "${FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 2 4 2 3 0 6 0
    after_each_test
}

block_device_test_10b() {
    next_test "Block Device Test 10b (mixed, dest=file, --on-src-error=zeroize, --on-dest-error=skip)"
    before_each_test
    prepare_file abcdefghijklmnop > "${DEVICE_FILE_A}"
    cp "${DEVICE_FILE_A}" "${ORIG_FILE_A}"
    prepare_file a1c3e5g7i9k > "${FILE_B}"
    prepare_file a_cde_ghijklmn_p > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_A="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_A}" "${DEVICE_FILE_A}" || error_exit "Could not setup '${LOOP_DEVICE_A}'." 3
    dmsetup create "${DEVICE_A}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((0 * SECTORS_PER_BLOCK))
$((1 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((2 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((2 * SECTORS_PER_BLOCK))
$((5 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((6 * SECTORS_PER_BLOCK)) $((8 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((6 * SECTORS_PER_BLOCK))
$((14 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((15 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((15 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "/dev/mapper/${DEVICE_A}" --dest "${FILE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=2 --force --on-src-error=zeroize --on-dest-error=skip --ibuf-size="$((1 * BLOCK_SIZE))"B --obuf-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_A}"
    losetup -d "${LOOP_DEVICE_A}"
    check_exit_code "$R" 8
    check_unmodified_file "${ORIG_FILE_A}" "${DEVICE_FILE_A}"
    check_diff "${FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 2 10 2 5 0 6 0
    after_each_test
}

block_device_test_10b2() {
    next_test "Block Device Test 10b2 (mixed, dest=file, --on-src-error=zeroize, --on-dest-error=skip, --copy-first)"
    before_each_test
    prepare_file abcdefghijklmnop > "${DEVICE_FILE_A}"
    cp "${DEVICE_FILE_A}" "${ORIG_FILE_A}"
    prepare_file a1c3e5g7i9k > "${FILE_B}"
    prepare_file a_cde_ghijklmn_p > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_A="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_A}" "${DEVICE_FILE_A}" || error_exit "Could not setup '${LOOP_DEVICE_A}'." 3
    dmsetup create "${DEVICE_A}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((0 * SECTORS_PER_BLOCK))
$((1 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((2 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((2 * SECTORS_PER_BLOCK))
$((5 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((6 * SECTORS_PER_BLOCK)) $((8 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((6 * SECTORS_PER_BLOCK))
$((14 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((15 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((15 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "/dev/mapper/${DEVICE_A}" --dest "${FILE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=2 --copy-first --force --on-src-error=zeroize --on-dest-error=skip --ibuf-size="$((1 * BLOCK_SIZE))"B --obuf-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_A}"
    losetup -d "${LOOP_DEVICE_A}"
    check_exit_code "$R" 8
    check_unmodified_file "${ORIG_FILE_A}" "${DEVICE_FILE_A}"
    check_diff "${FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 2 4 2 3 0 6 0
    after_each_test
}

block_device_test_11a() {
    next_test "Block Device Test 11a (mixed, dest=block device, --on-src-error=zeroize, --on-dest-error=skip, larger ranges of error)"
    before_each_test
    prepare_file abcdefghijklmnopqrstu > "${DEVICE_FILE_A}"
    cp "${DEVICE_FILE_A}" "${ORIG_FILE_A}"
    prepare_file abc123456789012p3r012 > "${DEVICE_FILE_B}"
    prepare_file abc123456789012pqr___ > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_A="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_A}" "${DEVICE_FILE_A}" || error_exit "Could not setup '${LOOP_DEVICE_A}'." 3
    dmsetup create "${DEVICE_A}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((0 * SECTORS_PER_BLOCK))
$((3 * SECTORS_PER_BLOCK)) $((12 * SECTORS_PER_BLOCK)) error
$((15 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((15 * SECTORS_PER_BLOCK))
$((18 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) error
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    local LOOP_DEVICE_B="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_B}" "${DEVICE_FILE_B}" || error_exit "Could not setup '${LOOP_DEVICE_B}'." 3
    dmsetup create "${DEVICE_B}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((4 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((0 * SECTORS_PER_BLOCK))
$((4 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((5 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((5 * SECTORS_PER_BLOCK))
$((6 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((7 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((7 * SECTORS_PER_BLOCK))
$((8 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((9 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((9 * SECTORS_PER_BLOCK))
$((11 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) error
$((13 * SECTORS_PER_BLOCK)) $((8 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((13 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "/dev/mapper/${DEVICE_A}" --dest "/dev/mapper/${DEVICE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=2 --force --on-src-error=zeroize --on-dest-error=skip --ibuf-size="$((3 * BLOCK_SIZE))"B --obuf-size="$((1 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_A}"
    losetup -d "${LOOP_DEVICE_A}"
    dmsetup remove "${DEVICE_B}"
    losetup -d "${LOOP_DEVICE_B}"
    check_exit_code "$R" 8
    check_unmodified_file "${ORIG_FILE_A}" "${DEVICE_FILE_A}"
    check_diff "${DEVICE_FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 2 6 0 2 0 10 8
    after_each_test
}

block_device_test_11b() {
    next_test "Block Device Test 11b (mixed, dest=block device, --on-src-error=zeroize, --on-dest-error=skip, --copy-first, larger ranges of error)"
    before_each_test
    prepare_file abcdefghijklmnopqrstu > "${DEVICE_FILE_A}"
    cp "${DEVICE_FILE_A}" "${ORIG_FILE_A}"
    prepare_file abc123456789012p3r012 > "${DEVICE_FILE_B}"
    prepare_file abc_2_4_6__90__pqr___ > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_A="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_A}" "${DEVICE_FILE_A}" || error_exit "Could not setup '${LOOP_DEVICE_A}'." 3
    dmsetup create "${DEVICE_A}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((0 * SECTORS_PER_BLOCK))
$((3 * SECTORS_PER_BLOCK)) $((12 * SECTORS_PER_BLOCK)) error
$((15 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((15 * SECTORS_PER_BLOCK))
$((18 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) error
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    local LOOP_DEVICE_B="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_B}" "${DEVICE_FILE_B}" || error_exit "Could not setup '${LOOP_DEVICE_B}'." 3
    dmsetup create "${DEVICE_B}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((4 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((0 * SECTORS_PER_BLOCK))
$((4 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((5 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((5 * SECTORS_PER_BLOCK))
$((6 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((7 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((7 * SECTORS_PER_BLOCK))
$((8 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((9 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((9 * SECTORS_PER_BLOCK))
$((11 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) error
$((13 * SECTORS_PER_BLOCK)) $((8 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((13 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "/dev/mapper/${DEVICE_A}" --dest "/dev/mapper/${DEVICE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=2 --copy-first --force --on-src-error=zeroize --on-dest-error=skip --ibuf-size="$((3 * BLOCK_SIZE))"B --obuf-size="$((1 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_A}"
    losetup -d "${LOOP_DEVICE_A}"
    dmsetup remove "${DEVICE_B}"
    losetup -d "${LOOP_DEVICE_B}"
    check_exit_code "$R" 8
    check_unmodified_file "${ORIG_FILE_A}" "${DEVICE_FILE_A}"
    check_diff "${DEVICE_FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 2 3 0 6 0 10 4
    after_each_test
}

block_device_test_11c() {
    next_test "Block Device Test 11c (mixed, dest=block device, --on-src-error=zeroize, --on-dest-error=skip, --copy-first, larger ranges of error, larger obuf-size)"
    before_each_test
    prepare_file abcdefghijklmnopqrstu > "${DEVICE_FILE_A}"
    cp "${DEVICE_FILE_A}" "${ORIG_FILE_A}"
    prepare_file abc123456789012p3r012 > "${DEVICE_FILE_B}"
    prepare_file abc_2_4_6__90__pqr___ > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_A="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_A}" "${DEVICE_FILE_A}" || error_exit "Could not setup '${LOOP_DEVICE_A}'." 3
    dmsetup create "${DEVICE_A}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((0 * SECTORS_PER_BLOCK))
$((3 * SECTORS_PER_BLOCK)) $((12 * SECTORS_PER_BLOCK)) error
$((15 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_A} $((15 * SECTORS_PER_BLOCK))
$((18 * SECTORS_PER_BLOCK)) $((3 * SECTORS_PER_BLOCK)) error
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    local LOOP_DEVICE_B="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_B}" "${DEVICE_FILE_B}" || error_exit "Could not setup '${LOOP_DEVICE_B}'." 3
    dmsetup create "${DEVICE_B}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((4 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((0 * SECTORS_PER_BLOCK))
$((4 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((5 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((5 * SECTORS_PER_BLOCK))
$((6 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((7 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((7 * SECTORS_PER_BLOCK))
$((8 * SECTORS_PER_BLOCK)) $((1 * SECTORS_PER_BLOCK)) error
$((9 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((9 * SECTORS_PER_BLOCK))
$((11 * SECTORS_PER_BLOCK)) $((2 * SECTORS_PER_BLOCK)) error
$((13 * SECTORS_PER_BLOCK)) $((8 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((13 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src "/dev/mapper/${DEVICE_A}" --dest "/dev/mapper/${DEVICE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=2 --copy-first --force --on-src-error=zeroize --on-dest-error=skip --ibuf-size="$((3 * BLOCK_SIZE))"B --obuf-size="$((3 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_A}"
    losetup -d "${LOOP_DEVICE_A}"
    dmsetup remove "${DEVICE_B}"
    losetup -d "${LOOP_DEVICE_B}"
    check_exit_code "$R" 8
    check_unmodified_file "${ORIG_FILE_A}" "${DEVICE_FILE_A}"
    check_diff "${DEVICE_FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 2 3 0 6 0 10 4
    after_each_test
}

block_device_test_12() {
    next_test "Block Device Test 12 (zeroizing device)"
    before_each_test
    prepare_file 1_34 > "${DEVICE_FILE_B}"
    prepare_file ____ > "${DESIRED_FILE_B}"
    local LOOP_DEVICE_B="$(find_free_loop_device)" || error_exit "No free loop device found." 3
    losetup "${LOOP_DEVICE_B}" "${DEVICE_FILE_B}" || error_exit "Could not setup '${LOOP_DEVICE_B}'." 3
    dmsetup create "${DEVICE_B}" << EOF
$((0 * SECTORS_PER_BLOCK)) $((4 * SECTORS_PER_BLOCK)) linear ${LOOP_DEVICE_B} $((0 * SECTORS_PER_BLOCK))
EOF
    [ "$?" = 0 ] || error_exit "Could not setup dm" 3
    "${UNDER_TEST}" --src /dev/zero --dest "/dev/mapper/${DEVICE_B}" --block-size="${BLOCK_SIZE}"B --ibuf-size="${BLOCK_SIZE}"B --obuf-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    dmsetup remove "${DEVICE_B}"
    losetup -d "${LOOP_DEVICE_B}"
    check_exit_code "$R" 65
    check_diff "${DEVICE_FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 1 0 2 0 0 0
    after_each_test
}

# run tests
block_device_test_1
block_device_test_2a
block_device_test_2b
block_device_test_2c
block_device_test_3
block_device_test_4
block_device_test_5
block_device_test_6a
block_device_test_6b
block_device_test_6c
block_device_test_6d
block_device_test_6e
block_device_test_6f
block_device_test_6g
block_device_test_6h
block_device_test_6i
block_device_test_7a
block_device_test_7b
block_device_test_7c
block_device_test_7d
block_device_test_7e
block_device_test_7f
block_device_test_7f2
block_device_test_7g
block_device_test_7h
block_device_test_7i
block_device_test_8a
block_device_test_8b
block_device_test_9a
block_device_test_9b
block_device_test_9b2
block_device_test_9c
block_device_test_9c2
block_device_test_10a
block_device_test_10a2
block_device_test_10b
block_device_test_10b2
block_device_test_11a
block_device_test_11b
block_device_test_11c
block_device_test_12



### file system tests ###

file_system_test_1a() {
    next_test "File System Test 1a (exceeding file system space)"
    before_each_test
    prepare_file aaaaaaaaaaaaaaaa > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    dd if=/dev/zero of="${DEVICE_FILE_C}" bs=64K count=1 status=none || error_exit "Could not create device file." 3
    mkfs.fat "${DEVICE_FILE_C}" > /dev/null || error_exit "Could not create FAT." 3
    mount "${DEVICE_FILE_C}" "${FS_C}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest "${FS_C_FILE_B}" --block-size=4KiB --ibuf-size=4KiB --obuf-size=4KiB --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    umount "${FS_C}" || error_exit "Could not umount ${FS_C}" 3
    check_exit_code "$R" 65
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    after_each_test
}

file_system_test_1b() {
    next_test "File System Test 1b (exceeding file system space, --on-dest-error=skip)"
    before_each_test
    prepare_file aaaaaaaaaaaaaaaa > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    dd if=/dev/zero of="${DEVICE_FILE_C}" bs=64K count=1 status=none || error_exit "Could not create device file." 3
    mkfs.fat "${DEVICE_FILE_C}" > /dev/null || error_exit "Could not create FAT." 3
    mount "${DEVICE_FILE_C}" "${FS_C}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest "${FS_C_FILE_B}" --on-dest-error=skip --block-size=4KiB --ibuf-size=4KiB --obuf-size=4KiB --block-log-file "${BLOCK_LOG_FILE}"
    R="$?"
    umount "${FS_C}" || error_exit "Could not umount ${FS_C}" 3
    check_exit_code "$R" 65
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    after_each_test
}

file_system_test_1a
file_system_test_1b

