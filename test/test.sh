#!/usr/bin/env bash

# ddpolymerase: Copy, repair and verify a file
# Copyright (C) 2021-2023  Tobias Killer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

source ./test_common.sh



### file tests ###

file_test_1a() {
    next_test "File Test 1a (src=emptyfile, no --force)"
    before_each_test
    prepare_file '' > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest "${FILE_B}" --max-passes=1 --block-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 65
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    after_each_test
}

file_test_1b() {
    next_test "File Test 1b (src=emptyfile, dest=emptyfile, --force)"
    before_each_test
    prepare_file '' > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest "${FILE_B}" --max-passes=1 --force --block-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 0
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 0 0 0 0 0
    after_each_test
}

file_test_1c() {
    next_test "File Test 1c (src=emptyfile, dest=file, --force)"
    before_each_test
    prepare_file '' > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    prepare_file 'hellos' > "${FILE_B}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest "${FILE_B}" --max-passes=1 --force --block-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 0
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 0 0 0 0 0
    after_each_test
}

file_test_2a() {
    next_test "File Test 2a (full copy, just one pass)"
    before_each_test
    prepare_file "hellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest "${FILE_B}" --max-passes=1 --block-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

file_test_2b() {
    next_test "File Test 2b (full copy, arbitrary passes)"
    before_each_test
    prepare_file "hellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest "${FILE_B}" --block-size="${BLOCK_SIZE}"B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 0
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 2 1 1 0 0 0 0
    after_each_test
}

file_test_3a() {
    next_test "File Test 3a (copy and repair)"
    before_each_test
    prepare_file "hellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    prepare_file "hals" > "${FILE_B}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest "${FILE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=1 --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 2 1 2 0 0 0
    after_each_test
}

file_test_3b() {
    next_test "File Test 3b (copy and repair zero data)"
    before_each_test
    prepare_file "h_llos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    prepare_file "h_ls" > "${FILE_B}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest "${FILE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=1 --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 1 1 1 0 0 0
    after_each_test
}

file_test_3c() {
    next_test "File Test 3c (copy and repair zero data 2)"
    before_each_test
    prepare_file "h_llos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    prepare_file "hals" > "${FILE_B}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest "${FILE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=1 --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 2 1 2 0 0 0
    after_each_test
}

file_test_3d() {
    next_test "File Test 3d (copy and repair zero data 3)"
    before_each_test
    prepare_file "hellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    prepare_file "h_ls" > "${FILE_B}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest "${FILE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=1 --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 2 1 2 0 0 0
    after_each_test
}

file_test_4() {
    next_test "File Test 4 (partial copy)"
    before_each_test
    prepare_file hello > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    prepare_file hel > "${FILE_B}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest "${FILE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=1 --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 1 1 0 0 0 0
    after_each_test
}

file_test_5a() {
    next_test "File Test 5a (no bad block; abort on bad block)"
    before_each_test
    prepare_file hello > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    cp "${FILE_A}" "${FILE_B}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest "${FILE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=1 --abort-on-bad-block --max-tries=0 --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 0
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 1 0 0 0 0 0
    after_each_test
}

file_test_5b() {
    next_test "File Test 5b (no blocks; abort on bad block)"
    before_each_test
    prepare_file hello > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    prepare_file "" > "${DESIRED_FILE_B}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest "${FILE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=1 --abort-on-bad-block --max-tries=0 --ibuf-size=1B --obuf-size=1B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 5
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 1
    check_diff "${FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 0 0 1 0 0
    after_each_test
}

file_test_5c() {
    next_test "File Test 5c (a few blocks; abort on bad block)"
    before_each_test
    prepare_file hello > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    prepare_file hal > "${FILE_B}"
    cp "${FILE_B}" "${DESIRED_FILE_B}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest "${FILE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=1 --abort-on-bad-block --max-tries=0 --ibuf-size=1B --obuf-size=1B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 5
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 1
    check_diff "${FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 1 0 0 1 0 0
    after_each_test
}

file_test_6a() {
    next_test "File Test 6a (no blocks; copy first)"
    before_each_test
    prepare_file hello > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest "${FILE_B}" --block-size="${BLOCK_SIZE}"B --copy-first --ibuf-size=2B --obuf-size=2B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 0
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 2 1 1 0 0 0 0
    after_each_test
}

file_test_6b() {
    next_test "File Test 6b (a few blocks; copy first)"
    before_each_test
    prepare_file hello > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    prepare_file hel > "${FILE_B}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest "${FILE_B}" --block-size="${BLOCK_SIZE}"B --copy-first --ibuf-size=2B --obuf-size=2B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 0
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 2 1 1 1 0 0 0
    after_each_test
}

file_test_6c() {
    next_test "File Test 6c (more blocks; copy first)"
    before_each_test
    prepare_file hello > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    prepare_file helloandgoodbye > "${FILE_B}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest "${FILE_B}" --block-size="${BLOCK_SIZE}"B --copy-first --ibuf-size=2B --obuf-size=2B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 0
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 2 1 0 1 0 0 0
    after_each_test
}

file_test_7a() {
    next_test "File Test 7a (exit status 0)"
    before_each_test
    prepare_file hello > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    cp "${FILE_A}" "${FILE_B}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest "${FILE_B}" --block-size="${BLOCK_SIZE}"B --max-tries=0 --max-passes=1 --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 0
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 1 0 0 0 0 0
    after_each_test
}

file_test_7b() {
    next_test "File Test 7b (exit status 5)"
    before_each_test
    prepare_file hello > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    prepare_file hel > "${FILE_B}"
    cp "${FILE_B}" "${DESIRED_FILE_B}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest "${FILE_B}" --block-size="${BLOCK_SIZE}"B --max-tries=0 --max-passes=1 --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 5
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 1
    check_diff "${FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 1 0 0 1 0 0
    after_each_test
}

file_test_8a() {
    next_test "File Test 8a (partial buffer)"
    before_each_test
    echo -n abclo > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    echo -n hello > "${FILE_B}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest "${FILE_B}" --block-size=1B --max-passes=1 --ibuf-size=2B --obuf-size=2B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 2 0 2 0 0 0
    after_each_test
}

file_test_8b() {
    next_test "File Test 8b (partial block)"
    before_each_test
    echo -n abclo > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    echo -n hello > "${FILE_B}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest "${FILE_B}" --block-size=2B --max-passes=1 --ibuf-size=2B --obuf-size=2B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 1 0 1 0 0 0
    after_each_test
}

file_test_9a() {
    next_test "File Test 9a (--stat-unit=B)"
    before_each_test
    prepare_file "hellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest "${FILE_B}" --max-passes=1 --block-size="${BLOCK_SIZE}"B --stats-unit=B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

file_test_9b() {
    next_test "File Test 9b (--stat-unit=blk)"
    before_each_test
    prepare_file "hellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest "${FILE_B}" --max-passes=1 --block-size="${BLOCK_SIZE}"B --stats-unit=blk --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

file_test_10a() {
    next_test "File Test 10a (--buf-size)"
    before_each_test
    prepare_file "hellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest "${FILE_B}" --max-passes=1 --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

file_test_10b() {
    next_test "File Test 10b (--buf-size --ibuf-size)"
    before_each_test
    prepare_file "hellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest "${FILE_B}" --max-passes=1 --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --ibuf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 64
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    after_each_test
}

file_test_10c() {
    next_test "File Test 10c (--buf-size --obuf-size)"
    before_each_test
    prepare_file "hellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest "${FILE_B}" --max-passes=1 --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --obuf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 64
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    after_each_test
}

file_test_11() {
    next_test "File Test 11 (--max-passes=0)"
    before_each_test
    prepare_file "hellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest "${FILE_B}" --max-passes=0 --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --ibuf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 64
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    after_each_test
}

file_test_12() {
    next_test "File Test 12 (--copy-first --max-tries=0)"
    before_each_test
    prepare_file "hellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest "${FILE_B}" --max-passes=1 --block-size="${BLOCK_SIZE}"B --copy-first --max-tries=0 --buf-size="$((2 * BLOCK_SIZE))"B --ibuf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 64
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    after_each_test
}

file_test_13a() {
    next_test "File Test 13a (no read access on source)"
    before_each_test
    prepare_file "hellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    chmod a-r "${FILE_A}"
    prepare_file "hals" > "${FILE_B}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest "${FILE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=1 --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 65
    after_each_test
}

file_test_13b() {
    next_test "File Test 13b (no read access on destination)"
    before_each_test
    prepare_file "hellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    prepare_file "hals" > "${FILE_B}"
    chmod a-r "${FILE_B}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest "${FILE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=1 --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 65
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 1
    after_each_test
}

file_test_13c() {
    next_test "File Test 13c (no write access on destination)"
    before_each_test
    prepare_file "hellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    prepare_file "hals" > "${FILE_B}"
    chmod a-w "${FILE_B}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest "${FILE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=1 --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 65
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 1
    after_each_test
}

file_test_13d() {
    next_test "File Test 13d (no write access on destination, --on-dest-error=skip)"
    before_each_test
    prepare_file "hellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    prepare_file "hals" > "${FILE_B}"
    cp "${FILE_B}" "${DESIRED_FILE_B}"
    chmod a-w "${FILE_B}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest "${FILE_B}" --on-dest-error=skip --block-size="${BLOCK_SIZE}"B --max-passes=1 --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 65
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    after_each_test
}

file_test_14() {
    next_test "File Test 14 (non-UTF8 file names)"
    before_each_test
    prepare_file "hellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${NON_UTF8_FILE_A}"
    prepare_file "hals" > "${NON_UTF8_FILE_B}"
    "${UNDER_TEST}" --src="${NON_UTF8_FILE_A}" --dest "${NON_UTF8_FILE_B}" --block-size="${BLOCK_SIZE}"B --max-passes=1 --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 2
    check_unmodified_file "${ORIG_FILE_A}" "${NON_UTF8_FILE_A}"
    check_diff "${NON_UTF8_FILE_A}" "${NON_UTF8_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 2 1 2 0 0 0
    after_each_test
}

# run tests
file_test_1a
file_test_1b
file_test_1c
file_test_2a
file_test_2b
file_test_3a
file_test_3b
file_test_3c
file_test_3d
file_test_4
file_test_5a
file_test_5b
file_test_5c
file_test_6a
file_test_6b
file_test_6c
file_test_7a
file_test_7b
file_test_8a
file_test_8b
file_test_9a
file_test_9b
file_test_10a
file_test_10b
file_test_10c
file_test_11
file_test_12
file_test_13a
file_test_13b
file_test_13c
file_test_13d
file_test_14



### stdout tests ###

stdout_test_1a() {
    next_test "Stdout Test 1a (stdout=|, --max-passes=1 --copy-first)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    "${UNDER_TEST}" --src "${FILE_A}" --max-passes=1 --copy-first --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}" | cat > "${FILE_B}"
    check_exit_code "${PIPESTATUS[0]}" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

stdout_test_1b() {
    next_test "Stdout Test 1b (stdout=|, --copy-first)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    "${UNDER_TEST}" --src "${FILE_A}" --copy-first --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}" | cat > "${FILE_B}"
    check_exit_code "${PIPESTATUS[0]}" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

stdout_test_1c() {
    next_test "Stdout Test 1c (stdout=|, --max-passes=1)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    "${UNDER_TEST}" --src "${FILE_A}" --max-passes=1 --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}" | cat > "${FILE_B}"
    check_exit_code "${PIPESTATUS[0]}" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

stdout_test_1d() {
    next_test "Stdout Test 1d (stdout=|)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    "${UNDER_TEST}" --src "${FILE_A}" --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}" | cat > "${FILE_B}"
    check_exit_code "${PIPESTATUS[0]}" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

stdout_test_1e() {
    next_test "Stdout Test 1e (stdout=|, --max-passes=2 --copy-first)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    "${UNDER_TEST}" --src "${FILE_A}" --max-passes=2 --copy-first --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}" | cat > "${FILE_B}"
    check_exit_code "${PIPESTATUS[0]}" 64
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    after_each_test
}

stdout_test_2a() {
    next_test "Stdout Test 2a (> file, --max-passes=1 --copy-first)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    "${UNDER_TEST}" --src "${FILE_A}" --max-passes=1 --copy-first --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}" > "${FILE_B}"
    check_exit_code "$?" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

stdout_test_2b() {
    next_test "Stdout Test 2b (> file, --copy-first)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    "${UNDER_TEST}" --src "${FILE_A}" --copy-first --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}" > "${FILE_B}"
    check_exit_code "$?" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

stdout_test_2c() {
    next_test "Stdout Test 2c (> file, --max-passes=1)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    "${UNDER_TEST}" --src "${FILE_A}" --max-passes=1 --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}" > "${FILE_B}"
    check_exit_code "$?" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

stdout_test_2d() {
    next_test "Stdout Test 2d (> file)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    "${UNDER_TEST}" --src "${FILE_A}" --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}" > "${FILE_B}"
    check_exit_code "$?" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

stdout_test_2e() {
    next_test "Stdout Test 2e (> file, --max-passes=2 --copy-first)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    "${UNDER_TEST}" --src "${FILE_A}" --max-passes=2 --copy-first --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}" > "${FILE_B}"
    check_exit_code "$?" 64
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    after_each_test
}

stdout_test_3a() {
    next_test "Stdout Test 3a (> fifo, --max-passes=1 --copy-first)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    mkfifo "${FIFO_FILE_B}"
    "${UNDER_TEST}" --src "${FILE_A}" --max-passes=1 --copy-first --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}" > "${FIFO_FILE_B}" &
    UNDER_TEST_PID="$!"
    cat "${FIFO_FILE_B}" > "${FILE_B}"
    wait "${UNDER_TEST_PID}"
    check_exit_code "$?" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

stdout_test_3b() {
    next_test "Stdout Test 3b (> fifo, --copy-first)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    mkfifo "${FIFO_FILE_B}"
    "${UNDER_TEST}" --src "${FILE_A}" --copy-first --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}" > "${FIFO_FILE_B}" &
    UNDER_TEST_PID="$!"
    cat "${FIFO_FILE_B}" > "${FILE_B}"
    wait "${UNDER_TEST_PID}"
    check_exit_code "${PIPESTATUS[0]}" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

stdout_test_3c() {
    next_test "Stdout Test 3c (> fifo, --max-passes=1)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    mkfifo "${FIFO_FILE_B}"
    "${UNDER_TEST}" --src "${FILE_A}" --max-passes=1 --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}" > "${FIFO_FILE_B}" &
    UNDER_TEST_PID="$!"
    cat "${FIFO_FILE_B}" > "${FILE_B}"
    wait "${UNDER_TEST_PID}"
    check_exit_code "${PIPESTATUS[0]}" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

stdout_test_3d() {
    next_test "Stdout Test 3d (> fifo)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    mkfifo "${FIFO_FILE_B}"
    "${UNDER_TEST}" --src "${FILE_A}" --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}" > "${FIFO_FILE_B}" &
    UNDER_TEST_PID="$!"
    cat "${FIFO_FILE_B}" > "${FILE_B}"
    wait "${UNDER_TEST_PID}"
    check_exit_code "${PIPESTATUS[0]}" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

stdout_test_3e() {
    next_test "Stdout Test 3e (> fifo, --max-passes=2 --copy-first)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    mkfifo "${FIFO_FILE_B}"
    "${UNDER_TEST}" --src "${FILE_A}" --max-passes=2 --copy-first --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}" > "${FIFO_FILE_B}" &
    UNDER_TEST_PID="$!"
    cat "${FIFO_FILE_B}" > "${FILE_B}"
    wait "${UNDER_TEST_PID}"
    check_exit_code "$?" 64
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    after_each_test
}

stdout_test_4a() {
    next_test "Stdout Test 4a (no dest file, --max-passes=1 --copy-first)"
    before_each_test
    echo -n "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    "${UNDER_TEST}" --src "${FILE_A}" --max-passes=1 --copy-first --block-size=1B --buf-size=2B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

stdout_test_4b() {
    next_test "Stdout Test 4b (no dest file, --copy-first)"
    before_each_test
    echo -n "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    "${UNDER_TEST}" --src "${FILE_A}" --copy-first --block-size=1B --buf-size=2B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

stdout_test_4c() {
    next_test "Stdout Test 4c (no dest file, --max-passes=1)"
    before_each_test
    echo -n "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    "${UNDER_TEST}" --src "${FILE_A}" --max-passes=1 --block-size=1B --buf-size=2B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

stdout_test_4d() {
    next_test "Stdout Test 4d (no dest file)"
    before_each_test
    echo -n "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    "${UNDER_TEST}" --src "${FILE_A}" --block-size=1B --buf-size=2B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

stdout_test_4e() {
    next_test "Stdout Test 4e (no dest file, --max-passes=2 --copy-first)"
    before_each_test
    echo -n "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    "${UNDER_TEST}" --src "${FILE_A}" --max-passes=2 --copy-first --block-size=1B --buf-size=2B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 64
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    after_each_test
}

stdout_test_5a() {
    next_test "Stdout Test 5a (dest=fifo, --max-passes=1 --copy-first)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    mkfifo "${FIFO_FILE_B}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest="${FIFO_FILE_B}" --max-passes=1 --copy-first --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}" &
    UNDER_TEST_PID="$!"
    cat "${FIFO_FILE_B}" > "${FILE_B}"
    wait "${UNDER_TEST_PID}"
    check_exit_code "$?" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

stdout_test_5b() {
    next_test "Stdout Test 5b (dest=fifo, --copy-first)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    mkfifo "${FIFO_FILE_B}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest="${FIFO_FILE_B}" --copy-first --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}" &
    UNDER_TEST_PID="$!"
    cat "${FIFO_FILE_B}" > "${FILE_B}"
    wait "${UNDER_TEST_PID}"
    check_exit_code "${PIPESTATUS[0]}" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

stdout_test_5c() {
    next_test "Stdout Test 5c (dest=fifo, --max-passes=1)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    mkfifo "${FIFO_FILE_B}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest="${FIFO_FILE_B}" --max-passes=1 --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}" &
    UNDER_TEST_PID="$!"
    cat "${FIFO_FILE_B}" > "${FILE_B}"
    wait "${UNDER_TEST_PID}"
    check_exit_code "${PIPESTATUS[0]}" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

stdout_test_5d() {
    next_test "Stdout Test 5d (dest=fifo)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    mkfifo "${FIFO_FILE_B}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest="${FIFO_FILE_B}" --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}" &
    UNDER_TEST_PID="$!"
    cat "${FIFO_FILE_B}" > "${FILE_B}"
    wait "${UNDER_TEST_PID}"
    check_exit_code "${PIPESTATUS[0]}" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

stdout_test_5e() {
    next_test "Stdout Test 5e (dest=fifo, --max-passes=2 --copy-first)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    mkfifo "${FIFO_FILE_B}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest="${FIFO_FILE_B}" --max-passes=2 --copy-first --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 64
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    after_each_test
}

stdout_test_6a() {
    next_test "Stdout Test 6a (dest=file, stdout=|)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest="${FILE_B}" --max-passes=1 --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}" | cat
    check_exit_code "$?" 0
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

stdout_test_6b() {
    next_test "Stdout Test 6b (dest=/dev/stdout, stdout=|)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest="/dev/stdout" --max-passes=1 --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}" | cat > "${FILE_B}"
    check_exit_code "$?" 0
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

stdout_test_7() {
    next_test "Stdout Test 7 (stdout=|, NUL-bytes)"
    before_each_test
    prepare_file "________________" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    "${UNDER_TEST}" --src "${FILE_A}" --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}" | cat > "${FILE_B}"
    check_exit_code "${PIPESTATUS[0]}" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

# run tests
stdout_test_1a
stdout_test_1b
stdout_test_1c
stdout_test_1d
stdout_test_1e
stdout_test_2a
stdout_test_2b
stdout_test_2c
stdout_test_2d
stdout_test_2e
stdout_test_3a
stdout_test_3b
stdout_test_3c
stdout_test_3d
stdout_test_3e
#stdout_test_4a # mixes stdout and stderr which leads to bad terminal escape sequences
#stdout_test_4b # mixes stdout and stderr which leads to bad terminal escape sequences
#stdout_test_4c # mixes stdout and stderr which leads to bad terminal escape sequences
#stdout_test_4d # mixes stdout and stderr which leads to bad terminal escape sequences
#stdout_test_4e # mixes stdout and stderr which leads to bad terminal escape sequences
stdout_test_5a
stdout_test_5b
stdout_test_5c
stdout_test_5d
stdout_test_5e
stdout_test_6a
stdout_test_6b
stdout_test_7



### stdin tests ###

stdin_test_1a() {
    next_test "Stdin Test 1a (stdin=|, --max-passes=1)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    cat "${FILE_A}" | "${UNDER_TEST}" --dest "${FILE_B}" --max-passes=1 --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "${PIPESTATUS[1]}" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

stdin_test_1b() {
    next_test "Stdin Test 1b (stdin=|)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    cat "${FILE_A}" | "${UNDER_TEST}" --dest "${FILE_B}" --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "${PIPESTATUS[1]}" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

stdin_test_1c() {
    next_test "Stdin Test 1c (stdin=|, --max-passes=2)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    cat "${FILE_A}" | "${UNDER_TEST}" --dest "${FILE_B}" --max-passes=2 --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "${PIPESTATUS[1]}" 64
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    after_each_test
}

stdin_test_2a() {
    next_test "Stdin Test 2a (< file, --max-passes=1)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    "${UNDER_TEST}" --dest "${FILE_B}" --max-passes=1 --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}" < "${FILE_A}"
    check_exit_code "$?" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

stdin_test_2b() {
    next_test "Stdin Test 2b (< file)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    "${UNDER_TEST}" --dest "${FILE_B}" --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}" < "${FILE_A}"
    check_exit_code "$?" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

stdin_test_2c() {
    next_test "Stdin Test 2c (< file, --max-passes=2)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    "${UNDER_TEST}" --dest "${FILE_B}" --max-passes=2 --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}" < "${FILE_A}"
    check_exit_code "$?" 64
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    after_each_test
}

stdin_test_3a() {
    next_test "Stdin Test 3a (< fifo, --max-passes=1)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    mkfifo "${FIFO_FILE_A}"
    "${UNDER_TEST}" --dest "${FILE_B}" --max-passes=1 --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}" < "${FIFO_FILE_A}" &
    UNDER_TEST_PID="$!"
    cat "${FILE_A}" > "${FIFO_FILE_A}"
    wait "${UNDER_TEST_PID}"
    check_exit_code "$?" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

stdin_test_3b() {
    next_test "Stdin Test 3b (< fifo)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    mkfifo "${FIFO_FILE_A}"
    "${UNDER_TEST}" --dest "${FILE_B}" --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}" < "${FIFO_FILE_A}" &
    UNDER_TEST_PID="$!"
    cat "${FILE_A}" > "${FIFO_FILE_A}"
    wait "${UNDER_TEST_PID}"
    check_exit_code "$?" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

stdin_test_3c() {
    next_test "Stdin Test 3c (< fifo, --max-passes=2)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    mkfifo "${FIFO_FILE_A}"
    "${UNDER_TEST}" --dest "${FILE_B}" --max-passes=2 --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}" < "${FIFO_FILE_A}" &
    UNDER_TEST_PID="$!"
    cat "${FILE_A}" > "${FIFO_FILE_A}"
    wait "${UNDER_TEST_PID}"
    check_exit_code "$?" 64
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    after_each_test
}

stdin_test_4a() {
    next_test "Stdin Test 4a (no src file, --max-passes=1)"
    before_each_test
    echo "hello" > "${DESIRED_FILE_B}"
    "${UNDER_TEST}" --dest "${FILE_B}" --max-passes=1 --block-size=1B --buf-size=2B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 2
    check_diff "${FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

stdin_test_4b() {
    next_test "Stdin Test 4b (no src file)"
    before_each_test
    echo "hello" > "${DESIRED_FILE_B}"
    "${UNDER_TEST}" --dest "${FILE_B}" --block-size=1B --buf-size=2B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 2
    check_diff "${FILE_B}" "${DESIRED_FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

stdin_test_4c() {
    next_test "Stdin Test 4c (no src file, --max-passes=2)"
    before_each_test
    echo "hello" > "${DESIRED_FILE_B}"
    "${UNDER_TEST}" --dest "${FILE_B}" --max-passes=2 --block-size=1B --buf-size=2B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 64
    after_each_test
}

stdin_test_5a() {
    next_test "Stdin Test 5a (src=fifo, --max-passes=1)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    mkfifo "${FIFO_FILE_A}"
    "${UNDER_TEST}" --src "${FIFO_FILE_A}" --dest="${FILE_B}" --max-passes=1 --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}" &
    UNDER_TEST_PID="$!"
    cat "${FILE_A}" > "${FIFO_FILE_A}"
    wait "${UNDER_TEST_PID}"
    check_exit_code "$?" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

stdin_test_5b() {
    next_test "Stdin Test 5b (src=fifo)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    mkfifo "${FIFO_FILE_A}"
    "${UNDER_TEST}" --src "${FIFO_FILE_A}" --dest="${FILE_B}" --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}" &
    UNDER_TEST_PID="$!"
    cat "${FILE_A}" > "${FIFO_FILE_A}"
    wait "${UNDER_TEST_PID}"
    check_exit_code "$?" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

stdin_test_5c() {
    next_test "Stdin Test 5c (src=fifo, --max-passes=2 --copy-first)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    mkfifo "${FIFO_FILE_A}"
    "${UNDER_TEST}" --src "${FIFO_FILE_A}" --dest="${FILE_B}" --max-passes=2 --copy-first --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 64
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    after_each_test
}

stdin_test_6a() {
    next_test "Stdin Test 6a (src=file, stdin=|)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    echo -n 'anything' | "${UNDER_TEST}" --src "${FILE_A}" --dest="${FILE_B}" --max-passes=1 --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "${PIPESTATUS[1]}" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

stdin_test_6b() {
    next_test "Stdin Test 6b (src=/dev/stdin, stdin=|)"
    before_each_test
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    cat "${FILE_A}" | "${UNDER_TEST}" --src "/dev/stdin" --dest="${FILE_B}" --max-passes=1 --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "${PIPESTATUS[1]}" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 0 0 0 0
    after_each_test
}

stdin_test_7a() {
    next_test "Stdin Test 7a (stdin=cat emptyfile |, no --force)"
    before_each_test
    prepare_file "" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${DESIRED_FILE_B}"
    cp "${DESIRED_FILE_B}" "${FILE_B}"
    cat "${FILE_A}" | "${UNDER_TEST}" --dest "${FILE_B}" --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "${PIPESTATUS[1]}" 65
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_unmodified_file "${DESIRED_FILE_B}" "${FILE_B}"
    after_each_test
}

stdin_test_7b() {
    next_test "Stdin Test 7b (stdin=cat emptyfile |, --force)"
    before_each_test
    prepare_file "" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    prepare_file "0123456789abcdef0123456789abcdefhellos" > "${FILE_B}"
    cat "${FILE_A}" | "${UNDER_TEST}" --dest "${FILE_B}" --force --block-size="${BLOCK_SIZE}"B --buf-size="$((2 * BLOCK_SIZE))"B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "${PIPESTATUS[1]}" 0
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 0 0 0 0 0
    after_each_test
}

# run tests
stdin_test_1a
stdin_test_1b
stdin_test_1c
stdin_test_2a
stdin_test_2b
stdin_test_2c
stdin_test_3a
stdin_test_3b
stdin_test_3c
#stdin_test_4a # needs input 'hello\n' from stdin (keyboard) in order to work
#stdin_test_4b # needs input 'hello\n' from stdin (keyboard) in order to work
stdin_test_4c
stdin_test_5a
stdin_test_5b
stdin_test_5c
stdin_test_6a
stdin_test_6b
stdin_test_7a
stdin_test_7b



### misc tests ###

misc_test_1() {
    next_test "Misc Test 1 (static progress bar stress test)"
    before_each_test
    prepare_file "hellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    prepare_file "hals" > "${FILE_B}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest "${FILE_B}" --block-size=1B --max-passes=1 --buf-size=1B --stats-unit=B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 2 1 2 0 0 0
    after_each_test
}

misc_test_2() {
    next_test "Misc Test 2 (dynamic progress bar stress test)"
    before_each_test
    # prepare_file "hellos" > "${ORIG_FILE_A}"
    prepare_file "hellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    prepare_file "hals" > "${FILE_B}"
    cat "${FILE_A}" | "${UNDER_TEST}" --dest "${FILE_B}" --block-size=1B --max-passes=1 --buf-size=1B --stats-unit=B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 2 1 2 0 0 0
    after_each_test
}

misc_test_3() {
    next_test "Misc Test 3 (big file, --copy-first)"
    before_each_test
    prepare_file "hellos" > "${ORIG_FILE_A}"
    cp "${ORIG_FILE_A}" "${FILE_A}"
    prepare_file "hals" > "${FILE_B}"
    "${UNDER_TEST}" --src "${FILE_A}" --dest "${FILE_B}" --copy-first --block-size=1B --max-passes=1 --buf-size=1B --stats-unit=B --block-log-file "${BLOCK_LOG_FILE}"
    check_exit_code "$?" 2
    check_unmodified_file "${ORIG_FILE_A}" "${FILE_A}"
    check_diff "${FILE_A}" "${FILE_B}" 0
    check_log "${BLOCK_LOG_FILE}" 1 0 1 1 0 0 0
    after_each_test
}

# run tests
misc_test_1
misc_test_2
misc_test_3

