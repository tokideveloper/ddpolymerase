#!/usr/bin/env bash

# ddpolymerase: Copy, repair and verify a file
# Copyright (C) 2021-2023  Tobias Killer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.



### init ###

next_test() {
    echo -e "\033[32;40;1m--- $1 ---\033[0m"
}

error_exit() {
    echo -e "\033[31;47;1mTest Error: $1\033[0m" >&2
    exit "$2"
}

UNDER_TEST='../target/debug/ddpolymerase'

TEST_DIR="/tmp/ddpolymerase-test"

BLOCK_LOG_FILE="${TEST_DIR}/log"

FILE_A="${TEST_DIR}/a"
FILE_B="${TEST_DIR}/b"
ORIG_FILE_A="${TEST_DIR}/c"
DESIRED_FILE_B="${TEST_DIR}/d"

NON_UTF8_FILE_A="${TEST_DIR}/$'\xAA'"
NON_UTF8_FILE_B="${TEST_DIR}/$'\xAB'"

DEVICE_FILE_A="${TEST_DIR}/dev_a"
DEVICE_FILE_B="${TEST_DIR}/dev_b"
DEVICE_FILE_C="${TEST_DIR}/dev_c"

DEVICE_A="ddpolymerase-test-a"
DEVICE_B="ddpolymerase-test-b"

FIFO_FILE_A="${TEST_DIR}/fifo_a"
FIFO_FILE_B="${TEST_DIR}/fifo_b"

FS_C="${TEST_DIR}/fs_c"
FS_C_FILE_B="${FS_C}/bbb"

get_block_size() {
    local DIR="$1"
    stat --file-system --cached=never --printf=%S "${DIR}"
}

mkdir -p "${TEST_DIR}"
# The size of a "real" block. (The device mapper throws I/O errors aligned to "real" block boundaries.)
BLOCK_SIZE="$(get_block_size ${TEST_DIR})" || error_exit "Could not determine block size for '${TEST_DIR}'." 1
SECTORS_PER_BLOCK="$((BLOCK_SIZE / 512))"

prepare_file() {
    local TEMPLATE="$1"
    while read -n 1 CHAR; do
        if [ ! -z "$CHAR" ]
        then
            if [ "$CHAR" = "_" ]
            then
                for i in $(seq 1 "${BLOCK_SIZE}");
                do
                    printf "%s\0"
                done
            else
                printf "%${BLOCK_SIZE}s" | tr " " "${CHAR}"
            fi
        fi
    done <<< "${TEMPLATE}"
}

find_free_loop_device() {
    losetup -f
}

detach_loop_devices() {
    losetup --list --output NAME,BACK-FILE | grep -e "$1" | awk '{print $1}' | \
    while IFS=$'\n' read LINE; do
        echo "${LINE}"
        losetup -d "${LINE}"
    done
}

rm_files() {
    rm -f "${FILE_A}" "${FILE_B}" "${ORIG_FILE_A}" "${DESIRED_FILE_B}" "${NON_UTF8_FILE_A}" "${NON_UTF8_FILE_B}" "${DEVICE_FILE_A}" "${DEVICE_FILE_B}" "${DEVICE_FILE_C}" "${FIFO_FILE_A}" "${FIFO_FILE_B}" "${BLOCK_LOG_FILE}" "${FS_C_FILE_B}"
    mount | grep -e "${FS_C}" && (umount "${FS_C}" || error_exit "Could not umount '${FS_C}' while removing files." 3)
    [ -d "${FS_C}" ] && rmdir --ignore-fail-on-non-empty "${FS_C}"
    dmsetup remove "${DEVICE_A}" 2> /dev/null
    dmsetup remove "${DEVICE_B}" 2> /dev/null
    detach_loop_devices "${DEVICE_FILE_A}"
    detach_loop_devices "${DEVICE_FILE_B}"
}

before_each_test() {
    mkdir -p "${TEST_DIR}"
    rm_files
    mkdir -p "${FS_C}"
}

after_each_test() {
    rm_files
    rmdir "${TEST_DIR}"
}

check_exit_code() {
    local RESULT="$1"
    local EXPECTED="$2"
    [ "$RESULT" -eq "$EXPECTED" ] || error_exit "Actual Result Exit Code: $RESULT, Expected: $EXPECTED." 2
}

check_diff() {
    diff -q "$1" "$2" 1> /dev/null
    local RESULT="$?"
    local EXPECTED="$3"
    [ "$RESULT" -eq "$EXPECTED" ] || error_exit "Actual Diff Exit Code: $RESULT, Expected: $EXPECTED." 2
}

check_unmodified_file() {
    diff -q "$1" "$2" 1> /dev/null
    local RESULT="$?"
    local EXPECTED=0
    [ "$RESULT" -eq "$EXPECTED" ] || error_exit "Actual Unmodified File Check Exit Code: $RESULT, Expected: $EXPECTED." 2
}

check_log_helper() {
    local LOG_FILE="$1"
    local LINE_TYPE="$2"
    local EXPECTED_NUMBER_OF_LINES="$3"
    local RESULT_NUMBER_OF_LINES="$(grep -c -e "^${LINE_TYPE}" "${LOG_FILE}")"
    [ "$RESULT_NUMBER_OF_LINES" -eq "$EXPECTED_NUMBER_OF_LINES" ] || error_exit "Different number of '$LINE_TYPE' log lines. Actual: $RESULT_NUMBER_OF_LINES, expected: $EXPECTED_NUMBER_OF_LINES." 2
}

check_log() {
    local LOG_FILE="$1"
    local EXPECTED_PASSES="$2"
    local EXPECTED_GOOD="$3"
    local EXPECTED_COPIED="$4"
    local EXPECTED_REPAIRED="$5"
    local EXPECTED_BAD="$6"
    local EXPECTED_SRC_ERRORS="$7"
    local EXPECTED_DEST_ERRORS="$8"
    check_log_helper "$LOG_FILE" 'P' "$EXPECTED_PASSES"
    check_log_helper "$LOG_FILE" 'g' "$EXPECTED_GOOD"
    check_log_helper "$LOG_FILE" 'c' "$EXPECTED_COPIED"
    check_log_helper "$LOG_FILE" 'r' "$EXPECTED_REPAIRED"
    check_log_helper "$LOG_FILE" 'b' "$EXPECTED_BAD"
    check_log_helper "$LOG_FILE" 's' "$EXPECTED_SRC_ERRORS"
    check_log_helper "$LOG_FILE" 'd' "$EXPECTED_DEST_ERRORS"
}

